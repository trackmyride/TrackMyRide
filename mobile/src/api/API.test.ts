import API from "./API";

// setup
jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
const globalAny: any = global;
const HTTP_GET = "GET";
const HTTP_HEADER = {"Accept": "application/json", "Content-Type": "application/json"};
const HTTP_POST = "POST";

// spies

// mocks

// tests
describe("Test session handling", () => {
    const avatar = null;
    const email = "test@dom.tld";
    const username = "tester";

    it("correctly fetches user data", async () => {
        // prepare
        globalAny.fetch = jest.fn().mockImplementation(() => Promise.resolve({
            json: () => Promise.resolve({
                avatar, email, username,
            }),
            status: 200,
        }));

        // run
        const data = await API.getSession();

        // assert
        expect(data.avatar).toEqual(avatar);
        expect(data.email).toEqual(email);
        expect(data.username).toEqual(username);
        expect(data.isValid).toBe(true);
    });

    it("fails on error responses", async () => {
        // prepare
        globalAny.fetch = jest.fn().mockImplementation(() => Promise.resolve({
            status: 321,
        }));

        // run
        const data = await API.getSession();

        // assert
        expect(data.avatar).toEqual("");
        expect(data.email).toEqual("");
        expect(data.username).toEqual("");
        expect(data.isValid).toBe(false);
    });
});

describe("Test tour handling", () => {
    const data = {
        str: "hype",
        test: [ 1, 2, 3, 4 ],
    };

    it("correctly returns on successful response", async () => {
        // prepare
        globalAny.fetch = jest.fn().mockImplementation(() => Promise.resolve({
            status: 201,
        }));

        // run
        const apiResponse = await API.sendTour(data);

        // assert
        expect(apiResponse.success).toBe(true);
        expect(globalAny.fetch).toHaveBeenCalledWith(jasmine.any(String), {
            body: JSON.stringify(data),
            credentials: "include",
            headers: HTTP_HEADER,
            method: HTTP_POST,
        });
    });

    it("correctly returns on error response", async () => {
        // prepare
        globalAny.fetch = jest.fn().mockImplementation(() => Promise.resolve({
            status: 409,
        }));

        // run
        const apiResponse = await API.sendTour(data);

        // assert
        expect(apiResponse.success).toBe(false);
        expect(globalAny.fetch).toHaveBeenCalledWith(jasmine.any(String), {
            body: JSON.stringify(data),
            credentials: "include",
            headers: HTTP_HEADER,
            method: HTTP_POST,
        });
    });

    it("correctly returns on fetch error", async () => {
        // prepare
        globalAny.fetch = jest.fn().mockImplementation(() => Promise.reject({
            reason: "nope",
        }));

        // run
        const apiResponse = await API.sendTour(data);

        // assert
        expect(apiResponse.success).toBe(false);
        expect(globalAny.fetch).toHaveBeenCalledWith(jasmine.any(String), {
            body: JSON.stringify(data),
            credentials: "include",
            headers: HTTP_HEADER,
            method: HTTP_POST,
        });
    });
});

describe("Test login handling", () => {
    const username = "tester";
    const password = "U|tr@S€cr3t!";

    it("correctly returns on successful response", async () => {
        // prepare
        globalAny.fetch = jest.fn().mockImplementation(() => Promise.resolve({
            status: 200,
        }));

        // run
        const apiResponse = await API.login(username, password);

        // assert
        expect(apiResponse.success).toBe(true);
        expect(globalAny.fetch).toHaveBeenCalledWith(jasmine.any(String), {
            body: JSON.stringify({username, password}),
            credentials: "include",
            headers: HTTP_HEADER,
            method: HTTP_POST,
        });
    });

    it("correctly returns on error response", async () => {
        // prepare
        globalAny.fetch = jest.fn().mockImplementation(() => Promise.resolve({
            status: 401,
        }));

        // run
        const apiResponse = await API.login(username, password);

        // assert
        expect(apiResponse.success).toBe(false);
        expect(globalAny.fetch).toHaveBeenCalledWith(jasmine.any(String), {
            body: JSON.stringify({username, password}),
            credentials: "include",
            headers: HTTP_HEADER,
            method: HTTP_POST,
        });
    });

    it("returns false on promise exceptions", async () => {
        // prepare
        globalAny.fetch = jest.fn().mockImplementation(() => Promise.reject({
            reason: "nope",
        }));

        // run
        const apiResponse = await API.login(username, password);

        // assert
        expect(apiResponse.success).toBe(false);
        expect(globalAny.fetch).toHaveBeenCalledWith(jasmine.any(String), {
            body: JSON.stringify({username, password}),
            credentials: "include",
            headers: HTTP_HEADER,
            method: HTTP_POST,
        });
    });
});

describe("Test logout handling", () => {

    it("correctly returns on successful response", async () => {
        // prepare
        globalAny.fetch = jest.fn().mockImplementation(() => Promise.resolve({
            status: 200,
        }));

        // run
        const apiResponse = await API.logout();

        // assert
        expect(apiResponse.success).toBe(true);
        expect(globalAny.fetch).toHaveBeenCalledWith(jasmine.any(String), {
            credentials: "include",
            headers: HTTP_HEADER,
            method: HTTP_GET,
        });
    });

    it("correctly returns on error response", async () => {
        // prepare
        globalAny.fetch = jest.fn().mockImplementation(() => Promise.resolve({
            status: 401,
        }));

        // run
        const apiResponse = await API.logout();

        // assert
        expect(apiResponse.success).toBe(false);
        expect(globalAny.fetch).toHaveBeenCalledWith(jasmine.any(String), {
            credentials: "include",
            headers: HTTP_HEADER,
            method: HTTP_GET,
        });
    });

    it("returns false on promise exceptions", async () => {
        // prepare
        globalAny.fetch = jest.fn().mockImplementation(() => Promise.reject({
            reason: "nope",
        }));

        // run
        const apiResponse = await API.logout();

        // assert
        expect(apiResponse.success).toBe(false);
        expect(globalAny.fetch).toHaveBeenCalledWith(jasmine.any(String), {
            credentials: "include",
            headers: HTTP_HEADER,
            method: HTTP_GET,
        });
    });
});
