const API_BASE = "https://www.trackmyride.ch/api";
const HEADER = { "Accept" : "application/json", "Content-Type" : "application/json" };

const GET = "GET";
const POST = "POST";

const USER_ENDPOINT = "/user";
const LOGIN_ENDPOINT = "/login";
const LOGOUT_ENDPOINT = "/logout";
const TOUR_ENDPOINT = "/tour";

export interface IAPIResponse {
    success: boolean;
    message: string;
    status: number;
}

export interface ISessionData {
    avatar: string;
    email: string;
    isValid: boolean;
    username: string;
}

interface ISessionResponse {
    avatar: string;
    email: string;
    username: string;
}

interface IHTTPStatusCode {
    [key: number]: string;
}

const httpStatusCodes: IHTTPStatusCode = {
    200: "OK",
    201: "Item created",
    400: "Invalid input, object invalid",
    403: "Unauthorized",
    500: "Internal server error",
};

const PROMISE_REJECTED: number = -1;

export default class API {

    /**
     * Tries getting a session from the backend and returns it.
     * @returns {Promise<ISessionData>} The session data object found.
     */
    public static getSession(): Promise<ISessionData> {

        const data: ISessionData = {
            avatar: "",
            email: "",
            isValid: false,
            username: "",
        };

        return this.baseGETCall(USER_ENDPOINT)
        .then((response: Response) => {
            if (response.status === 200) {
                data.isValid = true;
                return response.json().then((json: ISessionResponse) => {
                    data.avatar = json.avatar;
                    data.email = json.email;
                    data.username = json.username;
                    return data;
                });
            } else {
                return data;
            }
        });
    }

    /**
     * Tries logging out the user from the backend.
     * @returns {Promise<IAPIResponse>} False in case of error, true otherwise.
     */
    public static logout(): Promise<IAPIResponse> {
        const okStates = [200];

        return this.baseGETCall(LOGOUT_ENDPOINT)
        .then((response: Response) => {
            return this.determineErrorStatus(okStates, response.status);
        })
        .catch(() => {
            return this.determineErrorStatus(okStates, PROMISE_REJECTED);
        });
    }

    /**
     * Initiates a POST request to the backend's login endpoint.
     * @param  {string} username The username to authenticate as.
     * @param  {string} password The password belonging to the user.
     * @returns {Promise<IAPIResponse>} False in case of error, true otherwise.
     */
    public static login(username: string, password: string): Promise<IAPIResponse> {
        const okStates = [200];

        return this.basePOSTCall(LOGIN_ENDPOINT, {username, password} )
        .then((response: Response) => {
            return this.determineErrorStatus(okStates, response.status);
        })
        .catch(() => {
            return this.determineErrorStatus(okStates, PROMISE_REJECTED);
        });
    }

    /**
     * Tries uploading tour data to the backend.
     * @param  {object} data The data to upload.
     * @returns {Promise<IAPIResponse>} False in case of error, true otherwise.
     */
    public static sendTour(data: object): Promise<IAPIResponse> {
        const okStates = [201];

        return this.basePOSTCall(TOUR_ENDPOINT, data)
        .then((response: Response) => {
            return this.determineErrorStatus(okStates, response.status);
        }).catch((reason: any) => {
            return this.determineErrorStatus(okStates, PROMISE_REJECTED);
        });
    }

    /**
     * Basic GET call to the API.
     * @param  {string} endpoint The endpoint to call.
     * @returns {Promise<Response>} The full response object of fetch.
     */
    private static baseGETCall(endpoint: string): Promise<Response> {
        return fetch(API_BASE + endpoint, {
            credentials: "include",
            headers: HEADER,
            method: GET,
        });
    }

    /**
     * Basic POST call to the API.
     * @param  {string} endpoint The endpoint to post to.
     * @param  {object} data The data to post.
     * @returns {Promise<Response>} The full response object of fetch.
     */
    private static basePOSTCall(endpoint: string, data: object): Promise<Response> {
        return fetch(API_BASE + endpoint, {
            body: JSON.stringify(data),
            credentials: "include",
            headers: HEADER,
            method: POST,
        });
    }
    /**
     * Fill an APIResponse object with status messages based on the current status.
     * @param  {number[]} okStates The states which are considered ok.
     * @param  {number} status The current status.
     * @returns {IAPIResponse} An APIResponse object
     */
    private static determineErrorStatus(okStates: number[], status: number): IAPIResponse {
        const result: IAPIResponse = {
            message: "status message not found",
            status: 0,
            success: false,
        };

        result.success = okStates.some((e) => e === status);
        result.status = status;

        const message: string = httpStatusCodes[status];

        if (message) { // status code exists
            result.message = message;
        }

        return result;
    }
}
