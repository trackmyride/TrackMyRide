import * as React from "react";
import {
    ActivityIndicator,
    StyleSheet,
    Text,
    View,
} from "react-native";
import API, { ISessionData } from "../api/API";
import ExtendedButton from "../controls/ExtendedButton";

export default class Loader extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
        this.state = {
            failure: false,
        };

        this.checkLogon = this.checkLogon.bind(this);
    }

    /**
     * Executed when the component is first mounted.
     */
    public componentDidMount() {
        this.checkLogon();
    }

    /**
     * Render the loader view.
     * @returns {JSX.Element} The rendered component.
     */
    public render(): JSX.Element {
        let status = <ActivityIndicator size="large" color="#002674" />;
        let button = null;

        if (this.state.failure) {
            status = (
                <Text style={[styles.text, styles.error]}>
                    Fehler bei Serveranfrage.{"\n"}
                    Bitte prüfe die Internetverbindung deines Smartphones.
                </Text>
            );
            button = (
                <ExtendedButton
                    entypoIcon="ccw"
                    onPress={this.checkLogon}
                    style={styles.button}
                    title="Erneut versuchen"
                />
            );
        }

        return (
            <View style={styles.container}>
                <Text style={styles.text}>Prüfe Anmeldestatus...</Text>
                {status}
                {button}
            </View>
        );
    }

    /**
     * Tries validating the user's logon state and redirects on success.
     * In case of an error, the components state will be updated accordingly.
     */
    private checkLogon() {
        this.setState({failure: false});

        API.getSession().then((response: ISessionData) => {
            if (response.isValid) {
                this.props.navigation.navigate("App", {username: response.username});
            } else {
                this.props.navigation.navigate("LogonMask");
            }
        }).catch((reason: any) => {
            this.setState({failure: true});
        });
    }
}

const styles = StyleSheet.create({
    button: {
        marginTop: 10,
        width: "80%",
      },
    container: {
        alignItems: "center",
        flex: 1,
        justifyContent: "center",
    },
    error: {
        color: "rgb(255, 0, 0)",
        fontWeight: "bold",
        textAlign: "center",
    },
    text: {
        fontSize: 20,
    },
  });
