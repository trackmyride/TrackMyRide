const globalAny: any = global;
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import * as React from "react";
import API from "../api/API";
import LogonMask, { IMAGE_SIZE, IMAGE_SIZE_SMALL } from "./LogonMask";

// Note: test renderer must be required after react-native.
import renderer from "react-test-renderer";

// setup
jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;

configure({ adapter: new Adapter() });
let wrapper: any;
let render: any;

// spies

// mocks

// tests rendering of App
describe("Testing of LogonMask redering", () => {
    it("renders correctly", () => {
        const tree = renderer.create(<LogonMask />);
    });

    it("correctly unmounts", () => {
        wrapper = shallow(<LogonMask />);
        wrapper.unmount();
        expect(wrapper.instance()).toBeNull();
    });
});

describe("Testing state updates from input fields", () => {
    beforeEach(() => {
        wrapper = shallow(<LogonMask />);
        render = wrapper.dive();
    });

    const testUser = "testuser";
    const testPassword = "muchSecret";
    it("updates the state correctly when entering a username", () => {
        const input: any = render.find("#inputUserName");
        input.simulate("changeText", testUser);
        expect(wrapper.state().username).toEqual(testUser);
    });

    it("updates the state correctly when entering the password", () => {
        const input: any = render.find("#inputPassword");
        input.simulate("changeText", testPassword);
        expect(wrapper.state().password).toEqual(testPassword);
    });
});

describe("Testing the logon button", () => {

    beforeEach(() => {
        wrapper = shallow(<LogonMask />);
        render = wrapper.dive();
    });

    it("correctly validates emtpy username and password", () => {
        const button: any = render.find("ExtendedButton");
        button.simulate("press");
        expect(wrapper.state().passValid).toBe(false);
        expect(wrapper.state().userValid).toBe(false);
    });

    it("informs the user of a failed login", async () => {
        const ExtendedButton: any = render.find("ExtendedButton");
        const userInput: any = render.find("#inputUserName");
        const passInput: any = render.find("#inputPassword");

        API.login = jest.fn().mockImplementation(() => Promise.resolve({success: false}));

        userInput.simulate("changeText", "__no_user");
        expect(wrapper.state().userValid).toBe(true);

        passInput.simulate("changeText", "__no_pass");
        expect(wrapper.state().passValid).toBe(true);

        await ExtendedButton.simulate("press"); // there is a call to fetch which is async
        expect(wrapper.state().logonFailed).toBe(true);
    });

    it("redirects on successful login", async () => {
        const ExtendedButton: any = render.find("ExtendedButton");
        const userInput: any = render.find("#inputUserName");
        const passInput: any = render.find("#inputPassword");

        API.login = jest.fn().mockImplementation(() => Promise.resolve({success: true}));

        userInput.simulate("changeText", "__no_user");
        expect(wrapper.state().userValid).toBe(true);

        passInput.simulate("changeText", "__no_pass");
        expect(wrapper.state().passValid).toBe(true);

        await ExtendedButton.simulate("press"); // there is a call to fetch which is async
        expect(wrapper.state().logonFailed).toBe(false);

        // TODO: Does this test actually make sense?
        // We should be testing this.props.navigation.navigate here, shouldn't we?
    });
});

describe("Testing the view", () => {
    beforeEach(() => {
        wrapper = shallow(<LogonMask />);
        render = wrapper.dive();
    });

    it("correctly animates the logo size on keyboard events", async () => {
        // show keyboard
        wrapper.instance().keyboardDidShow(null);

        const logo: any = render.find("AnimatedComponent");
        let logoHeight = logo.props().style.height._animation._toValue;
        let logoWidth = logo.props().style.width._animation._toValue;

        expect(logoHeight).toEqual(IMAGE_SIZE_SMALL);
        expect(logoWidth).toEqual(IMAGE_SIZE_SMALL);

        // hide keyboard
        wrapper.instance().keyboardDidHide(null);

        logoHeight = logo.props().style.height._animation._toValue;
        logoWidth = logo.props().style.width._animation._toValue;

        expect(logoHeight).toEqual(IMAGE_SIZE);
        expect(logoWidth).toEqual(IMAGE_SIZE);

    });
});
