import * as React from "react";
import {
  Animated,
  Keyboard,
  KeyboardAvoidingView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import { Sae } from "react-native-textinput-effects";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import API, { ISessionData } from "../api/API";
import ExtendedButton from "../controls/ExtendedButton";

export const IMAGE_SIZE = 192; // px
export const IMAGE_SIZE_SMALL = 100; // px

export default class LogonMask extends React.Component<any, any> {
  public keyboardDidShowSub: any;
  public keyboardDidHideSub: any; // should be EventSubscription
  private imageSize: Animated.Value;

  constructor(props: any) {
    super(props);
    this.state = {
      logonFailed: false,
      passValid: true,
      password: "",
      userValid: true,
      username: "",
    };

    this.imageSize = new Animated.Value(IMAGE_SIZE);

    this.handleLogon = this.handleLogon.bind(this);
    this.updateUserName = this.updateUserName.bind(this);
    this.updatePassword = this.updatePassword.bind(this);
  }

  /**
   * Add event listeners to keyboard events to allow for dynamic UI scaling.
   */
  public componentWillMount() {
    this.keyboardDidShowSub = Keyboard.addListener("keyboardDidShow", this.keyboardDidShow.bind(this));
    this.keyboardDidHideSub = Keyboard.addListener("keyboardDidHide", this.keyboardDidHide.bind(this));
  }

  /**
   * Remove event listeners on keyboard events.
   */
  public componentWillUnmount() {
    this.keyboardDidShowSub.remove();
    this.keyboardDidHideSub.remove();
  }

  /**
   * Render the logon view.
   * @returns {JSX.Element} The rendered component.
   */
  public render(): JSX.Element {
    let logonHeader = (
      <Text style={styles.logonHead}>
        Bitte melde dich an,{"\n"}
        um deine Touren aufzuzeichnen
      </Text>
    );

    const userInputViewStyle = [styles.textInputWrapper];
    const passwordInputViewStyle = [styles.textInputWrapper];

    if (!this.state.passValid) {
      passwordInputViewStyle.push(styles.inputError);
    }

    if (!this.state.userValid) {
      userInputViewStyle.push(styles.inputError);
    }

    if (this.state.logonFailed) {
      logonHeader = (
        <Text style={[styles.logonHead, styles.logonHeadError]}>
          Bei der Anmeldung ist ein Fehler aufgetreten. Bitte versuche es erneut.
        </Text>
      );
    }

    return (
      <KeyboardAvoidingView
        style={styles.container}
        behavior="padding"
      >
        <Animated.Image
          style={{height: this.imageSize, width: this.imageSize}}
          source={require("../../res/logo.png")}
        />
        {logonHeader}
        <View style={userInputViewStyle}>
          <Sae
            id="inputUserName"
            value={this.state.username}
            onChangeText={this.updateUserName}
            label={"Benutzername"}
            // same for every input
            autoCapitalize={"none"}
            autoCorrect={false}
            iconClass={FontAwesomeIcon}
            iconName={"pencil"}
            iconColor={iconColor}
            inputStyle={styles.textInputInput}
            labelStyle={styles.textInputLabel}
            style={styles.textInput}
          />
        </View>
        <View style={passwordInputViewStyle}>
          <Sae
            id="inputPassword"
            value={this.state.password}
            onEndEditing={this.handleLogon}
            onChangeText={this.updatePassword}
            label={"Passwort"}
            secureTextEntry={true}
            // same for every input
            autoCapitalize={"none"}
            autoCorrect={false}
            iconClass={FontAwesomeIcon}
            iconName={"pencil"}
            iconColor={iconColor}
            inputStyle={styles.textInputInput}
            labelStyle={styles.textInputLabel}
            style={styles.textInput}
          />
        </View>
        <ExtendedButton
          onPress={this.handleLogon}
          style={styles.button}
          title="Anmelden"
        />
      </KeyboardAvoidingView>
    );
  }

  /**
   * Event handler function for keyboard shown event.
   * @param {any} event The event object.
   */
  public keyboardDidShow(event: any) {
    Animated.timing(this.imageSize, {
      toValue: IMAGE_SIZE_SMALL,
    }).start();
  }

  /**
   * Event handler function for keyboard hidden event.
   * @param {any} event The event object.
   */
  public keyboardDidHide(event: any) {
    Animated.timing(this.imageSize, {
      toValue: IMAGE_SIZE,
    }).start();
  }

  /**
   * Validates username and password are not emtpy,
   * executes a fetch call to the API's login endpoint and
   * validates the response.
   */
  private handleLogon() {
    if (this.validCredentials()) {
      API.login(this.state.username, this.state.password).then((apiResponse) => {
        if (apiResponse.success) {
            this.props.navigation.navigate("App", {username: this.state.username});
        } else {
            this.setState({logonFailed: true});
        }
      });
    }
  }

  /**
   * Handles text updates on the username field.
   * @param {string} text The new text to set in the username field.
   */
  private updateUserName(text: string) {
    this.setState({username: text});
  }

  /**
   * Handles text updates on the password field.
   * @param {string} text The new text to set in the password field.
   */
  private updatePassword(text: string) {
    this.setState({password: text});
  }

  /**
   * Verifies the input in username and password field are valid.
   * @returns {boolean} stating validity.
   */
  private validCredentials(): boolean {
    const passValid = !!this.state.password;
    const userValid = !!this.state.username;

    this.setState({
      passValid,
      userValid,
    });

    return passValid && userValid;
  }
}

const iconColor = "darkgray";
const styles = StyleSheet.create({
  button: {
    marginTop: 10,
    width: "80%",
  },
  container: {
    alignItems: "center",
    backgroundColor: "rgb(0, 38, 116)",
    flex: 1,
    justifyContent: "flex-start",
    paddingTop: "5%",
  },
  inputError: {
    borderColor: "red",
    borderWidth: 2,
  },
  logonHead: {
    color: "white",
    fontSize: 20,
    fontWeight: "bold",
    marginTop: 10,
    textAlign: "center",
  },
  logonHeadError: {
    color: "red",
  },
  textInput: {
    width: "95%",
  },
  textInputInput: {
    color: "black",
    fontSize: 18,
  },
  textInputLabel: {
    color: "darkgray",
  },
  textInputWrapper: {
    alignItems: "center",
    backgroundColor: "white",
    borderColor: "white",
    borderWidth: 2,
    marginTop: 5,
    width: "100%",
  },
});
