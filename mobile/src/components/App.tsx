import * as React from "react";
import {
    ActivityIndicator,
    Image,
    NativeModules,
    StyleSheet,
    Text,
    View,
} from "react-native";
import EntypoIcon from "react-native-vector-icons/Entypo";
import API, { IAPIResponse } from "../api/API";
import ExtendedButton from "../controls/ExtendedButton";
import TimeViewer from "../controls/TimeViewer";
import BackgroundTimer from "../logic_modules/BackgroundTimer";
import DataHandler, { ISensorData, STATES } from "../logic_modules/DataHandler";

// const globalAny: any = global;
const COLOR_RED: string = "rgb(255, 0, 0)";
const COLOR_GREEN: string = "rgb(0, 255, 0)";
const COLOR_YELLOW: string = "rgb(255, 255, 0)";

/**
 * Represents the View of the App
 */
export default class App extends React.Component<any, any> {

    private dataHandler: DataHandler = new DataHandler();

    private timerId: any;

    constructor(props: any) {
        super(props);

        this.state = {
            appState: STATES.SENSORS_STOPPED,
            time: 0,
        };

        this.startButtonHandler = this.startButtonHandler.bind(this);
        this.stopButtonHandler = this.stopButtonHandler.bind(this);
        this.logoutButtonHandler = this.logoutButtonHandler.bind(this);
    }

    /**
     * Render the central App view.
     * @returns {JSX.Element} The rendered component.
     */
    public render(): JSX.Element {
        const username = this.props.navigation ? this.props.navigation.getParam("username") : "";

        // button states
        let logoutButtonActive: boolean = false;
        let startButtonActive: boolean = true;
        let stopButtonActive: boolean = false;
        let statusElement: JSX.Element = (
            <Text style={styles.status}>
                Beginne die Aufzeichnung mit "Start"
            </Text>);

        // button layout
        let startButtonTitle: string = "Start";
        let startButtonColor: string = COLOR_GREEN;
        let startButtonImage: string = "controller-play";
        let stopButtonTitle: string = "Stopp";
        let stopButtonColor: string = COLOR_RED;
        let stopButtonImage: string = "controller-stop";

        // TODO: Add cancel button on UPLOAD_ACTIVE?
        // state handling
        const state = this.state.appState;
        if (state === STATES.SENSORS_STOPPED) {
            logoutButtonActive = true;

        } else if (state === STATES.SENSORS_STARTED) {
            stopButtonActive = true;

            startButtonTitle = "Pause";
            startButtonImage = "controller-paus";
            startButtonColor = COLOR_YELLOW;
            statusElement = (
                <Text style={styles.status}>
                    Aufzeichnung läuft
                </Text>);

        } else if (state === STATES.SENSORS_PAUSED) {
            stopButtonActive = true;

            statusElement = (
                <Text style={styles.status}>
                    Führe die Aufzeichnung mit "Start" fort
                </Text>);

        } else if (state === STATES.UPLOAD_ACTIVE) {
            startButtonActive = false;

            statusElement = (
                <View style={styles.statusView}>
                    <Text style={styles.status}>
                        Daten werden hochgeladen
                    </Text>
                    <ActivityIndicator style={styles.activity} size="small" color="#FFFFFF" />
                </View>);

        } else if (state === STATES.UPLOAD_FAILED) {
            stopButtonActive = true;

            startButtonTitle = "Erneut versuchen";
            startButtonColor = COLOR_GREEN;
            startButtonImage = "ccw";

            stopButtonTitle = "Tour entfernen";
            stopButtonColor = COLOR_RED;
            stopButtonImage = "trash";

            statusElement = (
                <Text style={[styles.status, styles.error]}>
                    Upload fehlgeschlagen
                </Text>);

        } else if (state === STATES.UPLOAD_SUCCESS) {
            logoutButtonActive = true;

            statusElement = (
                <Text style={[styles.status, styles.success]}>
                    Tour wurde erfolgreich hochgeladen!
                </Text>);
        }

        // render component
        return (
            <View style={styles.container}>
                <Image source={require("../../res/logo.png")} />
                <Text style={styles.welcome}>
                    Hallo {username}
                </Text>

                {statusElement}

                <ExtendedButton
                    entypoIcon={startButtonImage}
                    iconColor={startButtonColor}
                    disabled={!startButtonActive}
                    onPress={this.startButtonHandler}
                    style={styles.button}
                    title={startButtonTitle}
                />

                <ExtendedButton
                    entypoIcon={stopButtonImage}
                    iconColor={stopButtonColor}
                    disabled={!stopButtonActive}
                    onPress={this.stopButtonHandler}
                    style={styles.button}
                    title={stopButtonTitle}
                />

                <TimeViewer time={this.state.time} />

                <ExtendedButton
                    entypoIcon="log-out"
                    iconSize={25}
                    disabled={!logoutButtonActive}
                    onPress={this.logoutButtonHandler}
                    style={styles.logoutButton}
                    title="Abmelden"
                />
            </View>
        );
    }

    /**
     * Handles the start button, for differentiating start and pause
     */
    public startButtonHandler() {
        const state = this.state.appState;
        if (state === STATES.SENSORS_STARTED) { // recording is active; pause

            this.setState({
                appState: STATES.SENSORS_PAUSED,
            }, this.stopTimer);

            // stopping data recording
            this.dataHandler.pauseRecording();

            NativeModules.ToastCreator.show("Aufzeichnung pausiert.", NativeModules.ToastCreator.SHORT);

        } else if (
            state === STATES.SENSORS_PAUSED ||
            state === STATES.SENSORS_STOPPED ||
            state === STATES.UPLOAD_SUCCESS) {

            this.dataHandler.startRecording().then((locationGranted: boolean) => {
                if (locationGranted) {
                    // show toast
                    NativeModules.ToastCreator.show("Aufzeichnung gestartet.", NativeModules.ToastCreator.SHORT);

                    // update state and start timer
                    this.setState({
                        appState: STATES.SENSORS_STARTED,
                    }, this.startTimer);
                } else {
                    NativeModules.ToastCreator.show("Zugriff auf Positionsdaten wurde verweigert. " +
                        "Aufzeichnung kann nicht gestartet werden.",
                        NativeModules.ToastCreator.LONG);
                }
            });
        } else if (state === STATES.UPLOAD_FAILED) { // retry upload
            this.tryUpload();
        }
    }

    /**
     * Handles the stop button click event.
     * Depending on the state the app is in, this will either stop the recording
     * and upload data, clear the data or try re-uploading the data.
     */
    public stopButtonHandler() {
        const state = this.state.appState;
        if (state === STATES.SENSORS_STARTED || state === STATES.SENSORS_PAUSED) {
            this.stopTimer();

            NativeModules.ToastCreator.show("Aufzeichnung gestoppt.", NativeModules.ToastCreator.SHORT);

            // initiate upload
            this.tryUpload();

        } else if (state === STATES.UPLOAD_FAILED) { // discard tour
            // TODO: confirmation dialog for discarding the tour?
            this.setState({
                appState: STATES.SENSORS_STOPPED,
                time: 0,
            }, () => {
                this.dataHandler.clearStore();
                NativeModules.ToastCreator.show("Daten erfolgreich gelöscht.", NativeModules.ToastCreator.LONG);
            });
        }
    }

    /**
     * Handles the logout button click event.
     */
    public logoutButtonHandler() {
        API.logout().then(
            (apiResponse) => {
                if (apiResponse.success) {
                    this.props.navigation.navigate("LogonMask");
                } else {
                    NativeModules.ToastCreator.show("Logout fehlgeschlagen " +
                        "- bitte prüfe deine Internetverbindung",
                        NativeModules.ToastCreator.LONG);
                }
            },
        );
    }

    /**
     * Starts the Timer to update the amount of seconds, which the recording is started.
     */
    public startTimer() {
        this.timerId = BackgroundTimer.setInterval(() => {
            // this will be executed every 1000 ms even when app is the the background
            this.setState({ time: this.state.time + 1 });
        }, 1000);
    }

    /**
     * Stops the background timer responsible for counting the active time.
     */
    public stopTimer() {
        BackgroundTimer.clearInterval(this.timerId);
    }

    /**
     * Set the state to UPLOAD_ACTIVE and tries sending data to the backend.
     * Updates the state according to the result.
     */
    private tryUpload() {
        this.setState({
            appState: STATES.UPLOAD_ACTIVE,
        });

        this.dataHandler.stopRecording().then((apiResponse: IAPIResponse) => {
            if (apiResponse.success) {
                this.setState({
                    appState: STATES.UPLOAD_SUCCESS,
                    time: 0,
                });
            } else {
                NativeModules.ToastCreator.show(apiResponse.message, NativeModules.ToastCreator.LONG);
                this.setState({
                    appState: STATES.UPLOAD_FAILED,
                });
            }
        }).catch((reason: any) => { // some kind of exception
            // TODO: print reason somewhere?
            this.setState({
                appState: STATES.UPLOAD_FAILED,
            });
        });
    }
}

/**
 * Contains styling rules for the displayed components
 */
const styles = StyleSheet.create({
    activity: {
        paddingLeft: 10,
    },
    button: {
        marginTop: 10,
        width: "80%",
    },
    container: {
        alignItems: "center",
        backgroundColor: "rgb(0, 38, 116)",
        flex: 1,
        justifyContent: "flex-start",
        paddingTop: "5%",
    },
    error: {
        color: COLOR_RED,
    },
    logoutButton: {
        marginTop: 60,
        width: "80%",
    },
    status: {
        color: "white",
        fontSize: 20,
        fontWeight: "bold",
        marginBottom: 10,
        marginTop: 10,
        textAlign: "center",
    },
    statusView: {
        flexDirection: "row",
    },
    success: {
        color: COLOR_GREEN,
    },
    welcome: {
        color: "white",
        fontSize: 20,
        fontWeight: "bold",
        marginTop: 10,
        textAlign: "center",
    },
});
