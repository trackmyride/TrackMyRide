const globalAny: any = global;
import {
    NativeModules,
    PermissionsAndroid,
} from "react-native";
import API from "../api/API";

NativeModules.FGServiceBridge = {
    // mock ForegroundService bridge (has to be before import of DataHandler)
    clearData: jest.fn(),
    getData: jest.fn().mockImplementation(() => "[{\"test\": 2}]"),
    pauseRecording: jest.fn(),
    startRecording: jest.fn(),
    stopRecording: jest.fn(),
};
import DataHandler, { STATES } from "./DataHandler";

// SETUP
jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
let dataHandler: DataHandler = new DataHandler();

// Mocks
jest.mock("./BackgroundTimer", () => {
    class BackgroundTimer {
        public setInterval() {
            // do nothing
        }
        public clearInterval() {
            // do nothing
        }
    }
    return BackgroundTimer;
});

NativeModules.ToastCreator = { show: jest.fn() };

// **************** Spies
// Data
const spyStart = jest.spyOn(dataHandler, "startRecording");
const spyPause = jest.spyOn(dataHandler, "pauseRecording");

// Sensors
const checkPermissionFalse = () => {
    PermissionsAndroid.check = jest.fn().mockReturnValueOnce(Promise.resolve(false));
};
const checkPermissionTrue = () => {
    PermissionsAndroid.check = jest.fn().mockReturnValueOnce(Promise.resolve(true));
};
const grantPermission = () => {
    PermissionsAndroid.request = jest.fn().mockReturnValueOnce(
        Promise.resolve(PermissionsAndroid.RESULTS.GRANTED),
    );
};
const rejectPermission = () => {
    PermissionsAndroid.request = jest.fn().mockImplementation(
        () => { Promise.reject({reason: "nope"}); });
};

// **************** Tests
describe("Testing startRecording()", () => {
    let spyStartRecording: jest.SpyInstance;

    beforeEach(() => {
        dataHandler = new DataHandler();
        NativeModules.FGServiceBridge.startRecording.mockClear();
        spyStartRecording = jest.spyOn(NativeModules.FGServiceBridge, "startRecording");
    });

    afterAll(() => {
        dataHandler.stopRecording(); // to cleanup event listeners (they are global!)
    });

    it("correctly initialises when permissions are granted", async () => {
        // prepare
        checkPermissionFalse();
        grantPermission();

        // execute
        const granted = await dataHandler.startRecording();

        // assert
        expect(spyStartRecording).toHaveBeenCalled();
        expect(granted).toBe(true);
    });

    it("correctly initialises when permission is already available", async () => {
        // prepare
        checkPermissionTrue();

        // execute
        const granted = await dataHandler.startRecording();

        // assert
        expect(spyStartRecording).toHaveBeenCalled();
        expect(granted).toBe(true);
    });

    it("fails when permissions are denied", async () => {
        // prepare
        rejectPermission();

        // execute
        const success = await dataHandler.startRecording().then(
            (res: boolean) => {
                return res;
            },
        );

        // assert
        expect(success).toBe(false);
        expect(spyStartRecording).toHaveBeenCalledTimes(0);
    });
});

describe("Testing pauseRecording()", () => {
    let spyPauseRecording: jest.SpyInstance;

    beforeEach(async () => {
        dataHandler = new DataHandler();
        NativeModules.FGServiceBridge.pauseRecording.mockClear();
        spyPauseRecording = jest.spyOn(NativeModules.FGServiceBridge, "pauseRecording");

        grantPermission();
        await dataHandler.startRecording();
    });

    it("stops sensor updates when invoked", () => {
        expect(dataHandler.getSensorState()).toBe(STATES.SENSORS_STARTED);

        // execute
        dataHandler.pauseRecording();

        // assert
        expect(spyPauseRecording).toHaveBeenCalledTimes(1);
        expect(dataHandler.getSensorState()).toBe(STATES.SENSORS_PAUSED);
    });

    it("stays paused when invoked twice", () => {
        expect(dataHandler.getSensorState()).toBe(STATES.SENSORS_STARTED);

        dataHandler.pauseRecording();
        expect(dataHandler.getSensorState()).toBe(STATES.SENSORS_PAUSED);

        dataHandler.pauseRecording();
        expect(dataHandler.getSensorState()).toBe(STATES.SENSORS_PAUSED);

        expect(spyPauseRecording).toHaveBeenCalledTimes(1); // call to native module should only have been invoked once
    });
});

describe("Testing stopRecording()", () => {
    let spyStopRecording: jest.SpyInstance;
    let spySendDataToServer: jest.SpyInstance;

    beforeEach(async () => {
        dataHandler = new DataHandler();
        NativeModules.FGServiceBridge.stopRecording.mockClear();
        spyStopRecording = jest.spyOn(NativeModules.FGServiceBridge, "stopRecording");
        spySendDataToServer = jest.spyOn(dataHandler, "sendDataToServer");
        grantPermission();
        await dataHandler.startRecording();
    });

    it("correctly stops sensor updates", async () => {
        // prepare
        globalAny.fetch = jest.fn().mockImplementation(
            () => Promise.resolve({status: 201}));

        // execute
        const success = await dataHandler.stopRecording();

        // assert
        expect(spySendDataToServer).toHaveBeenCalled();
        expect(spyStopRecording).toHaveBeenCalled();
        expect(dataHandler.getSensorState()).toBe(STATES.SENSORS_STOPPED);
    });
});

// Tests sendDataToServer()
describe("Testing sendDataToServer()", () => {
    it("Gets data from storage and sends them server", async () => {
        NativeModules.FGServiceBridge.getData.mockClear();
        const spyGetData = jest.spyOn(NativeModules.FGServiceBridge, "getData");
        const spySend = jest.spyOn(API, "sendTour");

        await dataHandler.sendDataToServer();
        expect(spyGetData).toHaveBeenCalled();
        expect(spySend).toHaveBeenCalled();
    });

    it("correctly fails on failed upload", async () => {
        const answer = {success: false};
        // prepare
        API.sendTour = jest.fn().mockImplementation(
            () => Promise.resolve(answer));

        // execute
        const success = await dataHandler.sendDataToServer();

        // assert
        expect(success).toEqual(answer);
    });

    it("correctly fails on promise rejection", async () => {
        // prepare
        API.sendTour = jest.fn().mockImplementation(
            () => Promise.reject({success: false}));

        // execute
        const success = await dataHandler.sendDataToServer();

        // assert
        expect(success).toEqual({message: "sendTour failed with promise rejection", status: -1, success: false});
    });
});

describe("Testing getSensorState()", () => {
    it("returns the correct state", () => {
        // prepare
        dataHandler = new DataHandler();

        // assert
        expect(dataHandler.getSensorState()).toBe(STATES.SENSORS_STOPPED);
    });
});

describe("Testing clearStore()", () => {
    it("calls the native function", () => {
        // prepare
        NativeModules.FGServiceBridge.clearData.mockClear();
        const spyClearData = jest.spyOn(NativeModules.FGServiceBridge, "clearData");

        // execute
        dataHandler.clearStore();

        // assert
        expect(spyClearData).toHaveBeenCalled();
    });
});
