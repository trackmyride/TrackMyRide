import {
  DeviceEventEmitter,
  NativeAppEventEmitter,
  NativeEventEmitter,
  NativeModules,
  Platform,
} from "react-native";

const Emitter = new NativeEventEmitter(NativeModules.RNBackgroundTimer);

class BackgroundTimer {

  private uniqueId: any;
  private callbacks: any;
  private backgroundListener: any;
  private backgroundTimer: any;

  constructor() {
    this.uniqueId = 0;
    this.callbacks = {};

    Emitter.addListener("backgroundTimer.timeout", (id) => {
      if (this.callbacks[id]) {
        const callback = this.callbacks[id].callback;
        if (!this.callbacks[id].interval) {
          delete this.callbacks[id];
        } else {
          NativeModules.RNBackgroundTimer.setTimeout(id, this.callbacks[id].timeout);
        }
        callback();
      }
    });
  }

  /**
   * Start background execution at delay given.
   * @param  {any=0} delay the interval at which tasks should be run
   */
  public start(delay: any = 0) {
    return NativeModules.RNBackgroundTimer.start(delay);
  }
  /**
   * Stop background execution ob tasks.
   */
  public stop() {
    return NativeModules.RNBackgroundTimer.stop();
  }

  /**
   * Initiates the execution of a background task at a given delay/interval.
   * @param  {any} callback The callback to execute.
   * @param  {any} delay The delay between consecutive calls.
   */
  public runBackgroundTimer = (callback: any, delay: any) => {
    const EventEmitter = Platform.select({
      android: () => DeviceEventEmitter,
      ios: () => NativeAppEventEmitter,
    })();
    this.start(0);
    this.backgroundListener = EventEmitter.addListener("backgroundTimer", () => {
      this.backgroundListener.remove();
      this.backgroundClockMethod(callback, delay);
    });
  }
  /**
   * Runs a callback at a given delay.
   * @param  {any} callback The callback to be executed.
   * @param  {any} delay The delay between consecutive calls.
   */
  public backgroundClockMethod(callback: any, delay: any) {
    this.backgroundTimer = setTimeout(() => {
      callback();
      this.backgroundClockMethod(callback, delay);
    }, delay);
  }
  /**
   * Globally stops the execution of the timer.
   */
  public stopBackgroundTimer = () => {
    this.stop();
    clearTimeout(this.backgroundTimer);
  }

  // New API, allowing for multiple timers

  /**
   * Initiates a new background task using timeouts.
   * @param  {any} callback The callback to execute.
   * @param  {any} timeout The timeout at which the callback is called.
   * @returns {number} ID of the new timeout
   */
  public setTimeout(callback: any, timeout: any): number {
    const timeoutId = ++this.uniqueId;
    this.callbacks[timeoutId] = {
      callback,
      interval: false,
      timeout,
    };
    NativeModules.RNBackgroundTimer.setTimeout(timeoutId, timeout);
    return timeoutId;
  }

  /**
   * Cancels a background job callback.
   * @param  {any} timeoutId The id/name of the background job to cancel.
   */
  public clearTimeout(timeoutId: any) {
    if (this.callbacks[timeoutId]) {
      delete this.callbacks[timeoutId];
    }
  }

  /**
   * Initiates a new background task using intervals.
   * @param  {any} callback The callback to execute.
   * @param  {any} timeout The interval at which the callback is called.
   * @returns {number} ID of the new timeout
   */
  public setInterval(callback: any, timeout: any): number {
    const intervalId = ++this.uniqueId;
    this.callbacks[intervalId] = {
      callback,
      interval: true,
      timeout,
    };
    NativeModules.RNBackgroundTimer.setTimeout(intervalId, timeout);
    return intervalId;
  }
  /**
   * Drops a background job callback out of the active list.
   * @param  {any} intervalId The id/name of the background task to stop.
   */
  public clearInterval(intervalId: any) {
    if (this.callbacks[intervalId]) {
      delete this.callbacks[intervalId];
    }
  }
}

export default new BackgroundTimer();
