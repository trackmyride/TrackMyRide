import {
    NativeModules,
    PermissionsAndroid,
} from "react-native";
import API, { IAPIResponse } from "../api/API";

const { FGServiceBridge } = NativeModules;

export interface ISensorData {
    acceleration: {x: number; y: number; z: number};
    accuracy: number;
    altitude: number;
    bearing: number;
    gyroscope: {x: number; y: number; z: number};
    heading: number;
    longitude: number;
    latitude: number;
    orientation: {azimuth: number; pitch: number; roll: number};
    time: string;
}

export const STATES = {
    SENSORS_PAUSED: 0,
    SENSORS_STARTED: 1,
    SENSORS_STOPPED: 2, // INITIAL
    UPLOAD_ACTIVE: 3,
    UPLOAD_FAILED: 4,
    UPLOAD_PENDING: 5,
    UPLOAD_SUCCESS: 6,
};

/*
 * Contains all functionality to save and send data.
 */
export default class DataHandler {
    private sensorState: number = STATES.SENSORS_STOPPED;

    private data: ISensorData = {
        acceleration: {x: 0, y: 0, z: 0},
        accuracy: 0,
        altitude: 0,
        bearing: 0,
        gyroscope: {x: 0, y: 0, z: 0},
        heading: 0,
        latitude: 0,
        longitude: 0,
        orientation: {azimuth: 0, pitch: 0, roll: 0},
        time: "0",
    };

    /**
     * Returns the current sensorState.
     * @returns {number} indicating the state the sensor is in
     */
    public getSensorState(): number {
        return this.sensorState;
    }

    /**
     * Starts the recording of data. --> async
     * @returns {Promise<boolean>} indicating whether location access has been granted
     */
    public async startRecording(): Promise<boolean> {
        let granted = false;

        try {
            // check whether we already got the permission
            granted = await PermissionsAndroid.check(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            );

            // if the permission was not yet given, ask for it
            if (!granted) {
                const result = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
                        message: "TrackMyRide benötigt Zugriff auf deine Position " +
                                "um deine gefahrene Tour aufzeichnen zu können.",
                        title: "TrackMyRide benötigt Zugriff auf Position",
                    },
                );

                // did they choose to grant?
                granted = result === PermissionsAndroid.RESULTS.GRANTED;
            }

            // are we allowed to use GPS?
            if (granted) {
                // if so, start recording
                FGServiceBridge.startRecording();

                this.sensorState = STATES.SENSORS_STARTED;
            }
        } catch (err) {
            granted = false;
        }

        return Promise.resolve(granted);
    }

    /**
     * Pauses the recording of data.
     */
    public pauseRecording() {
        if (this.sensorState === STATES.SENSORS_STARTED) {
            FGServiceBridge.pauseRecording();

            this.sensorState = STATES.SENSORS_PAUSED;
        }
    }

    /**
     * Stops the recording of data.
     * @returns {Promise<boolean>} containing the upload to the server
     */
    public async stopRecording(): Promise<IAPIResponse> {
        if (this.sensorState === STATES.SENSORS_STARTED) {
            await FGServiceBridge.stopRecording(); // TODO: somehow handle promise rejection?

            this.sensorState = STATES.SENSORS_STOPPED;
        }

        return this.sendDataToServer();
    }

    /**
     * Extracts the data from AsyncStorage and uploads it to the backend server.
     * @returns {Promise<IAPIResponse>} indicating the success of the upload
     */
    public async sendDataToServer(): Promise<IAPIResponse> { // TODO: do we have to do this in the background?
        const data = await FGServiceBridge.getData();
        return API.sendTour({log : JSON.parse(data)}).then((apiResponse) => {
            if (apiResponse.success) {
                // clear storage when data was successfully uploaded (and only then!)
                FGServiceBridge.clearData();
            }
            return apiResponse;
        }).catch(() => {
            return {
                message: "sendTour failed with promise rejection",
                status: -1,
                success: false,
            };
        });
    }

    /**
     * Clears the AsyncStorage from all data.
     */
    public clearStore() {
        FGServiceBridge.clearData();
    }
}
