import * as React from "react";
import {
    RegisteredStyle,
    StyleSheet,
    Text,
    TouchableNativeFeedback,
    View,
} from "react-native";
import EntypoIcon from "react-native-vector-icons/Entypo";

/**
 * A button providing the possibility to include an optional icon.
 */
export default class ExtendedButton extends React.Component<{
    entypoIcon?: string,
    iconColor?: string,
    iconSize?: number,
    disabled?: boolean,
    onPress: () => any,
    style?: RegisteredStyle<any>,
    title: string,
}> {

    /**
     * Renders the component.
     * @returns {JSX.Element} The rendered component.
     */
    public render(): JSX.Element {
        const {
            iconColor,
            iconSize,
            entypoIcon,
            disabled,
            onPress,
            style,
            title,
        } = this.props;

        const buttonStyles: any = [styles.button];
        const textStyles: any = [styles.text];
        const accessibilityTraits = disabled ? "disabled" : "button";

        let color = iconColor ? iconColor : "white";
        const size = iconSize ? iconSize : 35;

        if (style) {
            buttonStyles.push(style);
        }

        if (entypoIcon) {
            buttonStyles.push(styles.container);
            textStyles.push({
                marginLeft: -(size + 10),
            });
        }

        if (disabled) {
            buttonStyles.push(styles.buttonDisabled);
            textStyles.push(styles.textDisabled);
            color = "darkgray";
        }

        return (
            <TouchableNativeFeedback
                accessibilityComponentType="button"
                accessibilityTraits={accessibilityTraits}
                disabled={disabled}
                onPress={onPress}
            >
                <View style={buttonStyles}>
                    {entypoIcon && <EntypoIcon name={entypoIcon} size={size} color={color} style={styles.logoImage} />}
                    <Text style={textStyles}>{title}</Text>
                </View>
            </TouchableNativeFeedback>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: "#0069d9",
        borderRadius: 2,
        elevation: 4,
        height: 40,
    },
    buttonDisabled: {
        backgroundColor: "#dfdfdf",
        elevation: 0,
    },
    container: {
        alignItems: "center",
        flexDirection: "row",
        height: 40,
    },
    logoImage: {
        margin: 5,
    },
    text: {
        color: "white",
        fontSize: 18,
        fontWeight: "500",
        padding: 8,
        textAlign: "center",
        width: "100%",
    },
    textDisabled: {
        color: "#a1a1a1",
    },
});
