import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import * as React from "react";
import TimeViewer from "./TimeViewer";

// SETUP
jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
configure({ adapter: new Adapter() });
const spyUpdateTimeState = jest.spyOn(TimeViewer.prototype, "updateTimeState");

// Tests rendering of App
describe("Testing of TimeViwer redering", () => {
  it("renders correctly", () => {
    const wrapper = shallow( <TimeViewer time={5} /> );
    expect(spyUpdateTimeState).toHaveBeenCalled();
    expect(wrapper).toMatchSnapshot();
    wrapper.setProps({ time: 60 });
    expect(spyUpdateTimeState).toHaveBeenCalled();
    expect(wrapper).toMatchSnapshot();
    wrapper.setProps({ time: 3600 });
    expect(spyUpdateTimeState).toHaveBeenCalled();
    expect(wrapper).toMatchSnapshot();
    wrapper.setProps({ time: 56789 });
    expect(spyUpdateTimeState).toHaveBeenCalled();
    expect(wrapper).toMatchSnapshot();
    wrapper.setProps({ time: 35 });
    expect(spyUpdateTimeState).toHaveBeenCalled();
    expect(wrapper).toMatchSnapshot();
  });
});
