import * as React from "react";
import {
  StyleSheet,
  Text,
  View,
} from "react-native";

/**
 * Shows nicely formatted time as a single component.
 */
export default class TimeViewer extends React.Component<any, any> {

  constructor(props: any) {
    super(props);
    this.state = {
      h: 0,
      min: 0,
      sec: 0,
    };
  }

  /**
   * Renders elements of the component.
   * @returns {JSX.Element} The rendered component.
   */
  public render(): JSX.Element {
    return (
      <View style={styles.statusView}>
        <Text style={styles.status}>
          {this.state.h}:{this.state.min}:{this.state.sec}
        </Text>
      </View>
    );
  }

  /**
   * Is invoked immediately after a component is mounted.
   * Used for initialisations that requires DOM nodes.
   */
  public componentDidMount() {
    this.updateTimeState();
  }

  /**
   * Is invoked before a mounted component receives new props.
   * Used for updating the state in response to prop changes.
   */
  public componentWillReceiveProps() {
    this.updateTimeState();
  }

  /**
   * Updates the component state regarding the current props.
   */
  public updateTimeState() {
    let val: number = this.props.time;
    let hours: number = 0;
    let minutes: number = 0;
    let seconds: number = 0;

    if (val / 3600 > 0) {
      hours = Math.floor(val / 3600);
    }
    val = val - hours * 3600;
    if (val / 60 > 0) {
      minutes = Math.floor(val / 60);
    }
    val = val - minutes * 60;
    seconds = val;

    this.setState({
      h: this.timeToString(hours),
      min: this.timeToString(minutes),
      sec: this.timeToString(seconds),
    });
  }

  /**
   * Converts a time value in seconds to a string with two digits.
   * @param {number} val The time in seconds to convert to a nicely readable string.
   * @returns string The converted time string.
   */
  protected timeToString(val: number): string {
    let str: string = "";
    if (val < 10) {
      str = "0" + val.toString();
    } else {
      str = val.toString();
    }
    return str;
  }
}

/**
 * Contains styling rules for the displayed components
 */
const styles = StyleSheet.create({
  status: {
    color: "white",
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 20,
    marginTop: 10,
    textAlign: "center",
  },
  statusView: {
    marginBottom: 20,
    marginTop: 10,
  },
});
