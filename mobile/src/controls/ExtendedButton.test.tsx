const globalAny: any = global;
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import * as React from "react";
import {StyleSheet } from "react-native";

import ExtendedButton from "./ExtendedButton";

// Note: test renderer must be required after react-native.
import renderer from "react-test-renderer";

// setup
jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
configure({ adapter: new Adapter() });

// spies

// mocks

// tests rendering of App
describe("Testing of LogonMask redering", () => {
    it("renders correctly without style", () => {
        const tree = renderer.create((
            <ExtendedButton
                onPress={jest.fn()}
                title="test"
            />
        ));
    });

    it("renders correctly with style", () => {
        const style = StyleSheet.create({
            button: {
                width: "100%",
            },
        });
        const tree = renderer.create((
            <ExtendedButton
                onPress={jest.fn()}
                style={style.button}
                title="test"
            />
        ));
    });
});
