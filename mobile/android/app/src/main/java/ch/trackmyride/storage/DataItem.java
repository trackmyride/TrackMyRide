package ch.trackmyride.storage;

import android.location.Location;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DataItem {

    private Location location;
    private float[] gyroscope;
    private float[] acceleration;
    private float[] orientation;

    public DataItem() {
        this.gyroscope = new float[3];
        this.acceleration = new float[3];
        this.orientation = new float[3];

    }

    @Override
    public String toString() {
        // start JSON string
        String out = "{";

        // JSON format data
        out += "\"accuracy\":" + location.getAccuracy() + ",";
        out += "\"altitude\":" + location.getAltitude() + ",";
        out += "\"bearing\":" + location.getBearing() + ",";
        out += "\"longitude\":" + location.getLongitude() + ",";
        out += "\"latitude\":" + location.getLatitude() + ",";
        out += "\"time\":\"" + location.getTime() + "\","; // this gets sent as a string!
        out += "\"heading\":" + "0" + ","; // TODO: get real data
        out += "\"acceleration\":"
                + "{\"x\":" + this.acceleration[0]
                + ",\"y\":" + this.acceleration[1]
                + ",\"z\":" + this.acceleration[2] + "},";
        out += "\"gyroscope\":"
                + "{\"x\":" + this.gyroscope[0]
                + ",\"y\":" + this.gyroscope[1]
                + ",\"z\":" + this.gyroscope[2] + "},";
        out += "\"orientation\":"
                + "{\"azimuth\":" + this.orientation[0]
                + ",\"pitch\":" + this.orientation[1]
                + ",\"roll\":" + this.orientation[2] + "}";

        // Close JSON string
        out += "}";
        return out;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setGyroscope(float[] gyroscope) {
        this.gyroscope = gyroscope;
    }

    public void setAcceleration(float[] acceleration) {
        this.acceleration = acceleration;
    }

    public void setOrientation(float[] orientation) {
        this.orientation = orientation;
    }
}
