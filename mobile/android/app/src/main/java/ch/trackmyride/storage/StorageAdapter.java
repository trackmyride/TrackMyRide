package ch.trackmyride.storage;

import java.util.ArrayList;
import java.util.List;

public class StorageAdapter implements IStorageAdapter {
    private static final StorageAdapter ourInstance = new StorageAdapter();
    private List<DataItem> storage;

    private StorageAdapter() {
        this.storage = new ArrayList<>();
    }

    public static StorageAdapter get() {
        return ourInstance;
    }

    @Override
    public void add(DataItem data) {
        storage.add(data);
    }

    @Override
    public String getAll() {
        String dataString = "[";

        for (DataItem item: storage) {
            dataString += item.toString() + ",";
        }

        // strip off last , and add closing bracket
        dataString = dataString.substring(0, dataString.length() - 1);
        dataString += "]";

        return dataString;
    }

    @Override
    public void clear() {
        storage.clear();
    }
}
