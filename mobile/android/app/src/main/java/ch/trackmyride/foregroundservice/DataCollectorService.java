package ch.trackmyride.foregroundservice;

import ch.trackmyride.R;
import ch.trackmyride.MainActivity;
import ch.trackmyride.storage.DataItem;
import ch.trackmyride.storage.StorageAdapter;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.app.Notification;
import android.annotation.TargetApi;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.os.IBinder;
import android.os.Build;
import android.content.Intent;
import android.app.PendingIntent;
import android.graphics.BitmapFactory;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
/*import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;*/

public class DataCollectorService extends Service implements SensorEventListener {
    public static final String APP_NAME = "TrackMyRide";
    public static final String REACT_CLASS = "DataCollectorService";
    private final IBinder mBinder = new LocalBinder();
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 1000;

    public static final String FOREGROUND = "ch.trackmyride.foregroundservice.DataCollectorService";
    private static int NOTIFICATION_ID = 3313;

    private StorageAdapter storageAdapter = StorageAdapter.get();
    private DataItem currentData = new DataItem();

    // Location handling and service settings
    private NotificationManager mNotificationManager;
    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private Handler mServiceHandler;

    // sensor listeners
    private Arguments mArguments;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private Sensor mGyroscope;
    private Sensor mMagnetometer; // used in conjunction with accelerometer for orientation

    private long gyroscopeUpdateTime = 0;
    private long accelerometerUpdateTime = 0;
    private long orientationUpdateTime = 0;

    private boolean accelerometerRegistered = false;
    private boolean orientationRegistered = false;
    private boolean gyroscopeRegistered = false;

    private float[] mGravity; // used for orientation
    private float[] mGeomagnetic; // used for orientation

    /**
     * Called on creation of the service instance
     */
    @Override
    @TargetApi(Build.VERSION_CODES.M)
    public void onCreate() {
        Log.d(REACT_CLASS, "onCreate");

        // initialise sensors
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        mMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        // set location callback
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                onNewLocation(locationResult.getLastLocation());
            }
        };

        // initialise location request
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // start thread
        HandlerThread handlerThread = new HandlerThread(REACT_CLASS);
        handlerThread.start();
        mServiceHandler = new Handler(handlerThread.getLooper());
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // Android O requires a Notification Channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);

            // Create the channel for the notification
            NotificationChannel mChannel = new NotificationChannel("Datensammlung", name, NotificationManager.IMPORTANCE_DEFAULT);

            // Set the Notification Channel for the Notification Manager.
            mNotificationManager.createNotificationChannel(mChannel);
        }

        super.onCreate();
    }

    /**
     * Called on destruction of the service instance
     */
    @Override
    public void onDestroy() {
        Log.d(REACT_CLASS, "onDestroy");
        mServiceHandler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }

    /**
     * Called when start command is received.
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(REACT_CLASS, "onStartCommand, calling startForeground");
        return START_STICKY; // TODO: is START_NON_STICKY in example
    }

    /**
     * Called when a client comes to the foreground and binds with this service.
     * The service should cease to be a foreground service when that happens.
     * @param intent
     * @return
     */
    @Override
    public IBinder onBind(Intent intent) {
        Log.i(REACT_CLASS, "in onBind()");
        stopForeground(true);
        return mBinder;
    }

    /**
     * Called when a client returns to the foreground and binds once again with this service.
     * The service should cease to be a foreground service when that happens.
     * @param intent
     */
    @Override
    public void onRebind(Intent intent) {
        Log.i(REACT_CLASS, "in onRebind()");
        stopForeground(true);
        super.onRebind(intent);
    }

    /**
     * Called when the last client unbinds from this service.
     * Results in switching to foreground operation.
     * @param intent
     * @return
     */
    @Override
    public boolean onUnbind(Intent intent) {
        Log.i(REACT_CLASS, "Last client unbound from service");

        if (Utils.requestingLocationUpdates(this)) {
            Log.i(REACT_CLASS, "Starting foreground service");

            /*
            // TODO(developer). If targeting O, use the following code.
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O) {
                mNotificationManager.startServiceInForeground(new Intent(this,
                        DataCollectorService.class), NOTIFICATION_ID, getCompatNotification());
            } else {
                startForeground(NOTIFICATION_ID, getCompatNotification());
            }*/
            startForeground(NOTIFICATION_ID, getCompatNotification());
        }
        return true; // Ensures onRebind() is called when a client re-binds.
    }

    /**
     * Callback function executed on location updates.
     * @param location The new location object by the sensor
     */
    private synchronized void onNewLocation(Location location) {
        Log.i(REACT_CLASS, "New location: " + location);
        currentData.setLocation(location);
        storageAdapter.add(currentData);
        currentData = new DataItem();
    }

    /**
     * Listens to changes to any sensor present on the device.
     * @param sensorEvent The event containing the sensor which led to the event.
     */
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor sensor = sensorEvent.sensor;
        WritableMap map = mArguments.createMap();

        // handle acceleration
        if (sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            mGravity = sensorEvent.values; // update instance variable

            long curTime = System.currentTimeMillis();
            if ((curTime - accelerometerUpdateTime) > UPDATE_INTERVAL_IN_MILLISECONDS) {
                Log.d(REACT_CLASS, "Got accelerometer update");
                currentData.setAcceleration(sensorEvent.values);

                accelerometerUpdateTime = curTime;
            }
        }

        // handle gyroscope
        if (sensor.getType() == Sensor.TYPE_GYROSCOPE) {
            long curTime = System.currentTimeMillis();
            if ((curTime - gyroscopeUpdateTime) > UPDATE_INTERVAL_IN_MILLISECONDS) {
                Log.d(REACT_CLASS, "Got gyroscope update");
                currentData.setGyroscope(sensorEvent.values);

                gyroscopeUpdateTime = curTime;
            }
        }

        // handle orientation (combination magnetic sensor and accelerometer)
        if (sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            mGeomagnetic = sensorEvent.values;
        }

        if (mGravity != null && mGeomagnetic != null) {
            float R[] = new float[9];
            float I[] = new float[9];
            boolean success = mSensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
            if (success) {
                long curTime = System.currentTimeMillis();

                if((curTime - orientationUpdateTime) > UPDATE_INTERVAL_IN_MILLISECONDS) {
                    Log.d(REACT_CLASS, "Calculating orientation update");
                    float orientation[] = new float[3];
                    mSensorManager.getOrientation(R, orientation);

                    float heading = (float)((Math.toDegrees(orientation[0])) % 360.0f);
                    float pitch = (float)((Math.toDegrees(orientation[1])) % 360.0f);
                    float roll = (float)((Math.toDegrees(orientation[2])) % 360.0f);

                    if (heading < 0) {
                        heading = 360 - (0 - heading);
                    }

                    if (pitch < 0) {
                        pitch = 360 - (0 - pitch);
                    }

                    if (roll < 0) {
                        roll = 360 - (0 - roll);
                    }

                    currentData.setOrientation(new float[] {heading, pitch, roll});

                    orientationUpdateTime = curTime;
                }
            }
        }
    }

    /**
     * Handles accuracy changes of hardware sensors.
     * @param sensor The sensor that changed
     * @param accuracy The current accuracy
     */
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    /**
     * Tells fused location to enable location change callbacks.
     */
    public void startSensorUpdates() {
        Log.d(REACT_CLASS, "Requesting location updates");
        Utils.setRequestingLocationUpdates(this, true);
        startService(new Intent(getApplicationContext(), DataCollectorService.class));
        try {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
            registerSensorListener();

        } catch (SecurityException unlikely) {
            Utils.setRequestingLocationUpdates(this, false);
            Log.e(REACT_CLASS, "Lost location permission. Could not request updates. " + unlikely);
        }
    }

    /**
     * Tells fused location to disable location change callbacks.
     */
    public void stopSensorUpdates() {
        Log.d(REACT_CLASS, "Removing location updates");
        try {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
            unregisterSensorListener();

            Utils.setRequestingLocationUpdates(this, false);
            stopSelf();

        } catch (SecurityException unlikely) {
            Utils.setRequestingLocationUpdates(this, true);
            Log.e(REACT_CLASS, "Lost location permission. Could not remove updates. " + unlikely);
        }
    }

    /**
     * Registers ourselves as listener on supported sensors.
     */
    private void registerSensorListener() {
        if (mAccelerometer != null && !accelerometerRegistered) {
            mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
            accelerometerRegistered = true;
        }
        if (mMagnetometer != null && !orientationRegistered) {
            mSensorManager.registerListener(this, mMagnetometer, SensorManager.SENSOR_DELAY_UI);
            orientationRegistered = true;
        }
        if (mGyroscope != null && !gyroscopeRegistered) {
            mSensorManager.registerListener(this, mGyroscope, SensorManager.SENSOR_DELAY_FASTEST);
            gyroscopeRegistered = true;
        }
    }

    /**
     * Unregisters ourselves as listener on supported sensors.
     */
    private void unregisterSensorListener() {
        if (accelerometerRegistered || orientationRegistered || gyroscopeRegistered) {
            mSensorManager.unregisterListener(this);
        }

        accelerometerRegistered = false;
        orientationRegistered = false;
        gyroscopeRegistered = false;
    }
    /**
     * Generate a basic notification object.
     * @return The new notification
     */
    private Notification getCompatNotification() {
        Log.d(REACT_CLASS, "getCompatNotification");
        String str = "Greift im Hintergrund auf Sensordaten zu.";

        Intent startIntent = new Intent(this, MainActivity.class);
        startIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, startIntent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
          .setSmallIcon(R.drawable.app_icon)
          .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
          .setContentTitle(APP_NAME)
          .setContentIntent(contentIntent)
          .setContentText(str)
          .setTicker(str)
          .setPriority(NotificationCompat.PRIORITY_LOW)
          .setWhen(System.currentTimeMillis());

        return builder.build();
    }

    /**
     * Tiny binder instance to allow simple binding to this service.
     */
    public class LocalBinder extends Binder {
        DataCollectorService getService() {
            return DataCollectorService.this;
        }
    }

}
