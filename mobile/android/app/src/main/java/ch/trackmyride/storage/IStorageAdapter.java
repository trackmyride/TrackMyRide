package ch.trackmyride.storage;

public interface IStorageAdapter {
    void add(DataItem data);

    String getAll();

    void clear();
}
