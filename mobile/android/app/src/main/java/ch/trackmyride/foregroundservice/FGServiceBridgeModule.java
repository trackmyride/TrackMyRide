package ch.trackmyride.foregroundservice;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.util.HashMap;
import java.util.Map;
import android.util.Log;
import com.facebook.react.bridge.Promise;

import android.content.Intent;

import ch.trackmyride.storage.StorageAdapter;

public class FGServiceBridgeModule extends ReactContextBaseJavaModule {
    public static final String REACT_CLASS = "FGServiceBridge";
    private static ReactApplicationContext reactContext = null;
    private StorageAdapter storageAdapter = StorageAdapter.get();

    // connection to service
    private boolean mBound = false;
    private DataCollectorService mService = null;
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            DataCollectorService.LocalBinder binder = (DataCollectorService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };

    public FGServiceBridgeModule(ReactApplicationContext context) {
        super(context);
        this.reactContext = context;
        bindService(this.reactContext);
    }

    private void bindService(ReactApplicationContext context) {
        reactContext.bindService(new Intent(context, DataCollectorService.class),
                    mServiceConnection,
                    Context.BIND_AUTO_CREATE);
    }

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();
        return constants;
    }

    @ReactMethod 
    public void startRecording(Promise promise) {
        Log.d(REACT_CLASS, "startRecording");
        try {
            storageAdapter.clear(); // clear storage

            // start service
            mService.startSensorUpdates();
            reactContext.unbindService(mServiceConnection); // start foreground service

            // resolve
            promise.resolve(true);

        } catch (Exception e) {
            Log.d(REACT_CLASS, "startRecording failed!");
            promise.reject(e);
        }
    }

    @ReactMethod
    public void pauseRecording(Promise promise) {
        Log.d(REACT_CLASS, "pauseRecording");

        try {
            // stop service
            mService.stopSensorUpdates();
            bindService(this.reactContext); // stop foreground service

            // resolve
            promise.resolve(true);

        } catch (Exception e) {
            Log.d(REACT_CLASS, "pauseRecording failed!" + e.toString());
            promise.reject(e);
        }
    }

    @ReactMethod
    public void stopRecording(Promise promise) {
        Log.d(REACT_CLASS, "stopRecording");

        try {
            // stop service
            if (Utils.requestingLocationUpdates(this.reactContext)) {
                mService.stopSensorUpdates();
                bindService(this.reactContext); // stop foreground service
            }

            // resolve
            promise.resolve(true);

        } catch (Exception e) {
            Log.d(REACT_CLASS, "stopRecording failed!" + e.toString());
            promise.reject(e);
        }
    }

    @ReactMethod
    public void getData(Promise promise) {
        String data = storageAdapter.getAll();
        Log.d(REACT_CLASS, "Data: " + data);
        promise.resolve(data);
    }

    @ReactMethod
    public void clearData() {
        storageAdapter.clear();
    }

    private static void emitDeviceEvent(String eventName, @Nullable WritableMap eventData) {
        reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(eventName, eventData);
    }
}
