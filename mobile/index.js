import { AppRegistry } from 'react-native';
import { SwitchNavigator } from 'react-navigation';
import App from './lib/components/App'
import Loader from './lib/components/Loader'
import LogonMask from './lib/components/LogonMask';

const nav = SwitchNavigator({
        App: App,
        Loader: Loader,
        LogonMask: LogonMask,
    },
    {
        initialRouteName: 'Loader',
    });

AppRegistry.registerComponent('trackmyride', () => nav);