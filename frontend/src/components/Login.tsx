import * as React from 'react';
import { connect, Dispatch } from 'react-redux';
import '../css/App.css';
import { Button, FormGroup, Input, Container, Jumbotron } from 'reactstrap';
import { performLogin } from '../store/user/actions';
import { LoginRequestParameters } from '../store/user/types.intf';
import { RouteComponentProps } from 'react-router-dom';
import { ApplicationState } from '../store/stateTypes.intf';
import { CSSProperties } from 'react';

interface RouterProps extends RouteComponentProps<any>, React.Props<any> {
}

export interface ConnectedDispatch {
  dispatchPerformLogin: (loginRequestParameters: LoginRequestParameters) => void;
}

const mapDispatchToProps = (dispatch: Dispatch<LoginRequestParameters>): ConnectedDispatch => ({
  dispatchPerformLogin:
    (loginRequestParameters: LoginRequestParameters) => dispatch(performLogin(loginRequestParameters))
});

const mapStateToProps = (state: ApplicationState) => (state);

export class Login extends React.Component<ApplicationState & ConnectedDispatch & RouterProps, {}> {
  // uncontrolled components:
  inputUsername: HTMLInputElement | null;
  inputPassword: HTMLInputElement | null;

  constructor(props: ApplicationState & ConnectedDispatch & RouterProps) {
    super(props);
    this.onLoginSubmit = this.onLoginSubmit.bind(this);
    this.onRegisterClick = this.onRegisterClick.bind(this);
  }

  onLoginSubmit() {

    let username = this.inputUsername!.value;
    let password = this.inputPassword!.value;

    let loginRequestParameters: LoginRequestParameters = {
      username: username,
      password: password,
    };
    this.props.dispatchPerformLogin(loginRequestParameters);
  }
  componentDidUpdate() {
    console.log('update');
    if (this.props.user.isLoggedIn) {
    this.props.history.push('/MyTours');
    }
  }

  onRegisterClick() {
    var routerlink = '/Register';
    this.props.history.push(routerlink);
  }

  render() {

    return (
      <div>
          <Jumbotron className="BetterJumbotron">
            <div>
              <h2 className="display-5">Login</h2>
              <p className="text">
                {'Logge dich ein um deine aufgezeichneten Touren zu sehen, sie zu bearbeiten und zu veröffentlichen. ' +
                  ' Wenn du eingeloggt bist, kannst du ausserdem andere Touren bewerten und kommentieren.'}
              </p>
              <hr className="my-2" />
            </div>

          </Jumbotron>
        <Container style={ContainerStyle}>
          <div style={CenterWrapper}>
                      <img src={DefaultImage} style={{ ...ImageStyle }} alt="Image preview..." />
          </div>
          <form className="form">
            <FormGroup>
                    <Input style={InputStyle} type="text" innerRef={(input) => this.inputUsername = input} placeholder="Username" />
                     <Input style={InputStyle} type="password" innerRef={(input) => this.inputPassword = input} placeholder="Password"/>
            </FormGroup>
          </form>
            <div style={LoginButtonsStyle}>
              <Button color="primary" onClick={this.onLoginSubmit} style={{backgroundColor: '#012674', margin: '10px' }} value="Login">Login</Button>
              <Button color="primary" style={{backgroundColor: '#012674', margin: '10px' }}onClick={this.onRegisterClick}>Registrierung</Button>
            </div>
        </Container>
      </div>
    );
  }
}

const ContainerStyle: CSSProperties = {
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  'backgroundColor': 'white',
  'border': '1px solid #d8d8d8',
  paddingBottom: '10px',
 
};

// sets all Label to use the size they can. (otherwise they are not centered)
const InputStyle: CSSProperties = {
  width: '100%',
  maxWidth: '700px',
  margin: '10px auto 10px auto'
};

const LoginButtonsStyle: React.CSSProperties = {
  margin: 'auto',
};

// sets the size of the image, makes it look round and ensure there is always something in the circle, no matter what resolution is uploaded
const ImageStyle: CSSProperties = {
  height: '25%',
  width: '25%',
  objectFit: 'cover',
  borderRadius: '50%'
};

const CenterWrapper: CSSProperties = {
  textAlign: 'center',
};

const DefaultImage = 
  `data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAUEBAUEAwUFBAUGBgUGCA4JCAcHCBEMDQoOFBEVFBMRExMWGB8bFhceFxMTGyUcHiAhIyMjF` +
  `RomKSYiKR8iIyL/2wBDAQYGBggHCBAJCRAiFhMWIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiL/wgARCADIAMgDASIAAhEBAxEB/8QAG` +
  `wABAAIDAQEAAAAAAAAAAAAAAAMFAQQGAgf/xAAUAQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIQAxAAAAH6AAAAAAAAAAAAAAAAAAirS0joMF/4ox0M3M75btfYAAAAAAAGtsc0S` +
  `RAAABm5pcnTNbZAAAAAB4KyuzgAAAAA2b3megJwAAAAIJ4jnQAAAAALeouDfAAAAA8+hzOJoQAAAABfUXQEwAAAAEckZzuM4AAAAAM31BfGyAAAAACg17KtAAAAAPXSUd6AAAAAA` +
  `eaS9rCrAAAAlisSxlAAAAAABBPg5l68gAAC+oulMgAAAAGDLU0i31abwevIAAAZtakdL65reLdBOAAYgp4Cx0owAAAAAAAAngFpuc+Onc+IAAAAAAAAAAAAAf/EACcQAAICAQIFA` +
  `wUAAAAAAAAAAAECAzAABBIREyAhQCIxMxAUMkFQ/9oACAEBAAEFAv7hYLnNTOdHn3EeLIr+G8gQPqSepNSQElV/AkmVMdy7dftkM2+72shfdHZqJOJrik2PWx4LbEeMVUvw26U+i` +
  `qTvFbphwjqPcWwfDXJ8tka7I6nbalsLl46nG5LYRwhrmXbLWO5HYV6pfVXAu6WwgMJo+W1KLvdECC3VCrTL6rpV3R0wrtivb8uv93e2NOi4e5oTUjAQwqadFxtSxwsWsBIxNSRiy` +
  `q3U0yLjao4zs3hLK64uqxZVb6NKzeQsrr5X/8QAFBEBAAAAAAAAAAAAAAAAAAAAcP/aAAgBAwEBPwEp/8QAFBEBAAAAAAAAAAAAAAAAAAAAcP/aAAgBAgEBPwEp/8QAJBAAAQMDA` +
  `wQDAAAAAAAAAAAAAQAhMAIiQBExURAgMmESUHH/2gAIAQEABj8C+8cryC8lurThurW7tKhqm3wPa1MDLSrfKHMvxpk9SEzUx1TEcR1fkxPMZmpkq0lAjJm1MZE1MhyRVIPUuhTbG` +
  `IBNNSYjVOREMA5HJRhvCYx8q1lcZGKudMe7dWhXHCYq4Jj0c5DHK//EACYQAQABBAICAQMFAAAAAAAAAAERACEwUTFhQEFxIIGREFChsfH/2gAIAQEAAT8h/fOOz5q3V1adl+1b5` +
  `14c2+0qxmG/dKrLd+gU4mg0Ae/dWdRo+BYedaRfhwCqbEoB/pzKCXjJ1ZZy3Wsc5G71nJ0cTSyy5ZtyxfH/ABc0u4xmD2zd4MZhbKbMZVOSIelOXePvH1eUssuQtegXJxj7WM3zC` +
  `+SA+m+QQG6EBrIllZIXJ0i5yxlkohxD1rPuoeflzW/s4p9Cxn29yYu5t3wBAaXAEg3QQRmU5IVxj0FKRtnAWZpVoHZU4JMShzaveS0VwgH5aalH5yKyw1Zj8q48nT9XMldFI4jtr` +
  `kV8LSum9B7/AJK4PnX6f1aW8jkWNN/K/9oADAMBAAIAAwAAABAAAAAAAAAAAAAAAAAAAxwggAAAAAAABzzzzgAAAAAABzzzzzxgAAAABDzzzzzzwAAAABBTzzzzyAAAAAADTzzzz` +
  `wgAAAAABTzzzzwgAAAAABTzzzyiAAAAAABTzzzywAAAAAAhRzzzxwwAADTzzzzzzzzzzwzzzzzzzzzzzzzz/8QAFBEBAAAAAAAAAAAAAAAAAAAAcP/aAAgBAwEBPxAp/8QAFBEBA` +
  `AAAAAAAAAAAAAAAAAAAcP/aAAgBAgEBPxAp/8QAKRABAAEDAgYBAwUAAAAAAAAAAREAITAxYUFRcYGRoUAgscEQUNHh8f/aAAgBAQABPxD98C7GgaSk9ln7UkS9kX7FDbbgf5KsI` +
  `PMs+/hyOu6OrRjIuKiNVNVu/QhLCaJZoxVpBiFOEo2n4AKnlH80lV3TgDkYDLagSibQcxysC6tKqrddXEKMlk0akyzqms5X3hzi8shJXhu3OhEk0x31iaT0pFWVZXdyqq6Bblsfs` +
  `8yP8w6P+Y+y1HOJzMpsdrY91Q8lBVajDlGNwEesuQm4lI85AVguuhU/aCepvjQS6ydeFIqyrK7uRLBqMlAXaqee+Pf2DrSIw2TUymYZhPuzkgwhImzfIouqD3FCLoAMgAeQA2++R` +
  `HROvesr8D8GhpqEg8MTT6HcEwBNWZp6jmRJoSjw4pn0Pcf6z8rB3C5ijyQN7W+dBIbjqUB2gPDg4USCaAhoEGYSSOaxRKci0J3ad0haHVnAkRqMlEndUPbhW7IhxCywc1ih0F73d` +
  `amy9r/yWqVn3Tk3+ENaAvJA0GAntP0qBLYNWpAA7z6qbBeFx8aUhOxrbx8LWR6B7q2AcPyw15tlnw/onejhYevkRk7oHv5X/9k=`;

export default connect(mapStateToProps, mapDispatchToProps)(Login);
