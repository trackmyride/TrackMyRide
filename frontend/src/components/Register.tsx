import * as React from 'react';
import { connect, Dispatch } from 'react-redux';
import '../css/App.css';
import { Button, FormGroup, Input, Container, Form } from 'reactstrap';
import { performRegister } from '../store/user/actions';
import { RegisterRequestParameters } from '../store/user/types.intf';
import { RouteComponentProps } from 'react-router-dom';
import { Jumbotron } from 'reactstrap';
import * as zxcvbn from 'zxcvbn';
import { CSSProperties } from 'react';
// import { ApplicationState } from '../store/stateTypes.intf';

export interface ConnectedDispatch {
  dispatchPerformRegister: (registerRequestParameters: RegisterRequestParameters) => void;
}
interface RouterProps extends RouteComponentProps<any>, React.Props<any> {
}

const mapDispatchToProps = (dispatch: Dispatch<RegisterRequestParameters>): ConnectedDispatch => ({
  dispatchPerformRegister:
    (registerRequestParameters: RegisterRequestParameters) => dispatch(performRegister(registerRequestParameters))
});

export class Register extends React.Component<ConnectedDispatch & RouterProps, {}> {
  inputUsername: HTMLInputElement | null;
  inputEmail: HTMLInputElement | null;
  inputPassword: HTMLInputElement | null;
  inputPasswordAgain: HTMLInputElement | null;

  image = DefaultImage;
  emailValid = false;
  passwordValid = false;
  formValid = false;
  passwordStrength = {
    name: '',
    color: '',
  };

  /**
   * Creates an instance of Register.
   * @param {(ConnectedDispatch & RouterProps)} props the properties to add to the component
   * @memberof Register
   */
  constructor(props: ConnectedDispatch & RouterProps) {
    super(props);
    // bind all functions to the component
    this.onRegisterSubmit = this.onRegisterSubmit.bind(this);
    this.previewFile = this.previewFile.bind(this);
    this.validateEmail = this.validateEmail.bind(this);
    this.passwordUpdate = this.passwordUpdate.bind(this);
  }

  /**
   * update the Validity of the passwordAgain Field. Also starts the calculation of the strangth of the password
   * @memberof Register
   */
  passwordUpdate() {
    this.passwordValid = this.inputPassword!.value !== '' && this.inputPassword!.value === this.inputPasswordAgain!.value;
    this.calculatePasswordStrength();
    this.updateFormValidity();
    this.forceUpdate();
  }

  /**
   * Calculates the strength of the password based on the zxcvbn algorithm. Sets the correct name and color to display the strength human readable
   * @memberof Register
   */
  calculatePasswordStrength() {
    let strength = zxcvbn(this.inputPassword!.value);
    switch (strength.score) {
      case 0: this.passwordStrength = { name: 'sehr schwach', color: 'red' }; break;
      case 1: this.passwordStrength = { name: 'schwach', color: 'orange' }; break;
      case 2: this.passwordStrength = { name: 'okay', color: 'yellow' }; break;
      case 3: this.passwordStrength = { name: 'stark', color: 'green' }; break;
      case 4: this.passwordStrength = { name: 'sehr stark', color: 'green' }; break;
      default: break;
    }
  }

  /**
   * validates if the email is valid. and sets the variables accordingly
   * only uses a simple alogrithm, that does not check if the entered TLD is valid or not.
   * @memberof Register
   */
  validateEmail() {
    let email = this.inputEmail!.value;
    this.emailValid = /[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}/.test(email);
    this.updateFormValidity();
    this.forceUpdate();
  }

  /**
   * updates the global validity of the form
   * @memberof Register
   */
  updateFormValidity() {
    this.formValid = this.emailValid && this.passwordValid;
    this.forceUpdate();
  }

  /**
   * reads the file from the fileupload and places it in the preview image.
   * Also reads the image as base64 string and saves it into the image property, ready to send it to the server
   * @returns 
   * @memberof Register
   */
  previewFile() {
    var inputField = document.querySelector('input[type=file]') as HTMLInputElement;
    if (!inputField) {
      return;
    }
    var file = inputField.files![0];
    var reader = new FileReader();

    reader.addEventListener(
      'load', () => {
        this.image = reader.result;
        this.forceUpdate();
      },
      false
    );

    if (file) {
      reader.readAsDataURL(file);
    }
  }

  /**
   * reads all fields and sends them to the backend to register the user
   * @memberof Register
   */
  onRegisterSubmit() {
    let username = this.inputUsername!.value;
    let email = this.inputEmail!.value;
    let password = this.inputPassword!.value;

    let registerRequestParameters: RegisterRequestParameters = {
      username: username,
      email: email,
      password: password,
      avatar: this.image,
    };
    this.props.dispatchPerformRegister(registerRequestParameters);
    this.props.history.push('/Login');
  }

  /**
   * the render function
   * @returns 
   * @memberof Register
   */
  render() {
    return (
      <div>
        <Jumbotron className="BetterJumbotron">
          <div>
            <h2 className="display-5">Registriere dich bei TrackMyRide</h2>
            <p className="text">
              {'Registriere dich ein um Touren aufzeichnen zu können, diese anzusehen, zu bearbeiten und zu veröffentlichen. ' +
                ' Wenn du einen Account bei TrackMyRide hast, kannst du ausserdem andere Touren bewerten und kommentieren.'}
            </p>
            <hr className="my-2" />
          </div>

        </Jumbotron>

        <Container >
          <Form className="form">
            <FormGroup>
              <div style={{ ...ContainerStyle, ...CenterWrapper }}>
                <div style={CenterWrapper}>
                  <img src={this.image} style={{ ...ImageStyle, ...FileInputStyle }} alt="Image preview..." />
                </div>
                <div style={{...CenterWrapper, ...InputStyle}} >
                  <input type="file" style={{ ...FileInputStyle, ...LabelStyle }} onChange={this.previewFile} />
                </div>
                <div style={{...CenterWrapper, ...InputStyle}} >
                  <Input
                    type="text"
                    innerRef={(input) => this.inputUsername = input}
                    placeholder="Username"
                  />

                  <Input
                    type="text"
                    style={this.emailValid ? {} : InvalidStyle}
                    onChange={this.validateEmail}
                    innerRef={(input) => this.inputEmail = input}
                    placeholder="Email"
                  />

                  <span style={{ color: this.passwordStrength.color, float: 'right' }}>{this.passwordStrength.name}</span>
                  <Input
                    type="password"
                    onChange={this.passwordUpdate}
                    innerRef={(input) => this.inputPassword = input}
                    placeholder="Passwort"
                  />

                  <Input
                    type="password"
                    style={this.passwordValid ? {} : InvalidStyle}
                    onChange={this.passwordUpdate}
                    innerRef={(input) => this.inputPasswordAgain = input}
                    placeholder="Passwort wiederholen"
                  />
                  <div style={ButtonWrapperStyle}>
                    <Button color="primary" disabled={!this.formValid} onClick={this.onRegisterSubmit} style={{ backgroundColor: '#012674', margin: '10px' }}>Register</Button>
                  </div>
                </div>
              </div>
            </FormGroup>
          </Form>
        </Container>
      </div>
    );
  }
}

const ContainerStyle: CSSProperties = {
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  'backgroundColor': 'white',
  'border': '1px solid #d8d8d8',
  paddingBottom: '10px',

};

// sets all Label to use the size they can. (otherwise they are not centered)
const InputStyle: CSSProperties = {
  width: '80%',
  maxWidth: '700px',
  margin: '10px auto 10px auto',
};

// sets the size of the image, makes it look round and ensure there is always something in the circle, no matter what resolution is uploaded
const ImageStyle: CSSProperties = {
  height: '25%',
  width: '25%',
  objectFit: 'cover',
  borderRadius: '50%'
};

// adds some space on top of the fileInput
const FileInputStyle: CSSProperties = {
  padding: '1vh 0',
};

// adds a red outline to any invalid fields
const InvalidStyle = {
  borderColor: 'red',
  outline: '0 0 0 0.2rem rgba(255, 0, 0,.25)'
};

// sets all inner elements to appear in center
const CenterWrapper: CSSProperties = {
  textAlign: 'center',
};

// sets all Label to use the size they can. (otherwise they are not centered)
const LabelStyle: CSSProperties  = {
  display: 'flex',
  justifyContent: 'center',
  width: '100%',
  maxWidth: '500px',
  margin: '10px, auto 10px, auto'
};

const ButtonWrapperStyle: CSSProperties  = {
  display: 'block',
  width: '100%',
  maxWidth: '500px',
  margin: 'auto',
  paddingTop: '10px'
};

// a default image of an avatar
const DefaultImage =
  `data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAUEBAUEAwUFBAUGBgUGCA4JCAcHCBEMDQoOFBEVFBMRExMWGB8bFhceFxMTGyUcHiAhIyMjF` +
  `RomKSYiKR8iIyL/2wBDAQYGBggHCBAJCRAiFhMWIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiL/wgARCADIAMgDASIAAhEBAxEB/8QAG` +
  `wABAAIDAQEAAAAAAAAAAAAAAAMFAQQGAgf/xAAUAQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIQAxAAAAH6AAAAAAAAAAAAAAAAAAirS0joMF/4ox0M3M75btfYAAAAAAAGtsc0S` +
  `RAAABm5pcnTNbZAAAAAB4KyuzgAAAAA2b3megJwAAAAIJ4jnQAAAAALeouDfAAAAA8+hzOJoQAAAABfUXQEwAAAAEckZzuM4AAAAAM31BfGyAAAAACg17KtAAAAAPXSUd6AAAAAA` +
  `eaS9rCrAAAAlisSxlAAAAAABBPg5l68gAAC+oulMgAAAAGDLU0i31abwevIAAAZtakdL65reLdBOAAYgp4Cx0owAAAAAAAAngFpuc+Onc+IAAAAAAAAAAAAAf/EACcQAAICAQIFA` +
  `wUAAAAAAAAAAAECAzAABBIREyAhQCIxMxAUMkFQ/9oACAEBAAEFAv7hYLnNTOdHn3EeLIr+G8gQPqSepNSQElV/AkmVMdy7dftkM2+72shfdHZqJOJrik2PWx4LbEeMVUvw26U+i` +
  `qTvFbphwjqPcWwfDXJ8tka7I6nbalsLl46nG5LYRwhrmXbLWO5HYV6pfVXAu6WwgMJo+W1KLvdECC3VCrTL6rpV3R0wrtivb8uv93e2NOi4e5oTUjAQwqadFxtSxwsWsBIxNSRiy` +
  `q3U0yLjao4zs3hLK64uqxZVb6NKzeQsrr5X/8QAFBEBAAAAAAAAAAAAAAAAAAAAcP/aAAgBAwEBPwEp/8QAFBEBAAAAAAAAAAAAAAAAAAAAcP/aAAgBAgEBPwEp/8QAJBAAAQMDA` +
  `wQDAAAAAAAAAAAAAQAhMAIiQBExURAgMmESUHH/2gAIAQEABj8C+8cryC8lurThurW7tKhqm3wPa1MDLSrfKHMvxpk9SEzUx1TEcR1fkxPMZmpkq0lAjJm1MZE1MhyRVIPUuhTbG` +
  `IBNNSYjVOREMA5HJRhvCYx8q1lcZGKudMe7dWhXHCYq4Jj0c5DHK//EACYQAQABBAICAQMFAAAAAAAAAAERACEwUTFhQEFxIIGREFChsfH/2gAIAQEAAT8h/fOOz5q3V1adl+1b5` +
  `14c2+0qxmG/dKrLd+gU4mg0Ae/dWdRo+BYedaRfhwCqbEoB/pzKCXjJ1ZZy3Wsc5G71nJ0cTSyy5ZtyxfH/ABc0u4xmD2zd4MZhbKbMZVOSIelOXePvH1eUssuQtegXJxj7WM3zC` +
  `+SA+m+QQG6EBrIllZIXJ0i5yxlkohxD1rPuoeflzW/s4p9Cxn29yYu5t3wBAaXAEg3QQRmU5IVxj0FKRtnAWZpVoHZU4JMShzaveS0VwgH5aalH5yKyw1Zj8q48nT9XMldFI4jtr` +
  `kV8LSum9B7/AJK4PnX6f1aW8jkWNN/K/9oADAMBAAIAAwAAABAAAAAAAAAAAAAAAAAAAxwggAAAAAAABzzzzgAAAAAABzzzzzxgAAAABDzzzzzzwAAAABBTzzzzyAAAAAADTzzzz` +
  `wgAAAAABTzzzzwgAAAAABTzzzyiAAAAAABTzzzywAAAAAAhRzzzxwwAADTzzzzzzzzzzwzzzzzzzzzzzzzz/8QAFBEBAAAAAAAAAAAAAAAAAAAAcP/aAAgBAwEBPxAp/8QAFBEBA` +
  `AAAAAAAAAAAAAAAAAAAcP/aAAgBAgEBPxAp/8QAKRABAAEDAgYBAwUAAAAAAAAAAREAITAxYUFRcYGRoUAgscEQUNHh8f/aAAgBAQABPxD98C7GgaSk9ln7UkS9kX7FDbbgf5KsI` +
  `PMs+/hyOu6OrRjIuKiNVNVu/QhLCaJZoxVpBiFOEo2n4AKnlH80lV3TgDkYDLagSibQcxysC6tKqrddXEKMlk0akyzqms5X3hzi8shJXhu3OhEk0x31iaT0pFWVZXdyqq6Bblsfs` +
  `8yP8w6P+Y+y1HOJzMpsdrY91Q8lBVajDlGNwEesuQm4lI85AVguuhU/aCepvjQS6ydeFIqyrK7uRLBqMlAXaqee+Pf2DrSIw2TUymYZhPuzkgwhImzfIouqD3FCLoAMgAeQA2++R` +
  `HROvesr8D8GhpqEg8MTT6HcEwBNWZp6jmRJoSjw4pn0Pcf6z8rB3C5ijyQN7W+dBIbjqUB2gPDg4USCaAhoEGYSSOaxRKci0J3ad0haHVnAkRqMlEndUPbhW7IhxCywc1ih0F73d` +
  `amy9r/yWqVn3Tk3+ENaAvJA0GAntP0qBLYNWpAA7z6qbBeFx8aUhOxrbx8LWR6B7q2AcPyw15tlnw/onejhYevkRk7oHv5X/9k=`;

export default connect(undefined, mapDispatchToProps)(Register);
