import * as React from 'react';
import { connect } from 'react-redux';
import { ApplicationState } from '../../store/stateTypes.intf';
import '../../css/App.css';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader, Container } from 'reactstrap';
import { Line } from 'react-chartjs-2';
import { CSSProperties } from 'react';

const minWidthGraph = 200;
const minHeigtGraph = 200;

interface DashboardGraphsInterface {
    width: number;
    height: number;
}

class DashboardGraphs extends React.Component<ApplicationState & DashboardGraphsInterface, 
                            { showToolTip: boolean, windowWidth: number, modalAltitude: boolean, modalVelocity: boolean }> {
    contentDiv: HTMLDivElement | null;
    constructor(props: ApplicationState & DashboardGraphsInterface) {
        super(props);
        const initialWidth = window.innerWidth > 0 ? window.innerWidth : 500;
        this.state = { showToolTip: false, windowWidth: initialWidth * 0.83, modalAltitude: false, modalVelocity: false };
        this.toggleAltitude = this.toggleAltitude.bind(this);
        this.toggleVelocity = this.toggleVelocity.bind(this);
    }

    componentDidMount() {
        window.addEventListener('resize', this.handleResize.bind(this));
        
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResize);
    }

    handleResize() {
        this.setState({ windowWidth: window.innerWidth - 100 });
    }

    toggleAltitude() {
        this.setState({
            modalAltitude: !this.state.modalAltitude
        });
    }

    toggleVelocity() {
        this.setState({
            modalVelocity: !this.state.modalVelocity
        });
    }

    fillChartData = (labels: number[], label: string, data: number[]) => {
        return {
            labels: labels.map((element: number) => element.toFixed(1)),
            cubicInterpolationMode: 'monotone',
            datasets: [{
                label: label,
                fill: false,
                lineTension: 0.1,
                backgroundColor: '#00AFD8',
                borderColor: '#00AFD8',
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: '#00AFD8',
                pointBackgroundColor: '#fff',
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: '#00AFD8',
                pointHoverBorderColor: 'rgba(220,220,220,1)',
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: data
            }]
        };
    }
    render() {
        return (  
            <div className="TourGraphs" style={GraphsContainerStyle} >
            <Container className="GraphContainer" style={{ ...GraphBoxStyle, }}>  
                        <div style={GraphBoxContentStyle}>
                        <Line
                            data={this.fillChartData(this.props.tours.tourData!.time, 'Höhenverlauf', this.props.tours.tourData!.altitude)}
                            width={Math.max(Math.min(this.state.windowWidth * 0.4, minWidthGraph), minWidthGraph)}
                            height={Math.max(Math.min(this.state.windowWidth * 0.4 / 1.2, minHeigtGraph), minHeigtGraph)}
                            options={{
                                maintainAspectRatio: false
                            }}
                        />
                        <Button className="button" color="primary" onClick={this.toggleAltitude}>Detailansicht</Button>
                        </div>
                        <div>
                            
                            <Modal isOpen={this.state.modalAltitude} toggle={this.toggleAltitude} className="largeModal">
                                <ModalHeader toggle={this.toggleAltitude}>Höhendiagramm</ModalHeader>
                                <ModalBody>
                                    <Line
                                        data={this.fillChartData(this.props.tours.tourData!.time, 'Höhenverlauf', this.props.tours.tourData!.altitude)}
                                        width={this.state.windowWidth}
                                        height={this.state.windowWidth * 0.4 / 1.2}
                                        options={{
                                            maintainAspectRatio: true
                                        }}
                                    />
                                </ModalBody>
                                <ModalFooter>
                                    <Button color="secondary" onClick={this.toggleAltitude}>Cancel</Button>
                                </ModalFooter>
                            </Modal>
                        </div>
                
              </Container>
              <Container className="GraphContainer" style={GraphBoxStyle}>  
                        <div className="GraphContentBox" style={GraphBoxContentStyle}>
                        <Line
                            data={this.fillChartData(this.props.tours.tourData!.time, 'Geschwindigkeitsverlauf', this.props.tours.tourData!.velocity)}
                            width={Math.max(Math.min(this.state.windowWidth * 0.4, minWidthGraph), minWidthGraph)}
                            height={Math.max(Math.min(this.state.windowWidth * 0.4 / 1.2, minHeigtGraph), minHeigtGraph)}
                            options={{
                                maintainAspectRatio: false
                            }}
                        />
                        <Button className="button" color="primary" onClick={this.toggleVelocity}>Detailansicht</Button>
                        </div>
                        <div>
                            <Modal isOpen={this.state.modalVelocity} toggle={this.toggleVelocity} className="largeModal">
                                <ModalHeader toggle={this.toggleVelocity}>Geschwindigkeitsverlauf</ModalHeader>
                                <ModalBody>
                                    <Line
                                        data={this.fillChartData(this.props.tours.tourData!.time, 'Geschwindigkeitsverlauf', this.props.tours.tourData!.velocity)}
                                        width={this.state.windowWidth * 0.4}
                                        height={this.state.windowWidth * 0.4 / 2}
                                        options={{
                                            maintainAspectRatio: true
                                        }}
                                    />
                                </ModalBody>
                                <ModalFooter>
                                    <Button color="secondary" onClick={this.toggleVelocity}>Cancel</Button>
                                </ModalFooter>
                            </Modal>
                        </div>
                </Container>
            </div>
        );
    }

}

const GraphsContainerStyle: CSSProperties = {
    display: 'flex',
    flexDirection : 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    width: 'calc(100% - 40px)',
    maxHeight: 'calc(100% - 40px)',
    margin: '10px 10px 10px 10px',
    alignContent: 'flex-end',
    alignSelf: 'flex-end'
};

const GraphBoxStyle: CSSProperties = {
    width: '50%',
    maxHeight: '400px',
    display: 'inline-block',
    margin: '20px auto 20px auto'
};
const GraphBoxContentStyle: CSSProperties = {
    width: '70%',
    height: '100%',
    margin: 'auto auto auto auto'
};

const mapStateToProps = (state: ApplicationState) => state;

export default connect(mapStateToProps)(DashboardGraphs);