import * as React from 'react';
import '../../css/App.css';
import { Map, TileLayer, Marker, Popup, Polyline } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import { ErrorBoundary } from './ErrorBoundary';
import { CSSProperties } from 'react';
import { ApplicationState, TourData } from '../../store/stateTypes.intf';
import { connect, Dispatch } from 'react-redux';
import { changeLineColors } from '../../store/tours/actions';
import { MapControls } from '../../containers/MapControls';

interface MapDispatchToPropsInterface {
  dispatchChangeLineColors: () => void;
}

const mapDispatchToProps = (dispatch: Dispatch<ApplicationState>): MapDispatchToPropsInterface => ({
  dispatchChangeLineColors: () =>
    dispatch(changeLineColors()),
});

const mapStateToProps = (state: ApplicationState) => (state);

class TourMap extends React.Component<ApplicationState & MapDispatchToPropsInterface, {}> {
  constructor(props: ApplicationState & MapDispatchToPropsInterface) {
    super(props);
    this.changeShow = this.changeShow.bind(this);
  }
  createGradientPolyline() {
    var index = [];
    var result;
    if (this.props.tours.tourData != null) {
      var tourData: TourData = this.props.tours.tourData;
      for (var i = 0; i < tourData.time.length - 1; i++) {
        index[i] = i;
      }
      if (tourData.showVelocity) {
        result = index.map(element =>
          <Polyline
            key={element}
            color={tourData.velocityMap[element]}
            positions={
              [tourData.coordinates[element], tourData.coordinates[element + 1]] as [number, number][]
            }
          />
        );
      } else {
        result =
          <Polyline
            color="rgb(0,0,255)"
            positions={tourData
              ? tourData.coordinates as [number, number][]
              : [[]]}
          />;

      }
    }

    return result;
  }

  changeShow() {
    this.props.dispatchChangeLineColors();
  }
  render() {
    var { tours } = this.props;
    var showDistance = true;
    var showVelocity = false;
    if (tours.tourData != null) {
      showDistance = tours.tourData.showDistance;
      showVelocity = tours.tourData.showVelocity;
    }
    
    return (
      <div className="Map" style={mapComponentStyle}>
      <ErrorBoundary>
          <Map
            center={
              [
                tours.tourData
                  ? tours.tourData.coordinates[0][0]
                  : 47.5013931,
                tours.tourData
                  ? tours.tourData.coordinates[0][1]
                  : 8.7192219
              ]
            }
            zoom={10}
            style={MapStyle}
          >
            {
              this.createGradientPolyline()
            }
            <TileLayer
              attribution="&copy; <a href='http://osm.org/copyright'>OpenStreetMap</a> contributors"
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            <Marker
              position={
                [
                  tours.tourData
                    ? tours.tourData.coordinates[0][0]
                    : 47.5013931,
                  tours.tourData
                    ? tours.tourData.coordinates[0][1]
                    : 8.7192219
                ]
              }
            >
              <Popup minWidth={90}>
                <span>
                  {'Start'}
                </span>
              </Popup>
            </Marker>
          </Map>
        </ErrorBoundary>
        <MapControls showDistance={showDistance} showVelocity={showVelocity} handleClick={this.changeShow} />
      </div>
    );
  }
}

const mapComponentStyle: CSSProperties = {
  display: 'flex',
  flexDirection: 'column',
  minWidth: '100px',
  minHeight: '450px', 
  width: '100%',
  height: '40%',
  margin: '5px auto 5px auto'
};

const MapStyle: CSSProperties = {
  height: '100%'
};

export default connect(mapStateToProps, mapDispatchToProps)(TourMap);
