import * as React from 'react';
import { connect, Dispatch } from 'react-redux';
import { ApplicationState, TourData } from '../../store/stateTypes.intf';
import '../../css/App.css';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader, FormGroup, Input, Label, Jumbotron } from 'reactstrap';
import Switch from 'react-switch';
import { UpdateTourMetaDataParameters, updateTourMetaData, deleteTour } from '../../store/tours/actions';
import { TourRequestParameters } from '../../store/tours/types.intf';
var FontAwesome = require('react-fontawesome');

export interface ConnectedDispatch {
  dispatchUpdateTourMetaData: (updateTourMetaDataParameters: UpdateTourMetaDataParameters) => void;
  dispatchDeleteTour: (tourRequestParameters: TourRequestParameters) => void;
}

const mapDispatchToProps = (dispatch: Dispatch<null>): ConnectedDispatch => ({
  dispatchUpdateTourMetaData:
    (updateTourMetaDataParameters: UpdateTourMetaDataParameters) => dispatch(updateTourMetaData(updateTourMetaDataParameters)),
  dispatchDeleteTour:
    (deleteTourParameters: TourRequestParameters) => dispatch(deleteTour(deleteTourParameters))
});

const mapStateToProps = (state: ApplicationState) => (state);

class TourDescription extends React.Component<ApplicationState & ConnectedDispatch, { modalTitle: boolean /*, inputValuePublic: boolean */ }> {
  // uncontrolled components:
  inputRefTitle: HTMLInputElement | null;
  inputRefDescription: HTMLInputElement | null;
  currentTour: TourData | null;
  inputValuePublicState: boolean;

  constructor(props: ApplicationState & ConnectedDispatch) {
    super(props);
    this.toggleEditModal = this.toggleEditModal.bind(this);
    this.onEditSubmit = this.onEditSubmit.bind(this);
    this.onPublicStateToggled = this.onPublicStateToggled.bind(this);
    this.onDeleteTour = this.onDeleteTour.bind(this);
    this.currentTour = this.props.tours.tourData;
    this.state = { modalTitle: false /*, inputValuePublic: this.currentTour!.public*/};
    this.inputValuePublicState = this.currentTour ? this.currentTour!.public : false;
  }

  toggleEditModal() {
    this.setState({
      modalTitle: !this.state.modalTitle
    });
  }

  onEditSubmit() {
    this.toggleEditModal();
    const oldTitle = this.currentTour!.title;
    const oldDescription = this.currentTour!.description;
    const oldPublicState = this.currentTour!.public;
    const newTitle = this.inputRefTitle!.value;
    const newDescription = this.inputRefDescription!.value;
    const newPublicState = this.inputValuePublicState;
    if (this.currentTour != null) {
      const titleIsNew = newTitle !== oldTitle;
      const descriptionIsNew = newDescription !== oldDescription;
      const publicStateIsNew = newPublicState === null ? false : newPublicState !== oldPublicState;
      if (titleIsNew || descriptionIsNew || publicStateIsNew) {
        let updateTourMetaDataParameters: UpdateTourMetaDataParameters = {
          id: this.currentTour!.tourId,
          name: titleIsNew ? newTitle : oldTitle,
          description: descriptionIsNew ? newDescription : oldDescription,
          public: publicStateIsNew ? newPublicState as boolean : oldPublicState,
        };
        this.props.dispatchUpdateTourMetaData(updateTourMetaDataParameters);
      }
    }
  }

  onPublicStateToggled(newState: boolean) {
    this.inputValuePublicState = newState;
    this.forceUpdate();
  }
  
  onDeleteTour() {
    if (confirm('Möchtest du diese Tour wirklich endgültig löschen?\nDies kann nicht rückgängig gemacht werden.')) {
      this.props.dispatchDeleteTour({tourId: this.currentTour!.tourId});
      location.reload();
    }
  }

  render() {
    // const { tours } = this.props;

    let editTourButton: JSX.Element | null;
    let deleteTourButton: JSX.Element | null;
    if (this.currentTour !== null && this.currentTour.userId === this.props.user.userId) {
      editTourButton = (
        <Button color="primary" onClick={this.toggleEditModal} style={{ display: 'inline', float: 'right', zIndex: 9999 }}>
          Bearbeiten
          &nbsp;
          <FontAwesome name="edit" />
        </Button>
      );
      deleteTourButton = null;
      /*
      (
        <Button color="danger" onClick={this.onDeleteTour} style={{ display: 'inline', float: 'right', marginLeft: 10 }}>
          Löschen
          &nbsp;
          <FontAwesome name="trash" />
        </Button>
      );
      */
    } else {
      editTourButton = null;
      deleteTourButton = null;
    }
    return (
<div>
        <Jumbotron className="BetterJumbotron">
        <h2 className="display-5">
            <span>
              {this.currentTour != null ? this.currentTour.title : 'Tour-Titel'}
            </span>
            {deleteTourButton}
            {editTourButton}
          </h2>
          <p className="text" style={pStyle}>
            {this.currentTour != null ? this.currentTour.description : 'Tour-Beschreibung'}
          </p>
          </Jumbotron>
          <div>
            <Modal isOpen={this.state.modalTitle} toggle={this.toggleEditModal} className="largeModal">
              <ModalHeader toggle={this.toggleEditModal}>
                Tour bearbeiten
              </ModalHeader>
              <ModalBody>
                <form className="form-edit-tour">
                  <FormGroup>
                    <Label style={LabelStyle}>
                      Titel
                      <Input type="text" innerRef={(input) => this.inputRefTitle = input} defaultValue={this.currentTour!.title} />
                    </Label>
                  </FormGroup>
                  <FormGroup>
                    <Label style={LabelStyle}>
                      Beschreibung
                      <Input style={{height: '15em'}} type="textarea" innerRef={(input) => this.inputRefDescription = input} defaultValue={this.currentTour!.description} />
                    </Label>
                  </FormGroup>
                  <FormGroup>
                    <Label style={LabelStyle}>
                      Freigabeeinstellung
                      <div style={{display: 'block', marginTop: 10}}>
                        <Switch
                          onChange={this.onPublicStateToggled}
                          checked={this.inputValuePublicState}
                          handleDiameter={28}
                          height={40}
                          width={70}
                          className="react-switch"
                          uncheckedIcon={
                            <div
                              style={{
                                display: 'flex',
                                justifyContent: 'center',
                                alignItems: 'center',
                                height: '100%',
                                fontSize: 18,
                                color: 'white',
                                paddingRight: 4,
                              }}
                            >
                              <FontAwesome name="lock" />
                            </div>
                          }
                          checkedIcon={
                            <div
                              style={{
                                display: 'flex',
                                justifyContent: 'center',
                                alignItems: 'center',
                                height: '100%',
                                fontSize: 18,
                                color: 'white',
                                paddingLeft: 4,
                              }}
                            >
                              <FontAwesome name="globe" />
                            </div>
                          }
                        />
                      </div>
                    </Label>
                  </FormGroup>
                </form>
              </ModalBody>
              <ModalFooter>
                <Button color="primary" onClick={this.onEditSubmit} style={{ margin: '10px' }}>Speichern</Button>
                <Button color="secondary" onClick={this.toggleEditModal}>Abbrechen</Button>
              </ModalFooter>
            </Modal>
          </div>
       </div>

    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TourDescription);

// sets all Label to use the size they can. (otherwise they are not centered)
const LabelStyle = {
  display: 'block',
  width: '100%',
  maxWidth: '500px',
  margin: 'auto'
};
const pStyle = {
  marginBottom: '0px',
  paddingBottom: '0px'
};