import * as React from 'react';
import { connect, Dispatch } from 'react-redux';
import { ApplicationState } from '../../store/stateTypes.intf';
import '../../css/App.css';
import TourMap from './TourMap';
import TourDescription from './TourDescription';
import DashboardGraphs from './DashboardGraphs';
import { RouteComponentProps } from 'react-router-dom';
import { fetchTourData } from '../../store/tours/actions';
import { Jumbotron } from 'reactstrap';
import { CSSProperties } from 'react';
var Loader = require('react-loader');

interface MapDispatchToPropsInterface {
  dispatchFetchTourData: (tourId: number) => void;
}

const mapDispatchToProps = (dispatch: Dispatch<ApplicationState>): MapDispatchToPropsInterface => ({
  dispatchFetchTourData: (tourId: number) =>
    dispatch(fetchTourData({ tourId })),
});

const mapStateToProps = (state: ApplicationState) => (state);

class Dashboard extends React.Component<ApplicationState & MapDispatchToPropsInterface & RouteComponentProps<{}>, {}> {

  constructor(props: ApplicationState & MapDispatchToPropsInterface & RouteComponentProps<{}>) {
    super(props);
    var location = this.props.location.pathname.split('/');
    var tourId = location[location.length - 1];
    this.props.dispatchFetchTourData(Number(tourId));
    this.state = {...this.state, ...{
      elemHeight: 0,
      elementWidth: 0,
    }};
  }

  componentDidUpdate() {
    if (this.props.tours.accessDenied) {
      this.props.history.push('/Login');
    }
  }

  render() {
    const { isLoadingTourData, accessDenied } = this.props.tours;

    if ((isLoadingTourData || accessDenied)) {
      console.log(isLoadingTourData);
      return (
        <div className="Dashboard">
          <Loader loaded={!isLoadingTourData} />
        </div>
      );
    } else if ((!isLoadingTourData && !accessDenied)) {
      console.log(isLoadingTourData);
      console.log(this.props.tours.tourData);
      return (
        <div 
             className="Dashboard" 
             style={DashboardStyle} 
        >
          <Jumbotron className="BetterJumbotron">
            <div>
              <p>
                <TourDescription {...this.props} />
              </p>
              <hr className="my-2" />
            </div>
          </Jumbotron>
          <TourMap />
          <DashboardGraphs height={100} width={100} />
        </div>

      );
    } else {
      return (<div />);
    }
  }
}

const DashboardStyle: CSSProperties = {
  height: '100%',
  overflowY: 'auto',
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
