import * as React from 'react';
import { connect, Dispatch } from 'react-redux';
import '../css/App.css';
import { performLogout } from '../store/user/actions';
import { RouteComponentProps } from 'react-router-dom';
import { ApplicationState } from '../store/stateTypes.intf';

interface RouterProps extends RouteComponentProps<any>, React.Props<any> {
}

export interface ConnectedDispatch {
  dispatchPerformLogout: () => void;
}

const mapDispatchToProps = (dispatch: Dispatch<ApplicationState>): ConnectedDispatch => ({
  dispatchPerformLogout: () => dispatch(performLogout())
});

const mapStateToProps = (state: ApplicationState) => (state);

export class Logout extends React.Component<ApplicationState & ConnectedDispatch & RouterProps, {}> {
  
  constructor(props: ApplicationState & ConnectedDispatch & RouterProps) {
    super(props);
  }
  
  componentWillMount () {
    this.props.dispatchPerformLogout();
    var routerlink = '/AllTours';
    this.props.history.push(routerlink);
  }

  render() {
    return null;
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Logout);
