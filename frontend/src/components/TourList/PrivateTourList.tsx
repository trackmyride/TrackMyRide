'use strict';
import * as React from 'react';
import '../../css/App.css';
import { Jumbotron, Collapse } from 'reactstrap';
import { ApplicationState } from '../../store/stateTypes.intf';
import { connect, Dispatch } from 'react-redux';
import { fetchUserTourList } from '../../store/tours/actions';
import { RouteComponentProps } from 'react-router-dom';
import { TourList } from '../../containers/TourList';
import { ErrorBoundary } from '../Dashboard/ErrorBoundary';
import { CSSProperties } from 'react';

var Loader = require('react-loader');
var deltaY = 0;
var yMax = 0;

interface MapDispatchToPropsInterface {
  dispatchFetchUserTourList: () => void;
}

const mapDispatchToProps = (dispatch: Dispatch<ApplicationState>): MapDispatchToPropsInterface => ({
  dispatchFetchUserTourList: () =>
    dispatch(fetchUserTourList())
});

const mapStateToProps = (state: ApplicationState) => state;

class PrivateTourList extends React.Component<ApplicationState & MapDispatchToPropsInterface & RouteComponentProps<{}>, {}> {

  state = {
    fadeIn: true,
  };

  constructor(props: ApplicationState & MapDispatchToPropsInterface & RouteComponentProps<{}>) {
    super(props);
    this.handleClickOnListItem = this.handleClickOnListItem.bind(this);
    this.handleClickOnFadeable = this.handleClickOnFadeable.bind(this);
    this.handleWheel = this.handleWheel.bind(this);
    this.props.dispatchFetchUserTourList();
  }

  /*
  componentDidMount() {
    this.props.dispatchFetchUserTourList();
  }
  */

  handleClickOnListItem(tourid: number) {
    let route = '/Dashboard/'.concat(tourid.toString());
    this.props.history.push(route);
  }

  handleClickOnFadeable() {
    console.log('handleFade');
    console.log(this.state);
    this.setState({ fadeIn: !this.state.fadeIn });
  }

  handleWheel(e: any) {
    deltaY += e.deltaY;
    if (deltaY > 10 && this.state.fadeIn) {
      this.setState({ fadeIn: false });
    } else if (deltaY <= -50) {
      this.setState({ fadeIn: true });
      deltaY = 0;
    } else if (yMax < deltaY) {
      deltaY = yMax;
    }
  }

  render() {
    if (!this.props.user.isLoggedIn) {
      this.props.history.push('/Login');
    }
    const { tours } = this.props;
    yMax = tours.tourList.length * 40;

    if (tours.isLoadingTourMetadata) {
      return (
        <div className="MeineTouren" id="MeineTouren" style={ListStyle} onClick={this.handleClickOnFadeable} onWheel={this.handleWheel} >
          <Jumbotron className="BetterJumbotron" >
            <Collapse className="BetterJumbotron" isOpen={this.state.fadeIn} >
              <h2 className="display-5">Meine Touren</h2>
              <p className="text">
                {'In dieser Liste werden alle deine privaten Touren angezeigt. Mit einem Klick auf eine Tour ' +
                  'öffnet sich dein persönliches Dashboard und es werden dir weitere Daten angezeigt.'}
              </p>
              <hr className="my-2" />
            </Collapse>
          </Jumbotron>
          <ErrorBoundary>
            <Loader loaded={!tours.isLoadingTourMetadata} />

          </ErrorBoundary>
        </div>
      );
    } else {
      return (
        <div className="MeineTouren" id="MeineTouren" style={ListStyle} onClick={this.handleClickOnFadeable} onWheel={this.handleWheel} >

          <Jumbotron className="BetterJumbotron" >
            <Collapse className="BetterJumbotron" isOpen={this.state.fadeIn} >
              <h2 className="display-5">Meine Touren</h2>
              <p className="text">
                {'In dieser Liste werden alle deine privaten Touren angezeigt. Mit einem Klick auf eine Tour ' +
                  'öffnet sich dein persönliches Dashboard und es werden dir weitere Daten angezeigt.'}
              </p>
              <hr className="my-2" />
            </Collapse>

          </Jumbotron>
          <ErrorBoundary>
            <TourList
              id="List"
              tourList={tours.tourList}
              handleClick={this.handleClickOnListItem}
            />
          </ErrorBoundary>
        </div >
      );
    }
  }
}

let ListStyle: CSSProperties = {
  height: '100%',
  display: 'flex',
  flexDirection: 'column'
};

export default connect(mapStateToProps, mapDispatchToProps)(PrivateTourList);
