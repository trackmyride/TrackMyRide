import { createStore, Store, applyMiddleware } from 'redux';
import { ApplicationState } from './stateTypes.intf';
import { toursReducer, emptyInitialTourState } from './tours/reducer';
import { userReducer, initialUserState } from './user/reducer';
import { combineReducers } from 'redux';
import thunk from 'redux-thunk';

/**
 * Creates one reducer function out of all existing reducers.
 * Properties have to be named the same way as in the ApplicationState type
 */
const reducer = combineReducers<ApplicationState>({
  tours: toursReducer,
  user: userReducer,
});

export const initialApplicationState: ApplicationState = {
  tours: emptyInitialTourState,
  user: initialUserState,
};

/**
 * Creates the Redux Store and returns it
 * @param initialApplicationStateParameter Initial application state
 */
function configureStore(initialApplicationStateParameter: ApplicationState): Store<ApplicationState> {
  return createStore(
    reducer,
    initialApplicationStateParameter,
    applyMiddleware(thunk),
  );
}

export const store = configureStore(initialApplicationState);
