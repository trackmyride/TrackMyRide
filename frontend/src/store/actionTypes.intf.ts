import { TourMetaData, TourData, UserData } from './stateTypes.intf';
import { LoginRequestParameters, RegisterRequestParameters } from './user/types.intf';
import { TourRequestParameters } from './tours/types.intf';
import { ResponseContainer, ErrorContainer, RequestContainer } from '../api/types.intf';
import key from './actionTypeKeys.intf';
import { UpdateTourMetaDataParameters } from './tours/actions';

/**
 * All Action types for all reducer functions are registered here
 */
export type Action =
  | ({ type: key.FETCH_TOUR_LIST_METADATA_REQUEST } & RequestContainer<NoRequestParameters> )
  | ({ type: key.FETCH_TOUR_LIST_METADATA_SUCCESS } & RequestContainer<NoRequestParameters> & ResponseContainer<TourMetaData[]>)
  | ({ type: key.FETCH_TOUR_LIST_METADATA_UNAUTHORIZED } & RequestContainer<NoRequestParameters> )
  | ({ type: key.FETCH_TOUR_LIST_METADATA_ERROR } & RequestContainer<NoRequestParameters> & ErrorContainer)
  | ({ type: key.FETCH_TOUR_DATA_REQUEST } & RequestContainer<TourRequestParameters> )
  | ({ type: key.FETCH_TOUR_DATA_SUCCESS } & RequestContainer<TourRequestParameters> & ResponseContainer<TourData> )
  | ({ type: key.FETCH_TOUR_DATA_ERROR } & RequestContainer<TourRequestParameters> & ErrorContainer )
  | ({ type: key.DELETE_TOUR_REQUEST } & RequestContainer<NoRequestParameters> )
  | ({ type: key.DELETE_TOUR_SUCCESS } & RequestContainer<NoRequestParameters> )
  | ({ type: key.DELETE_TOUR_ERROR } & RequestContainer<NoRequestParameters> & ErrorContainer)
  | ({ type: key.PERFORM_LOGIN_REQUEST } & RequestContainer<LoginRequestParameters> )
  | ({ type: key.PERFORM_LOGIN_SUCCESS } & RequestContainer<LoginRequestParameters> & ResponseContainer<UserData> )
  | ({ type: key.PERFORM_LOGIN_ERROR } & RequestContainer<LoginRequestParameters> & ErrorContainer )
  | ({ type: key.PERFORM_REGISTER_REQUEST } & RequestContainer<RegisterRequestParameters> )
  | ({ type: key.PERFORM_REGISTER_SUCCESS } & RequestContainer<RegisterRequestParameters> )
  | ({ type: key.PERFORM_REGISTER_ERROR } & RequestContainer<RegisterRequestParameters> & ErrorContainer )
  | ({ type: key.PERFORM_LOGOUT_REQUEST } & RequestContainer<NoRequestParameters> )
  | ({ type: key.PERFORM_LOGOUT_SUCCESS } & RequestContainer<NoRequestParameters> )
  | ({ type: key.PERFORM_LOGOUT_ERROR } & RequestContainer<NoRequestParameters> & ErrorContainer )
  | ({ type: key.GET_USER_REQUEST } & RequestContainer<NoRequestParameters> )
  | ({ type: key.GET_USER_SUCCESS } & RequestContainer<NoRequestParameters> & ResponseContainer<UserData> )
  | ({ type: key.GET_USER_ERROR } & RequestContainer<NoRequestParameters> & ErrorContainer )
  | ({ type: key.UPDATE_TOUR_META_DATA_REQUEST } & RequestContainer<UpdateTourMetaDataParameters> )
  | ({ type: key.UPDATE_TOUR_META_DATA_SUCCESS } & RequestContainer<UpdateTourMetaDataParameters> )
  | ({ type: key.UPDATE_TOUR_META_DATA_ERROR } & RequestContainer<UpdateTourMetaDataParameters> & ErrorContainer )
  | ({ type: key.CHANGE_LINE_COLORS})
  ;

export type NoRequestParameters = {};
