import { TourState } from '../stateTypes.intf';
import { Action } from '../actionTypes.intf';
import key from '../actionTypeKeys.intf';

/**
 * @summary initial state for the subtree 'tours'
 */
export const emptyInitialTourState: TourState = {
    tourList: [],
    tourData: null,
    isLoadingTourMetadata: false,
    isLoadingTourData: true,
    accessDenied: false,
};

/**
 * @summary reducer function for the state subtree 'tours'
 */
export function toursReducer(tourState: TourState = emptyInitialTourState, action: Action): TourState {
    switch (action.type) {
        case key.FETCH_TOUR_LIST_METADATA_REQUEST: {
            return Object.assign({}, tourState, { isLoadingTourMetadata: true });
        }
        case key.FETCH_TOUR_LIST_METADATA_SUCCESS: {
            return Object.assign({}, tourState, { tourList: action.response, tourData: tourState.tourData, isLoadingTourMetadata: false });
        }
        case key.FETCH_TOUR_LIST_METADATA_UNAUTHORIZED: {
            return Object.assign({}, tourState, { isLoadingTourMetadata: false });
        }
        case key.FETCH_TOUR_LIST_METADATA_ERROR: {
            return Object.assign({}, tourState, { isLoadingTourMetadata: false });
        }
        case key.FETCH_TOUR_DATA_REQUEST: {
            return Object.assign({}, tourState, { isLoadingTourData: true });
        }
        case key.FETCH_TOUR_DATA_SUCCESS: {
            const responseTourData = action.response;
            return Object.assign({}, tourState, { tourList: tourState.tourList, tourData: responseTourData, isLoadingTourData: false, accessDenied: false });
        }
        case key.FETCH_TOUR_DATA_ERROR: {
            return Object.assign({}, tourState, { isLoadingTourData: false, accessDenied: true });
        }
        case key.UPDATE_TOUR_META_DATA_REQUEST: {
            return Object.assign({}, tourState, { isLoadingTourMetadata: true });
        }
        case key.UPDATE_TOUR_META_DATA_SUCCESS: {
            let newState = Object.assign(
                {},
                tourState);
            newState.isLoadingTourMetadata = false;
            const currentTour = newState.tourData;
            currentTour!.title = action.request.name;
            currentTour!.description = action.request.description;
            currentTour!.public = action.request.public;
            return newState;
        }
        case key.UPDATE_TOUR_META_DATA_ERROR: {
            return Object.assign({}, tourState, { isLoadingTourMetadata: false });
        }
        case key.CHANGE_LINE_COLORS: {
            var result: TourState = emptyInitialTourState;
            if (tourState.tourData != null) {
                result = Object.assign({}, tourState, { 
                    tourData: { 
                        tourId: tourState.tourData.tourId,
                        userId: tourState.tourData.userId,
                        title: tourState.tourData.title,
                        description: tourState.tourData.description,
                        public: tourState.tourData.public,
                        coordinates: tourState.tourData.coordinates,
                        velocity: tourState.tourData.velocity,
                        accelerationMap: tourState.tourData.accelerationMap,
                        velocityMap: tourState.tourData.velocityMap,
                        altitude: tourState.tourData.altitude,
                        time: tourState.tourData.time,
                        showDistance: !tourState.tourData.showDistance, 
                        showVelocity: !tourState.tourData.showVelocity } });
            }
            return result;
        }
        default:
            return tourState;
    }
}
