import { NoRequestParameters, Action } from '../actionTypes.intf';
import { TourMetaData, TourData } from '../stateTypes.intf';
import { TourRequestParameters } from './types.intf';
import API from '../../api/API';
import { ApiActionGroup, NoExpectedResultType } from '../../api/types.intf';
import key from '../actionTypeKeys.intf';

export type ExpectedFetchTourDataType = {
  tourData: TourData
};

export const changeLineColors = (): Action => ({
  type: key.CHANGE_LINE_COLORS,
});

/* Functions used to fetch Tour Meta Data from the API */
const fetchTourListApiActionGroup: ApiActionGroup<NoRequestParameters, TourMetaData[]> = {
  request: (request) =>
    ({ type: key.FETCH_TOUR_LIST_METADATA_REQUEST, request }),
  success: (response, request) =>
    ({ type: key.FETCH_TOUR_LIST_METADATA_SUCCESS, request, response }),
  unauthorized: (request) =>
    ({ type: key.FETCH_TOUR_LIST_METADATA_UNAUTHORIZED, request }),
  error: (error, request) =>
    ({ type: key.FETCH_TOUR_LIST_METADATA_ERROR, request, error }),
};

/**
 * API call to fetch all public tours
 */
export function fetchTourList() {
  return API.fetchTourList(
    fetchTourListApiActionGroup,
    transformApiResponseIntoTourMetaData);
}

/**
 * API Call to fetch all tours of the logged in user
 */
export function fetchUserTourList() {
  return API.fetchUserTourList(
    fetchTourListApiActionGroup,
    transformApiResponseIntoTourMetaData);
}

/**
 * 
 * @param responseData Response from API call
 */
export function transformApiResponseIntoTourMetaData(responseData: any): TourMetaData[] {
  var tourList: TourMetaData[] = [];
  for (var i = 0; i < responseData.length; i++) {
    tourList[i] = {
      title: responseData[i].name,
      tourId: responseData[i].id,
      description: responseData[i].description,
      public: responseData[i].public,
      comments: [{
        text: '',
        rating: 0,
        timestamp: 0
      }]
    };
  }
  return tourList;
}

/* Functions used to fetch Tour Data from the API */

const fetchTourDataApiActionGroup: ApiActionGroup<TourRequestParameters, TourData> = {
  request: (request) =>
    ({ type: key.FETCH_TOUR_DATA_REQUEST, request }),
  success: (response, request) =>
    ({ type: key.FETCH_TOUR_DATA_SUCCESS, request, response }),
  error: (error, request) =>
    ({ type: key.FETCH_TOUR_DATA_ERROR, request, error }),
};

/**
 * API Call to fetch tour data of one specific tour
 * @param requestParameters TourId for the api call
 */
export function fetchTourData(requestParameters: TourRequestParameters) {
  return API.fetchTourData(
    requestParameters,
    fetchTourDataApiActionGroup,
    transformApiResponseIntoTourData);
}

/**
 * Transforms the API response into an object of type TourData
 * @param responseData Response by the API call
 */
export function transformApiResponseIntoTourData(responseData: any): TourData {
  var tourData: TourData = {
    tourId: Number(responseData.id),
    userId: Number(responseData.userId),
    title: responseData.name,
    description: responseData.description,
    public: responseData.public,
    coordinates: [[]],
    velocity: [],
    accelerationMap: [],
    velocityMap: [],
    altitude: [],
    time: [],
    showDistance: true,
    showVelocity: false
  };
  
  tourData.time[0] = 0;
  for (var i = 1; i < responseData.log.length; i++) {
    tourData.time[i] = i / 60;
  }

  tourData.coordinates = mapObjectToTwoDimensionalArray(responseData.log, ['latitude', 'longitude']);
  tourData.altitude = responseData.log.map((item: any) => item.altitude);
  tourData.velocity = responseData.log.map((item: any) => item.velocity);
  tourData.velocityMap = createColorGradient(tourData.velocity);

  return tourData;
}

export function createColorGradient(data: number[]) {
  var returnValue: string[] = [];
  var colorStart = 'rgb(0,0,255)';
  var colorEnd = 'rgb(255,0,0';
  var max = Math.max.apply(Math, data);
  var colorGradient = interpolateColors(colorStart, colorEnd, max);

  for (var i = 0; i < data.length - 1; i++) {
    returnValue[i] = data[i].toFixed(0) ? colorGradient[Math.abs(data[i]).toFixed(0)] : returnValue[i - 1];
  }

  return returnValue;
}

function interpolateColors(color1: any, color2: any, steps: number) {
  var stepFactor = 1 / (steps - 1),
    interpolatedColorArray = [];

  color1 = color1.match(/\d+/g).map(Number);
  color2 = color2.match(/\d+/g).map(Number);

  for (var i = 0; i < steps; i++) {
    interpolatedColorArray.push(interpolateColor(color1, color2, stepFactor * i));
  }

  return interpolatedColorArray;
}

function interpolateColor(color1: any, color2: any, factor: number) {
  if (arguments.length < 3) {
    factor = 0.5;
  }
  var result = color1.slice();
  var resultString = 'rgb(';
  for (var i = 0; i < 3; i++) {
    result[i] = Math.round(result[i] + factor * (color2[i] - color1[i]));
    if (i < 2) {
      resultString += result[i] + ',';
    } else {
      resultString += result[i] + ')';
    }
  }
  return resultString;
}

/**
 * Maps an array of objects into a two dimensional array
 * @param arrayOfObjects Array of objects to map specific elements into a two dimensional array
 * @param parametersOfObject The strings in this objects are searched for in the arrayOfObjects to extract their values
 */
export function mapObjectToTwoDimensionalArray(arrayOfObjects: object[], parametersOfObject: string[]) {
  var returnValue = [];

  for (var i = 0; i < arrayOfObjects.length; i++) {
    var items = [];
    for (var j = 0; j < parametersOfObject.length; j++) {
      items[j] = arrayOfObjects[i][parametersOfObject[j]];
    }
    returnValue.push(items);
  }
  return returnValue;
}

export type UpdateTourMetaDataParameters = {
  id: number,
  name: string,
  description: string,
  public: boolean,
};

const updateTourMetaDataApiActionGroup: ApiActionGroup<UpdateTourMetaDataParameters, NoExpectedResultType> = {
  request: (request) =>
    ({ type: key.UPDATE_TOUR_META_DATA_REQUEST, request }),
  success: (response, request) =>
    ({ type: key.UPDATE_TOUR_META_DATA_SUCCESS, request, response }),
  error: (error, request) =>
    ({ type: key.UPDATE_TOUR_META_DATA_ERROR, request, error }),
};

export function updateTourMetaData(requestParameters: UpdateTourMetaDataParameters) {
  return API.updateTourMetaData(
    requestParameters,
    updateTourMetaDataApiActionGroup);
}

const deleteTourApiActionGroup: ApiActionGroup<TourRequestParameters, NoExpectedResultType> = {
  request: (request) =>
    ({ type: key.DELETE_TOUR_REQUEST, request }),
  success: (response, request) =>
    ({ type: key.DELETE_TOUR_SUCCESS, request, response }),
  error: (error, request) =>
    ({ type: key.DELETE_TOUR_ERROR, request, error }),
};

export function deleteTour(requestParameters: TourRequestParameters) {
  return API.deleteTour(
    requestParameters,
    deleteTourApiActionGroup);
}
