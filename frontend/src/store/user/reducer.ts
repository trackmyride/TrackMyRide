import { Action } from '../actionTypes.intf';
import { UserState, UserData } from '../stateTypes.intf';
import key from '../actionTypeKeys.intf';

/**
 * @summary initial state for the subtree 'tours'
 */
export const initialUserState: UserState = {
    isLoggedIn: false,
    username: '',
    userId: -1,
    avatar: '',
};

/**
 * @summary reducer function for the state subtree 'tours'
 */
export function userReducer(userState: UserState = initialUserState, action: Action): UserState {
    switch (action.type) {
        case key.PERFORM_LOGIN_REQUEST: {
            return userState;
        }
        case key.PERFORM_LOGIN_SUCCESS: {
            let newState: UserState;
            newState = getUserState(userState, action.response);
            newState = Object.assign({}, newState, {
                isLoggedIn: true,
            });
            return newState;
        }
        case key.PERFORM_LOGIN_ERROR: {
            console.log(action.error);
            return userState;
        }
        case key.PERFORM_LOGOUT_REQUEST: {
            // console.log('PERFORM_LOGOUT_REQUEST');
            return userState;
        }
        case key.PERFORM_LOGOUT_SUCCESS: {
            // console.log('PERFORM_LOGOUT_SUCCESS');
            let newState: UserState;
            // Works but looks hacky. -> Better solution should be looked for
            newState = Object.assign({}, userState, {
                isLoggedIn: false,
                username: '',
                userId: -1,
                avatar: '',
            });
            console.log('newState, PERFORM_LOGOUT_SUCCESS', newState);
            return newState;
        }
        case key.PERFORM_LOGOUT_ERROR: {
            // console.log('PERFORM_LOGOUT_ERROR');
            console.log(action.error);
            return userState;
        }
        case key.GET_USER_REQUEST: {
            return userState;
        }
        case key.GET_USER_SUCCESS: {
            let newState: UserState;
            newState = getUserState(userState, action.response);
            console.log('newState, GET_USER_SUCCESS', newState);
            return newState;
        }
        case key.GET_USER_ERROR: {
            console.log(action.error);
            return userState;
        }
        default:
            return userState;
    }
}

function getUserState(userState: UserState, userResponse: UserData): UserState {
    let newState: UserState;

    console.log('userResponse', userResponse);

    // Works but looks hacky. -> Better solution should be looked for
    newState = Object.assign({}, userState, {
        isLoggedIn: true,
        username: userResponse.username,
        userId: userResponse.userId,
        avatar: userResponse.avatar,
    });
    return newState;
}
