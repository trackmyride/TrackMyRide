import {
  LoginRequestParameters,
  RegisterRequestParameters,
  getUserApiActionGroup,
  performRegisterApiActionGroup,
  performLogoutApiActionGroup,
  performLoginApiActionGroup
} from './types.intf';
import API from '../../api/API';
import { UserData } from '../stateTypes.intf';

export function performLogin(requestParameters: LoginRequestParameters) {
  return API.performLogin(
    requestParameters,
    performLoginApiActionGroup,
    transformApiResponseIntoUserData);
}

export function performLogout() {
  return API.performLogout(performLogoutApiActionGroup);
}

export function performRegister(requestParameters: RegisterRequestParameters) {
  return API.performRegister(
    requestParameters,
    performRegisterApiActionGroup
  );
}

export function getUser() {
  return API.getUser(
    getUserApiActionGroup,
    transformApiResponseIntoUserData
  );
}

export function transformApiResponseIntoUserData(responseData: any): UserData {
  return {
    username: responseData.username,
    userId: responseData.userId,
    email: responseData.email,
    avatar: responseData.avatar,
  };
}
