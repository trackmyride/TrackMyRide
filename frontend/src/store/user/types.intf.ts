import { ApiActionGroup, NoExpectedResultType } from '../../api/types.intf';
import key from '../actionTypeKeys.intf';
import { NoRequestParameters } from '../actionTypes.intf';
import { UserData } from '../stateTypes.intf';

export type LoginRequestParameters = { username: string, password: string, };

export type RegisterRequestParameters = { username: string, email: string, password: string, avatar: string, };

export const performLoginApiActionGroup: ApiActionGroup<LoginRequestParameters, UserData> = {
    request: (request) =>
        ({ type: key.PERFORM_LOGIN_REQUEST, request }),
    success: (response, request) =>
        ({ type: key.PERFORM_LOGIN_SUCCESS, request, response }),
    error: (error, request) =>
        ({ type: key.PERFORM_LOGIN_ERROR, request, error }),
};

export const performLogoutApiActionGroup: ApiActionGroup<NoRequestParameters, NoExpectedResultType> = {
    request: (request) =>
        ({ type: key.PERFORM_LOGOUT_REQUEST, request }),
    success: (response, request) =>
        ({ type: key.PERFORM_LOGOUT_SUCCESS, request, response }),
    error: (error, request) =>
        ({ type: key.PERFORM_LOGOUT_ERROR, request, error }),
};

export const performRegisterApiActionGroup: ApiActionGroup<RegisterRequestParameters, NoExpectedResultType> = {
    request: (request) =>
        ({ type: key.PERFORM_REGISTER_REQUEST, request }),
    success: (response, request) =>
        ({ type: key.PERFORM_REGISTER_SUCCESS, request, response }),
    error: (error, request) =>
        ({ type: key.PERFORM_REGISTER_ERROR, request, error }),
};

export const getUserApiActionGroup: ApiActionGroup<NoRequestParameters, UserData> = {
    request: (request) =>
        ({ type: key.GET_USER_REQUEST, request }),
    success: (response, request) =>
        ({ type: key.GET_USER_SUCCESS, request, response }),
    error: (error, request) =>
        ({ type: key.GET_USER_ERROR, request, error }),
};
