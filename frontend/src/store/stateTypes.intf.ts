/**
 * Complete ApplicationState interface
 */
export interface ApplicationState {
    tours: TourState;
    user: UserState;
}

/**
 * TourState subtree
 */
export interface TourState {
    tourList: TourMetaData[];
    // tourData contains the data of the current tour
    tourData: TourData | null;
    isLoadingTourMetadata: boolean;
    isLoadingTourData: boolean;
    accessDenied: boolean;
}

export interface TourMetaData {
    title: string;
    tourId: number;
    description: string;
    public: boolean;
    comments: Comment[];
}

export interface TourData {
    tourId: number;
    userId: number;
    title: string;
    description: string;
    public: boolean;
    coordinates: number[][];
    velocity: number[];
    accelerationMap: string[];
    velocityMap: string[];
    altitude: number[];
    time: number[];
    showDistance: boolean;
    showVelocity: boolean;
}

export interface Comment {
    text: string;
    rating: number;
    timestamp: number;
}

/**
 * UserState subtree
 */
export interface UserState {
    isLoggedIn: boolean;
    username: string;
    userId: number;
    avatar: string;
}

export interface UserData {
    username: string;
    userId: number;
    email: string;
    avatar: string;
}
