import * as userActions from '../store/user/actions';
import { UserData } from '../store/stateTypes.intf';
import API from '../api/API';
import { RegisterRequestParameters } from '../store/user/types.intf';

let testUserData: UserData;
let testRegisterRequestParameters: RegisterRequestParameters;

beforeAll(() => {
    testUserData = {
        username: 'myTestUsername@#_!',
        userId: 44,
        email: 'myTestEmail@example.com',
        avatar: 'testAvatarStringDoesNotExist.png',
    };
    testRegisterRequestParameters = {
        username: 'myTestUsername@#_!',
        password: 'testPwNotValid$@89§°',
        email: 'myTestEmail@example.com',
        avatar: 'testAvatarStringDoesNotExist.png',
    };
});

describe('Transform API response into object of type UserData', () => {
    it('should return UserData array', () => {
        expect(userActions.transformApiResponseIntoUserData(testUserData)).toEqual(testUserData);
    });
});

describe('User Actions invoke the corresponding api calls', () => {
    it('calls the API method performLogin', () => {
        API.performLogin = jest.fn().mockImplementation(() => {
            return Promise.resolve({ });
        });
        let apiPerformLogin = API.performLogin as jest.Mock<{}>;
        userActions.performLogin({ username: 'testUserName656', password: 'dkfowuhNotValid' });
        expect(apiPerformLogin.mock.calls.length).toBe(1);
    });

    it('calls the API method getUser', () => {
        API.getUser = jest.fn().mockImplementation(() => {
            return Promise.resolve({ });
        });
        let apiGetUser = API.getUser as jest.Mock<{}>;
        userActions.getUser();
        expect(apiGetUser.mock.calls.length).toBe(1);
    });

    it('calls the API method performLogout', () => {
        API.performLogout = jest.fn().mockImplementation(() => {
            return Promise.resolve({ });
        });
        let apiPerformLogout = API.performLogout as jest.Mock<{}>;
        userActions.performLogout();
        expect(apiPerformLogout.mock.calls.length).toBe(1);
    });

    it('calls the API method performRegister', () => {
        API.performRegister = jest.fn().mockImplementation(() => {
            return Promise.resolve({ });
        });
        let apiPerformRegister = API.performRegister as jest.Mock<{}>;
        userActions.performRegister(testRegisterRequestParameters);
        expect(apiPerformRegister.mock.calls.length).toBe(1);
    });
});
