import * as React from 'react';
import * as Enzyme from 'enzyme';
import { mount } from 'enzyme';
import * as Adapter  from 'enzyme-adapter-react-16';
import { Provider } from 'react-redux';
import TourMap from '../components/Dashboard/TourMap';
import { store } from '../store/configureStore';
// import * as ReactDOM from 'react-dom';

Enzyme.configure({ adapter: new Adapter() });

function setup() {

  const enzymeWrapper = mount(<Provider store={store}> 
                                <TourMap/> 
                              </Provider>);

  return {
    store,
    enzymeWrapper
  };
}

describe('components', () => {
  it('renders without crashing', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find('TourMap').exists() === true);
  });
});
