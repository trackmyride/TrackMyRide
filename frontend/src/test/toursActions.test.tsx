import { mapObjectToTwoDimensionalArray, transformApiResponseIntoTourMetaData, transformApiResponseIntoTourData } from '../store/tours/actions';
import { TourMetaData, TourData } from '../store/stateTypes.intf';

interface Point {
     longitude: number;
     latitude: number;
     accuracy: number;
     altitude: number;
     bearing: number;
     gyroscope: object;
     acceleration: object;
     heading: string;
     orientation: object;
     time: number;
     velocity: number;
}
let expectedCoordinates;

let responseTourList: any;
let expectedResponseTourList: TourMetaData[];

let responseTourData: any;
let expectedResponseTourData: TourData;
let log: Point[];
beforeAll(() => {
    log = [
        {
            acceleration: {x: null, y: null, z: null},
            accuracy: 0,
            altitude : 1247,
            bearing : 0,
            gyroscope: {x: null, y: null, z: null},
            heading : 'heading',
            latitude : 46.5816293,
            longitude: 9.6188665,
            orientation : {azimuth: null, pitch: null, roll: null},
            time : 0,
            velocity : 0
        },
        {
            acceleration: {x: null, y: null, z: null},
            accuracy: 0,
            altitude : 1248,
            bearing : 0,
            gyroscope: {x: null, y: null, z: null},
            heading : 'heading',
            latitude : 47.5816293,
            longitude: 10.6188665,
            orientation : {azimuth: null, pitch: null, roll: null},
            time : 1,
            velocity : 1
        },
        {
            acceleration: {x: null, y: null, z: null},
            accuracy: 0,
            altitude : 1249,
            bearing : 0,
            gyroscope: {x: null, y: null, z: null},
            heading : 'heading',
            latitude : 48.5816293,
            longitude: 11.6188665,
            orientation : {azimuth: null, pitch: null, roll: null},
            time : 2,
            velocity : 2
        },
        {
            acceleration: {x: null, y: null, z: null},
            accuracy: 0,
            altitude : 1250,
            bearing : 0,
            gyroscope: {x: null, y: null, z: null},
            heading : 'heading',
            latitude : 49.5816293,
            longitude: 12.6188665,
            orientation : {azimuth: null, pitch: null, roll: null},
            time : 3,
            velocity : 3
        },
        {
            acceleration: {x: null, y: null, z: null},
            accuracy: 0,
            altitude : 1251,
            bearing : 0,
            gyroscope: {x: null, y: null, z: null},
            heading : 'heading',
            latitude : 50.5816293,
            longitude: 13.6188665,
            orientation : {azimuth: null, pitch: null, roll: null},
            time : 4,
            velocity : 4
        }
    ];

    expectedCoordinates = [[ 46.5816293, 9.6188665 ],
    [ 47.5816293, 10.6188665 ],
    [ 48.5816293, 11.6188665 ],
    [ 49.5816293, 12.6188665 ],
    [ 50.5816293, 13.6188665 ]];

    responseTourList = [{
        description: 'bli',
        name: 'name',
        endTime: '23',
        startTime: 'asdf',
        id: 1,
        userId: 44,
        public: true,
        picture: {
            name: '',
            resource: ''
        }
    }];
    expectedResponseTourList = [{
        title: 'name',
        description: 'bli',
        tourId: 1,
        public: true,
        comments: [{
            text: '',
            rating: 0,
            timestamp: 0
          }],
    }];
    responseTourData = {
        description: 'bli',
        name: 'name',
        endTime: '23',
        startTime: 'asdf',
        id: 1,
        userId: 44,
        public: true,
        picture: {
            name: '',
            resource: ''
        },
        log: log
    };
    expectedResponseTourData = {
        tourId: 1,
        userId: 44,
        title: 'name',
        description: 'bli',
        public: true,
        coordinates: expectedCoordinates,
        velocity: [0, 1, 2, 3, 4],
        accelerationMap: [],
        velocityMap: ['rgb(0,0,255)', 'rgb(85,0,170)', 'rgb(170,0,85)', 'rgb(255,0,0)'],
        altitude: [1247, 1248, 1249, 1250, 1251],
        time: [0, 1 / 60, 2 / 60, 3 / 60, 4 / 60],        
        showDistance: true,
        showVelocity: false
    };
  });

describe('Map array of objects to two dimensional array', () => {
    it('should return two dimensional array', () => {
        var coordinates = mapObjectToTwoDimensionalArray(log, ['latitude', 'longitude']);
        expect(coordinates).toEqual([[ 46.5816293, 9.6188665 ],
            [ 47.5816293, 10.6188665 ],
            [ 48.5816293, 11.6188665 ],
            [ 49.5816293, 12.6188665 ],
            [ 50.5816293, 13.6188665 ]]);
    });
});

describe('Transform API response into object of type TourMetaData', () => {
    it('should return TourMetaData array', () => {
        expect(transformApiResponseIntoTourMetaData(responseTourList)).toEqual(expectedResponseTourList);
    });
});

describe('Transform API response into object of type TourData', () => {
    it('should return TourMetaData array', () => {
        expect(transformApiResponseIntoTourData(responseTourData)).toEqual(expectedResponseTourData);
    });
});
