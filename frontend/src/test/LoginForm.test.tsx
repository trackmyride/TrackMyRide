import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Login from '../components/Login';
import { Provider } from 'react-redux';
import { store } from '../store/configureStore';
import { Route, BrowserRouter } from 'react-router-dom';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <Provider store={store}>
      <BrowserRouter>
         <Route exact={true} path="/Login" component={Login} />
      </BrowserRouter>
    </Provider>,
    div);
});