import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Register from '../components/Register';
import { Provider } from 'react-redux';
import { store } from '../store/configureStore';
import { BrowserRouter, Route } from 'react-router-dom';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
          <Route to="/Register" component={Register} />
        </BrowserRouter>
    </Provider>, 
    div);
});