import * as React from 'react';
import { createStore } from 'redux';
import * as Enzyme from 'enzyme';
import { mount } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import { Provider } from 'react-redux';
import TourDescription from '../components/Dashboard/TourDescription';
import { initialUserState } from '../store/user/reducer';
import { ApplicationState } from '../store/stateTypes.intf';
import { Action } from 'redux';

Enzyme.configure({ adapter: new Adapter() });

function setup() {
  let appStateWithOneTourInList: ApplicationState = {
    tours: {
      tourList: [],
      tourData:
        {
          tourId: 2,
          userId: 44,
          title: 'my Title',
          description: 'my description\nnext row',
          public: false,
          coordinates: [],
          velocity: [],
          accelerationMap: [],
          velocityMap: [],
          altitude: [],
          time: [],        
          showDistance: true,
          showVelocity: false
        },
      isLoadingTourData: false,
      isLoadingTourMetadata: false,
      accessDenied: false,
    },
    user: initialUserState
  };
  let storeWithOneTourInList = createStore(
    reducerDoingNothing,
    appStateWithOneTourInList,
  );

  const enzymeWrapper = mount(<Provider store={storeWithOneTourInList}>
    <TourDescription />
  </Provider>);

  return {
    storeWithOneTourInList,
    enzymeWrapper
  };
}

describe('components', () => {
  it('renders without crashing', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find('TourDescription').exists() === true);
  });
});

function reducerDoingNothing(applicationState: ApplicationState, action: Action): ApplicationState {
  return applicationState;
}
