import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from '../containers/App';
import { Provider } from 'react-redux';
import { store } from '../store/configureStore';
import * as puppeteer from 'puppeteer';
import { } from 'jest';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000;

let page: puppeteer.Page;
let browser: puppeteer.Browser;
const DEBUG = false; // set to true to see a browser :)
const APP = 'http://localhost:3000/';
const width = 1920;
const height = 1080;

beforeAll(async () => {
  browser = await puppeteer.launch({
    headless: !DEBUG,
    slowMo: 80,
    args: [`--window-size=${width},${height}`, `--no-sandbox`, `--disable-setuid-sandbox` ]
  });
  page = await browser.newPage();
  await page.setViewport({ width, height });
  await page.goto(APP);
});

afterAll(() => {
    browser.close();
});

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Provider store={store}><App/></Provider>, div);
});

it('contains all Routing Elements', async () => {
  const div = document.createElement('div');
  ReactDOM.render(<Provider store={store}><App/></Provider>, div);
  const Routes = await page.$eval('div.Routes', (el: Element) => (el ? true : false));
  expect(Routes).toBe(true);
});
it('contains all Navbar Elements', async () => {
  const div = document.createElement('div');
  ReactDOM.render(<Provider store={store}><App/></Provider>, div);
  const Navbar = await page.$eval('div.Navbar', (el: Element) => (el ? true : false));
  expect(Navbar).toBe(true);
});