import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { MapControls } from '../containers/MapControls';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MapControls showDistance={true} showVelocity={false} handleClick={() => true}/>, div);
});