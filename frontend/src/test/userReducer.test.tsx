import { userReducer } from '../store/user/reducer';
import { initialUserState } from '../store/user/reducer';
// import { initialApplicationState } from '../store/configureStore';
import { UserState } from '../store/stateTypes.intf';
import key from '../store/actionTypeKeys.intf';

describe('user reducer', () => {
    it('should handle PERFORM_LOGIN_REQUEST', () => {
        expect(userReducer(initialUserState, {
            type: key.PERFORM_LOGIN_REQUEST,
            request: { username: 'testUserExistsNowhere', password: 'testPassword' },
            }))
            .toEqual(initialUserState);
    });
    it('should handle PERFORM_LOGIN_SUCCESS', () => {
        var expectedUserStateAfterLogin: UserState = {
            isLoggedIn: true,
            username: 'testUserExistsNowhere',
            userId: 44,
            avatar: 'whatever',
            };
        expect(userReducer(initialUserState, {
            type: key.PERFORM_LOGIN_SUCCESS,
            request: { username: 'testUserExistsNowhere', password: 'testPassword' },
            response: { 
                username: 'testUserExistsNowhere',
                userId: 44,
                email: 'testUser@example.com',
                avatar: 'whatever',
            },
            }))
            .toEqual(expectedUserStateAfterLogin);
    });
    it('should handle PERFORM_LOGIN_ERROR', () => {
        expect(userReducer(initialUserState, {
            type: key.PERFORM_LOGIN_ERROR,
            request: { username: 'testUserExistsNowhere', password: 'testPassword' },
            error: {name: 'asdf', message: 'asdfsdf'},
            }))
            .toEqual(initialUserState);
    });
    it('should handle PERFORM_LOGOUT_REQUEST', () => {
        expect(userReducer(initialUserState, {
            type: key.PERFORM_LOGOUT_REQUEST,
            request: {},
            }))
            .toEqual(initialUserState);
    });
    it('should handle PERFORM_LOGOUT_SUCCESS', () => {
        var expectedUserStateAfterLogout: UserState;
        expectedUserStateAfterLogout = Object.assign({}, initialUserState, {
            isLoggedIn: false,
            username: '',
            });
        expect(userReducer(initialUserState, {
            type: key.PERFORM_LOGOUT_SUCCESS,
            request: {},
            }))
            .toEqual(expectedUserStateAfterLogout);
    });
    it('should handle PERFORM_LOGOUT_ERROR', () => {
        expect(userReducer(initialUserState, {
            type: key.PERFORM_LOGOUT_ERROR,
            request: {},
            error: {name: 'asdf', message: 'asdfsdf'},
            }))
            .toEqual(initialUserState);
    });
    it('should handle PERFORM_REGISTER_REQUEST', () => {
        expect(userReducer(initialUserState, {
            type: key.PERFORM_REGISTER_REQUEST,
            request: { username: 'testUserExistsNowhere', email: 'test@example.com', password: 'testPassword', avatar: 'testAvatar.png' },
            }))
            .toEqual(initialUserState);
    });
    it('should handle PERFORM_REGISTER_SUCCESS', () => {
        expect(userReducer(initialUserState, {
            type: key.PERFORM_REGISTER_SUCCESS,
            request: { username: 'testUserExistsNowhere', email: 'test@example.com', password: 'testPassword', avatar: 'testAvatar.png' },
            }))
            .toEqual(initialUserState);
    });
    it('should handle PERFORM_REGISTER_ERROR', () => {
        expect(userReducer(initialUserState, {
            type: key.PERFORM_REGISTER_ERROR,
            request: { username: 'testUserExistsNowhere', email: 'test@example.com', password: 'testPassword', avatar: 'testAvatar.png' },
            error: {name: 'asdf', message: 'asdfsdf'},
            }))
            .toEqual(initialUserState);
    });
    it('should handle GET_USER_REQUEST', () => {
        expect(userReducer(initialUserState, {
            type: key.GET_USER_REQUEST,
            request: { },
            }))
            .toEqual(initialUserState);
    });
    it('should handle GET_USER_SUCCESS', () => {
        var expectedUserStateAfterGetUser: UserState;
        expectedUserStateAfterGetUser = Object.assign({}, initialUserState, {
            isLoggedIn: true,
            username: 'anExampleUserName',
            userId: 44,
            avatar: 'anExampleAvatar.png',
            });
        expect(userReducer(initialUserState, {
            type: key.GET_USER_SUCCESS,
            request: { },
            response: { username: 'anExampleUserName', userId: 44, email: 'anExampleEmail@example.com' , avatar: 'anExampleAvatar.png' },
            }))
            .toEqual(expectedUserStateAfterGetUser);
    });
    it('should handle GET_USER_ERROR', () => {
        expect(userReducer(initialUserState, {
            type: key.GET_USER_ERROR,
            request: { },
            error: {name: 'asdf', message: 'asdfsdf'},
            }))
            .toEqual(initialUserState);
    });
  });
