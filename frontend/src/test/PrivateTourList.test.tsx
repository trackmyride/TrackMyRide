import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { store } from '../store/configureStore';
import { Route, BrowserRouter } from 'react-router-dom';

import * as puppeteer from 'puppeteer';
import PrivateTourList from '../components/TourList/PrivateTourList';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000;

let page: puppeteer.Page;
let browser: puppeteer.Browser;
const DEBUG = false; // set to true to see a browser :)
const APP = 'http://localhost:3000/';
const width = 1920;
const height = 1080;

beforeAll(async () => {
    browser = await puppeteer.launch({
        headless: !DEBUG,
        slowMo: 80,
        args: [`--window-size=${width},${height}`, `--no-sandbox`, `--disable-setuid-sandbox`]
    });
    page = await browser.newPage();
    await page.setViewport({ width, height });
    await page.goto(APP);
});

afterAll(() => {
    browser.close();
});

it('renders PublicTourListComponent without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(
        <Provider store={store}>
            <BrowserRouter>
                <Route exact={true} path="/MeineTouren" component={PrivateTourList} />
            </BrowserRouter>
        </Provider>,
        div);
});