import { store, initialApplicationState } from '../store/configureStore';
import { ApplicationState } from '../store/stateTypes.intf';
let state: ApplicationState = initialApplicationState;
beforeAll(() => {
    state = store.getState();
});

describe('Store', () => {

    it('should render self and subcomponents', () => {
        expect(state).toEqual(initialApplicationState);
    });
    
    it('Initial tour state is set correctly', () => {
        expect(state.tours.tourData).toEqual(null);
        expect(state.tours.tourList).toEqual([]);
        expect(state.tours.isLoadingTourData).toEqual(true);
        expect(state.tours.isLoadingTourMetadata).toEqual(false);
    });

    it('Initial login state is set correctly', () => {
        expect(state.user.isLoggedIn).toEqual(false);
        expect(state.user.username).toEqual('');
    });

});
