import { toursReducer } from '../store/tours/reducer';
import { emptyInitialTourState } from '../store/tours/reducer';
// import { initialApplicationState } from '../store/configureStore';
import { TourMetaData, TourData, TourState } from '../store/stateTypes.intf';
import key from '../store/actionTypeKeys.intf';

var mockTourData: TourData;
var mockTourList: TourMetaData[];
var expectedStateTourList: object;
var expectedStateUnauthorised: object;

export const formerInitialTourState: TourState = {
    tourList:
        [
            {
                title: 'Tour 1',
                tourId: 1,
                description: 'Beschreibung von Tour 1',
                public: true,
                comments: [{
                    text: 'bli',
                    rating: 4,
                    timestamp: 2134123412341,
                }]
            },
            {
                title: 'Tour 2',
                tourId: 2,
                description: 'Beschreibung von Tour 2',
                public: true,
                comments: [{
                    text: 'bla',
                    rating: 3,
                    timestamp: 2135673412341,
                }]
            }
        ],
    tourData:
        {
            tourId: 1,
            userId: 44,
            title: 'Tour 1',
            description: 'Beschreibung von Tour 1',
            public: true,
            coordinates: [[51.48127, -0.18431], [51.48012, -0.18082], [51.48015, -0.17969],
            [51.48031, -0.17823], [51.48061, -0.17754], [51.48181, -0.17578],
            [51.48315, -0.16844], [51.48341, -0.1675], [51.48357, -0.16733],
            [51.48349, -0.16651], [51.48344, -0.166], [51.48553, -0.15158], [51.4859, -0.1499],
            [51.48569, -0.14913], [51.48574, -0.14651], [51.48547, -0.14329],
            [51.48495, -0.1406], [51.48486, -0.13952], [51.48577, -0.13415], [51.48887, -0.13046],
            [51.49451, -0.14155], [51.50255, -0.15141], [51.50212, -0.15965],
            [51.49685, -0.16911], [51.49487, -0.19572], [51.49164, -0.20359], [51.491, -0.21114],
            [51.49143, -0.2187], [51.4925, -0.22831], [51.49271, -0.23346]],
            velocity: [14, 45, 34, 50, 45, 60, 75, 50, 45, 30, 14, 45, 34, 50, 45, 60, 75, 50, 45, 30, 14, 45, 34, 50, 45, 60, 75, 50, 45, 30],
            accelerationMap: ['blue', 'red', 'blue', 'red', 'blue', 'red', 'blue', 'red', 'blue', 'red',
                'blue', 'red', 'blue', 'red', 'blue', 'red', 'blue', 'red', 'blue', 'red',
                'blue', 'red', 'blue', 'red', 'blue', 'red', 'blue', 'red', 'blue', 'red'
            ],
            velocityMap: ['green', 'red', 'green', 'red', 'green', 'red', 'green', 'red', 'green', 'red',
                'green', 'red', 'green', 'red', 'green', 'red', 'green', 'red', 'green', 'red',
                'green', 'red', 'green', 'red', 'green', 'red', 'green', 'red', 'green', 'red'
            ],
            altitude: [400, 450, 480, 500, 460, 420, 480, 510, 540, 600, 400, 450, 480, 500, 460, 420, 480, 510, 540, 600, 400, 450, 480, 500, 460, 420, 480, 510, 540, 600],
            time: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30],
            showDistance: true,
            showVelocity: false
        },
    isLoadingTourData: true,
    isLoadingTourMetadata: true,
    accessDenied: false,
};

describe('tours reducer', () => {
    it('should handle FETCH_TOUR_LIST_METADATA_REQUEST', () => {
        var expectedTourstate = emptyInitialTourState;
        expectedTourstate.isLoadingTourMetadata = true;
        expect(toursReducer(emptyInitialTourState, {
            type: key.FETCH_TOUR_LIST_METADATA_REQUEST,
            request: {}
        }))
            .toEqual(expectedTourstate);
    });
    it('should handle FETCH_TOUR_LIST_METADATA_UNAUTHORIZED', () => {
        expect(toursReducer(emptyInitialTourState, {
            type: key.FETCH_TOUR_LIST_METADATA_UNAUTHORIZED,
            request: {}
        }))
            .toEqual(expectedStateUnauthorised);
    });

    it('should handle FETCH_TOUR_LIST_METADATA_SUCCESS', () => {

        expect(toursReducer(Object.assign({}, emptyInitialTourState, { isLoadingTourMetadata: true }), {
            type: key.FETCH_TOUR_LIST_METADATA_SUCCESS,
            request: {},
            response: mockTourList
        }))
            .toEqual(expectedStateTourList);
    });
    it('should handle FETCH_TOUR_LIST_METADATA_ERROR', () => {
        var expectedTourstate = emptyInitialTourState;
        expectedTourstate.isLoadingTourMetadata = false;
        expect(toursReducer(emptyInitialTourState, {
            type: key.FETCH_TOUR_LIST_METADATA_ERROR,
            request: {},
            error: { name: 'asdf', message: 'asdfsdf' }
        }))
            .toEqual(expectedTourstate);
    });
    it('should handle FETCH_TOUR_DATA_REQUEST', () => {
        var expectedTourstate = emptyInitialTourState;
        expectedTourstate.isLoadingTourData = true;
        expect(toursReducer(emptyInitialTourState, {
            type: key.FETCH_TOUR_DATA_REQUEST,
            request: { tourId: 1 }
        }))
            .toEqual(expectedTourstate);
    });
    it('should handle FETCH_TOUR_DATA_SUCCESS', () => {

        var initialTourStateWithoutTourData: TourState = {
            tourList: [],
            tourData: {
                tourId: -1, userId: -1, title: '', description: '', public: false, coordinates: [], velocity: [], accelerationMap: [], velocityMap: [], altitude: [], time: []
                , showDistance: true,
                showVelocity: false
            },
            isLoadingTourData: true,
            isLoadingTourMetadata: true,
            accessDenied: false,
        };
        var expectedTourState: TourState = {
            tourList: [],
            tourData: mockTourData,
            isLoadingTourData: false,
            isLoadingTourMetadata: true,
            accessDenied: false,
        };

        expect(
            toursReducer(initialTourStateWithoutTourData, {
                type: key.FETCH_TOUR_DATA_SUCCESS,
                request: { tourId: 1 },
                response: mockTourData
            })
        )
        .toEqual(expectedTourState);
    });
    it('should handle FETCH_TOUR_DATA_ERROR', () => {
        var expectedTourstate = emptyInitialTourState;
        expectedTourstate.isLoadingTourData = false;
        expectedTourstate.accessDenied = true;
        expect(toursReducer(emptyInitialTourState, {
            type: key.FETCH_TOUR_DATA_ERROR,
            request: { tourId: 1 },
            error: { name: 'asdf', message: 'asdfsdf' }
        }))
            .toEqual(expectedTourstate);
    });
});

beforeEach(() => {
    mockTourData = {
        tourId: 1,
        userId: 44,
        title: 'Tour 1',
        description: 'Beschreibung von Tour 1',
        public: true,
        coordinates: [[51.48127, -0.18431], [51.48012, -0.18082]],
        velocity: [14, 45],
        accelerationMap: ['blue', 'red'],
        velocityMap: ['green', 'red'],
        altitude: [400, 450],
        time: [1, 2],
        showDistance: true,
        showVelocity: false
    };
    mockTourList = [{
        title: 'Tour 1',
        tourId: 1,
        description: 'Beschreibung von Tour 1',
        public: true,
        comments: [{
            text: 'bli',
            rating: 4,
            timestamp: 2134123412341,
        }]
    },
    {
        title: 'Tour 2',
        tourId: 2,
        description: 'Beschreibung von Tour 2',
        public: true,
        comments: [{
            text: 'bla',
            rating: 3,
            timestamp: 2135673412341,
        }]
    }
    ];
    
    expectedStateTourList = Object.assign({}, emptyInitialTourState, {
        tourList: mockTourList,
        tourData: emptyInitialTourState.tourData,
        isLoadingTourData: true,
        isLoadingTourMetadata: false,
    });

    expectedStateUnauthorised = Object.assign({}, emptyInitialTourState, {
        tourData: null, 
        tourList: [],
        isLoadingTourData: true,
        isLoadingTourMetadata: false,
    });
});
