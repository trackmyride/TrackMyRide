import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { store } from '../store/configureStore';
import { Route, BrowserRouter } from 'react-router-dom';
import PublicTourList from '../components/TourList/PublicTourList';
import * as puppeteer from 'puppeteer';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000;

let page: puppeteer.Page;
let browser: puppeteer.Browser;
const DEBUG = false; // set to true to see a browser :)
const APP = 'http://localhost:3000/';
const width = 1920;
const height = 1080;

beforeAll(async () => {
    browser = await puppeteer.launch({
        headless: !DEBUG,
        slowMo: 80,
        args: [`--window-size=${width},${height}`, `--no-sandbox`, `--disable-setuid-sandbox`]
    });
    page = await browser.newPage();
    await page.setViewport({ width, height });
    await page.goto(APP);
});

afterAll(() => {
    browser.close();
});

it('renders PublicTourListComponent without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(
        <Provider store={store}>
            <BrowserRouter>
                <Route exact={true} path="/AllTours" component={PublicTourList} />
            </BrowserRouter>
        </Provider>,
        div);
});

it('contains all AllTour Element', async () => {
    const div = document.createElement('div');
    ReactDOM.render(<Provider store={store}><BrowserRouter><div><Route exact={true} path="/AllTours" component={PublicTourList} /></div></BrowserRouter></Provider>, div);
    const AllTours = await page.$eval('div.AllTours', (el: Element) => (el ? true : false));
    expect(AllTours).toBe(true);
});

it('contains Leadtext Element', async () => {
    const div = document.createElement('div');
    ReactDOM.render(<Provider store={store}><BrowserRouter><div> <Route exact={true} path="/AllTours" component={PublicTourList} /></div></BrowserRouter></Provider>, div);
    const lead = await page.$eval('p.text', (el: Element) => (el ? true : false));
    expect(lead).toBe(true);
});
