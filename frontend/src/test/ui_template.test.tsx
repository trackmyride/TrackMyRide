import * as puppeteer from 'puppeteer';
import { } from 'jest';

// based on
// https://www.valentinog.com/blog/ui-testing-jest-puppetteer/

jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000;

const DEBUG = false; // set to true to see a browser :)
const APP = 'http://localhost:3000/';
const width = 1920;
const height = 1080;

let page: puppeteer.Page;
let browser: puppeteer.Browser;

beforeAll(async () => {
  browser = await puppeteer.launch({
    headless: !DEBUG,
    slowMo: 80,
    args: [`--window-size=${width},${height}`, `--no-sandbox`, `--disable-setuid-sandbox` ]
  });
  page = await browser.newPage();
  await page.setViewport({ width, height });
  await page.goto(APP);
});

afterAll(() => {
  browser.close();
});

describe('Start page', () => {
  test(
    'assert that a div named MapContainer exists', async () => {
      //                                     vvv this is a CSS selector!!!!
      const dashboard = await page.$eval('div.AllTours', (el: Element) => (el ? true : false));
      expect(dashboard).toBe(true);
    }
  );
});
