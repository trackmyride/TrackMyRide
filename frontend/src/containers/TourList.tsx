import * as React from 'react';
import '../css/App.css';
import { ListGroup, ListGroupItem, Row, Col, ListGroupItemHeading, ListGroupItemText } from 'reactstrap';
import { TourMetaData } from '../store/stateTypes.intf';
import Rating from 'react-rating';
import 'font-awesome/css/font-awesome.min.css';
import { CSSProperties } from 'react';
var FontAwesome = require('react-fontawesome');

interface TourListState {
    id: string;
    tourList: TourMetaData[];
    handleClick: (tourId: number) => void;
}

export const TourList: React.SFC<TourListState> = ({id, tourList, handleClick }) => (
        <ListGroup id={id} style={ListStyle}>
            {tourList.map((tour: TourMetaData) => (
                <ListGroupItem
                    key={tour.tourId}
                    id={tour.tourId.toString()}
                    onClick={() => handleClick(tour.tourId)}
                >
                    <Row>
                        <Col xs="10">
                            <ListGroupItemHeading>{tour.title}</ListGroupItemHeading>
                            <ListGroupItemText style={ListGroupItemStyle}>{tour.description}</ListGroupItemText>
                        </Col>
                        <Col xs="2">
                            <Row>

                                <Col xs="8">
                                    <Rating
                                        emptySymbol="fa fa-star-o fa"
                                        fullSymbol="fa fa-star fa"
                                        readonly={true}
                                        initialRating={tour.comments.map((item) => item.rating).reduce((previous, current) => current += previous)}
                                    />
                                </Col>
                                <Col xs="4">
                                    {tour.public ? <FontAwesome name="globe" /> : <FontAwesome name="lock" />}
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </ListGroupItem>
            ))}
        </ListGroup>
);

const ListStyle: CSSProperties = {
    overflowY: 'auto'
};

const ListGroupItemStyle: CSSProperties = {
    width: '300px',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
};