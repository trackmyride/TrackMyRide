import * as React from 'react';
import '../css/App.css';
import '../css/bootstrap.min.css';
import Login from '../components/Login';
import Logout from '../components/Logout';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import PublicTourList from '../components/TourList/PublicTourList';
import PrivateTourList from '../components/TourList/PrivateTourList';
import { ApplicationState } from '../store/stateTypes.intf';
import { BrowserRouter, Route, Switch, NavLink as RRNavLink } from 'react-router-dom';
import { connect, Dispatch } from 'react-redux';
import { performLogout } from '../store/user/actions';
import Register from '../components/Register';
import Dashboard from '../components/Dashboard/Dashboard';
let logo = require('../assets/Icon_192.png');
let toggler = require('../assets/toggler.png');
let downloadLogo = require('../assets/google-play-store.svg');

export interface ConnectedDispatch {
  dispatchPerformLogout: () => void;
}

const mapDispatchToProps = (dispatch: Dispatch<ApplicationState>): ConnectedDispatch => ({
  dispatchPerformLogout: () => dispatch(performLogout())
});

const mapStateToProps = (state: ApplicationState) => (state);

class App extends React.Component<ApplicationState & ConnectedDispatch, {}> {

  state = {
    isOpen: false
  };

  constructor(props: ApplicationState & ConnectedDispatch) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.handleOnClickLoginLogout = this.handleOnClickLoginLogout.bind(this);
    this.state = {
      isOpen: false
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  handleOnClickLoginLogout() {
    if (this.props.user.isLoggedIn) {
      this.props.dispatchPerformLogout();
    }
  }

  render() {
    let stringPath = '';
    let stringLoginOrLogoutRedirectTo = '';
    let registerNavItemOrEmpty: JSX.Element | undefined = undefined;
    let userIconOrEmpty: JSX.Element | undefined = undefined;
    if (this.props.user.isLoggedIn) {
      stringPath = 'Logout';
      stringLoginOrLogoutRedirectTo = '/AllTours';
      if (this.props.user.avatar) {
        userIconOrEmpty =
          <img alt="user-icon" src={this.props.user.avatar} style={{ marginLeft: '1vw', objectFit: 'cover', borderRadius: '50%' }} height="42" width="42" />;
      }

    } else {
      stringPath = 'Login';
      stringLoginOrLogoutRedirectTo = '/Login';
      registerNavItemOrEmpty =
        <NavItem>
          <NavLink><RRNavLink to="/Register" style={{ textDecoration: 'none', color: 'white' }}>Registrieren</RRNavLink></NavLink>
        </NavItem>;
    }

    return (
      <div style={AppStyle}>
      <BrowserRouter>
        <div className="Navbar">
          <Navbar dark={false} light={false} expand="md" style={NavBarStyle}>
            <NavbarBrand href="/" style={{ textDecoration: 'none', color: 'white' }}>
              <img alt="Logo" src={logo} height="44" width="44" />
              <h2 style={{display: 'inline', position: 'relative', top: 6, marginLeft: 2}}> TrackMyRide </h2>
            </NavbarBrand>
            <NavbarToggler onClick={this.toggle} className="mr-2">
              <img alt="toggler" src={toggler} height="34" width="34" />
            </NavbarToggler>
            <Collapse isOpen={this.state.isOpen} navbar={true}>
              <Nav className="ml-auto" navbar={true} style={navigationMiddle}>
                <NavItem>
                  <RRNavLink
                    to="/AllTours"
                    style={{ textDecoration: 'none', color: 'white' }}
                  ><NavLink>
                      Alle Touren
                      </NavLink>
                  </RRNavLink>
                </NavItem>
                <NavItem>

                  <RRNavLink
                    to="/MyTours"
                    style={{
                      textDecoration: 'none', color: 'white'
                    }}
                  > <NavLink>
                      Meine Touren
                    </NavLink>
                  </RRNavLink>
                </NavItem>
              </Nav>
              <Nav className="ml-auto" navbar={true} style={navigationRight}>
                <NavItem>
                  <NavLink>
                    <RRNavLink
                      to={stringLoginOrLogoutRedirectTo}
                      onClick={this.handleOnClickLoginLogout}
                      style={{ textDecoration: 'none', color: 'white' }}
                    >
                      {stringPath}
                    </RRNavLink>
                  </NavLink>
                </NavItem>
                {registerNavItemOrEmpty}
                {userIconOrEmpty}
              </Nav>
            </Collapse>
          </Navbar>
         
          <div className="Routes" style={AppContainerStyle}>
            <Switch>
              <div style={AppContentStyle} >
              <Route exact={true} path="/Login"  component={Login} />
              <Route exact={true} path="/Logout" component={Logout} />
              <Route exact={true} path="/Register" component={Register} />
              <Route exact={true} default={true} path="/" component={PublicTourList} />
              <Route exact={false} default={true} path="/AllTours" component={PublicTourList} />
              <Route exact={true} path="/MyTours" component={PrivateTourList} />
              <Route exact={false} path="/Dashboard/" component={Dashboard} />
              </div>
            </Switch>
            </div>
                 
        <div className="Footer" style={FooterStyle}>
            <a download={true} href="/__download/app-release.apk" style={FooterLinkStyle}>
                <img className="img-fluid" alt="Logo" src={downloadLogo} style={FooterImageStyle}/>
            </a>
        </div>
        </div >
      </BrowserRouter >
      </div>
    );
  }
}

const AppStyle: React.CSSProperties = {
  height: '100%',
  width: '100%',
  margin : 'auto',
  overflowY: 'hidden',  
  overflowX: 'hidden',
  flexDirection: 'column',
};

const AppContainerStyle: React.CSSProperties = {
  width: '100%',
  height: '100%',
  overflowX: 'hidden',
  overflowY: 'hidden',
  position: 'fixed',
  backgroundColor: 'Light',
  display: 'flex',
  justifyContent: 'center',
};

const AppContentStyle: React.CSSProperties = {
  width: '98%',
  height: '85%',
  overflowY: 'hidden',
  margin: '10px auto 68px auto',
  padding: '10px',
  border: '2px solid #d8d8d8',  
};

const FooterStyle: React.CSSProperties = {
  display: 'flex',
  height: '5%',
  width: '100%',
  backgroundColor: '#012674',
  textAlign: 'center',
  padding: '20px',
  zIndex: 9999,
  position: 'fixed',
  left: '0',
  bottom: '0',
  justifyContent: 'center',
  alignContent: 'center',
  alignItems: 'center'
};

const FooterLinkStyle: React.CSSProperties = {
  textDecoration: 'none', 
  color: 'white', 
  display: 'inline' , 
  position: 'fixed', 
  margin: 'auto auto auto auto',
};

const FooterImageStyle: React.CSSProperties = {
  height: '90%',
  width: '20%',
  padding: '2px',
  margin: '2px auto auto 2px',
};

const NavBarStyle: React.CSSProperties = {
  display: 'flex',
  alignItems: 'center', 
  backgroundColor: '#012674',
  width: '100%',
};

const navigationRight: React.CSSProperties = {
  float: 'right'
};

const navigationMiddle: React.CSSProperties = {
  float: 'left'
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
