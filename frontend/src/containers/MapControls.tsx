import * as React from 'react';
import '../css/App.css';
import { Form, FormGroup, Label, Input, Container } from 'reactstrap';

interface MapControlsState {
  showDistance: boolean;
  showVelocity: boolean;
  handleClick: () => void;
}

export const MapControls: React.SFC<MapControlsState> = ({ showDistance, showVelocity, handleClick }) => (
  <Container style={MapCointainerControlsStyle}>
    <Form inline={true} >
      <FormGroup>
        <FormGroup check={true} style={MapCointainerControlRadioStyle}>
          <Label check={true}>
            <Input
              type="radio"
              name="disp"
              value="Strecke"
              checked={showDistance}
              onClick={handleClick}
            />
            Strecke
            </Label>
        </FormGroup>

        <FormGroup check={true} style={MapCointainerControlRadioStyle}>
          <Label check={true}>
            <Input
              type="radio"
              name="disp"
              value="Geschwindigkeit"
              checked={showVelocity}
              onClick={handleClick}
            />
            Geschwindigkeit
            </Label>
        </FormGroup>
      </FormGroup>
    </Form>
  </Container>
);

let MapCointainerControlsStyle = {
  display: 'block',
  height: '50px',
  width: '100%',
  verticalAlign: 'bottom',
  margin: '5px auto 5px auto',
  padding: '2px',
};

let MapCointainerControlRadioStyle = {
  marginLeft: '10px',
};
