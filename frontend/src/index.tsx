import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './containers/App';
import registerServiceWorker from './registerServiceWorker';
import './css/index.css';
import './css/bootstrap.min.css';
import { Provider } from 'react-redux';
import { store } from './store/configureStore';
import { getUser } from './store/user/actions';

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
  ,
  document.getElementById('root'));
registerServiceWorker();

store.dispatch(getUser());
