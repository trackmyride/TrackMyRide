import * as redux from 'redux';
import { ApiActionGroup, ApiMethod, NoExpectedResultType } from './types.intf';
import { NoRequestParameters } from '../store/actionTypes.intf';
import { TourMetaData, TourData, UserData } from '../store/stateTypes.intf';
import { TourRequestParameters } from '../store/tours/types.intf';
import { LoginRequestParameters, RegisterRequestParameters } from '../store/user/types.intf';
import { UpdateTourMetaDataParameters } from '../store/tours/actions';

const API_BASE = '/api';

const TOURS_ENDPOINT = '/tours';
const USER_TOURS_ENDPOINT = '/tours/user';
const TOUR_ENDPOINT_WITHOUT_ID = '/tours/';
const USER_ENDPOINT = '/user';
const LOGIN_ENDPOINT = '/login';
const LOGOUT_ENDPOINT = '/logout';
const REGISTER_ENDPOINT = '/register';
const UPDATE_TOUR_META_DATA_ENDPOINT_WITHOUT_ID = '/tours/';
const DELETE_TOUR_ENDPOINT_WITHOUT_ID = '/tours/';

export default class API {

    public static fetchTourList(
        fetchTourListApiActionGroup: ApiActionGroup<NoRequestParameters, TourMetaData[]>,
        transformApiResponseIntoTourMetaData: ((responseData: any) => TourMetaData[])
    ) {
        return this.apiCallNoParameters<TourMetaData[]>(
            ApiMethod.get,
            TOURS_ENDPOINT,
            fetchTourListApiActionGroup,
            transformApiResponseIntoTourMetaData);
    }

    public static fetchUserTourList(
        fetchTourListApiActionGroup: ApiActionGroup<NoRequestParameters, TourMetaData[]>,
        transformApiResponseIntoTourMetaData: ((responseData: any) => TourMetaData[])
    ) {
        return this.apiCallNoParameters<TourMetaData[]>(
            ApiMethod.get,
            USER_TOURS_ENDPOINT,
            fetchTourListApiActionGroup,
            transformApiResponseIntoTourMetaData);
    }

    public static fetchTourData(
        requestParameters: TourRequestParameters,
        fetchTourDataApiActionGroup: ApiActionGroup<TourRequestParameters, TourData>,
        transformApiResponseIntoTourMetaData: ((responseData: any) => TourData)
    ) {
        return API.apiCall<TourRequestParameters, TourData>(
        ApiMethod.get,
        TOUR_ENDPOINT_WITHOUT_ID + requestParameters.tourId,
        fetchTourDataApiActionGroup,
        transformApiResponseIntoTourMetaData,
        requestParameters);
    }

    public static performLogin(
        requestParameters: LoginRequestParameters,
        performLoginApiActionGroup: ApiActionGroup<LoginRequestParameters, UserData>,
        transformApiResponseIntoUserData: ((responseData: any) => UserData)
    ) {
        return this.apiCall<LoginRequestParameters, UserData>(
            ApiMethod.post,
            LOGIN_ENDPOINT,
            performLoginApiActionGroup,
            transformApiResponseIntoUserData,
            requestParameters
        );
    }

    public static performLogout(
        performLogoutApiActionGroup: ApiActionGroup<NoRequestParameters, NoExpectedResultType>
    ) {
        return API.apiCallNoParametersNoResult(
            ApiMethod.get,
            LOGOUT_ENDPOINT,
            performLogoutApiActionGroup
        );
    }

    public static performRegister(
        requestParameters: RegisterRequestParameters,
        performRegisterApiActionGroup: ApiActionGroup<RegisterRequestParameters, NoExpectedResultType>
    ) {
        return this.apiCallNoResult<RegisterRequestParameters>(
            ApiMethod.post,
            REGISTER_ENDPOINT,
            performRegisterApiActionGroup,
            requestParameters
        );
    }

    public static getUser(
        getUserApiActionGroup: ApiActionGroup<NoRequestParameters, UserData>,
        transformApiResponseIntoUserData: ((responseData: any) => UserData)
    ) {
        return this.apiCallNoParameters<UserData>(
            ApiMethod.get,
            USER_ENDPOINT,
            getUserApiActionGroup,
            transformApiResponseIntoUserData);
    }
    
    public static updateTourMetaData(
        requestParameters: UpdateTourMetaDataParameters,
        updateTourMetaDataApiActionGroup: ApiActionGroup<UpdateTourMetaDataParameters, NoExpectedResultType>
    ) {
        return this.apiCallNoResult<UpdateTourMetaDataParameters>(
            ApiMethod.put,
            UPDATE_TOUR_META_DATA_ENDPOINT_WITHOUT_ID + requestParameters.id,
            updateTourMetaDataApiActionGroup,
            requestParameters
        );
    }
    
    public static deleteTour(
        requestParameters: TourRequestParameters,
        deleteTourApiActionGroup: ApiActionGroup<TourRequestParameters, NoExpectedResultType>
    ) {
        console.log('will telete this one right now', requestParameters.tourId);
        return this.apiCallNoResult<TourRequestParameters>(
            ApiMethod.delete,
            DELETE_TOUR_ENDPOINT_WITHOUT_ID + requestParameters.tourId,
            deleteTourApiActionGroup,
            requestParameters
        );
    }

    /**
     * make a call to the api and dispatch the appropriate action
     * @param method request method: get or post
     * @param apiPath api path, e. g. '/tours/user' ('/api' is added automatically)
     * @param apiActionGroup api action group containing the actions to dispatch
     * @param transformAPIResponseIntoExpectedResultType method that fills the response data into an object of the expected type
     * @param requestParemeters object containing the request parameterst (sent in body if method is post)
     */
    private static apiCall<RequestParameters, ExpectedResultType>(
        method: ApiMethod,
        apiPath: string,
        apiActionGroup: ApiActionGroup<RequestParameters, ExpectedResultType>,
        transformAPIResponseIntoExpectedResultType: ((responseData: any) => ExpectedResultType),
        requestParemeters: RequestParameters)
        : (dispatch: redux.Dispatch<null>) => void {
            // expectJsonResponse is false when the transform function is equal to getEmptyResult
            // getEmptyResult is provided when the call happened via the function fetchDataNoResult
            let expectJsonResponse = transformAPIResponseIntoExpectedResultType !== this.getEmptyResult;

            let requestInit = this.getRequestInit(method, requestParemeters);
            return (dispatch: redux.Dispatch<null>) => {
                dispatch(apiActionGroup.request(requestParemeters));
                fetch(location.protocol + '//' + location.host + API_BASE + apiPath, requestInit)
                    .then((response) => {
                        if (response.ok) {
                            // response is ok → extract json content if expected and dispatch success action
                            if (expectJsonResponse) {
                                // extract json content and dispatch success
                                if (!this.containsJsonBody(response)) {
                                    dispatch(apiActionGroup.error(new Error('response contains no json data'), requestParemeters));
                                }
                                try {
                                    let jsonPromise = response.json();
                                    jsonPromise.then((responseData) => {
                                            let transformedResponse = transformAPIResponseIntoExpectedResultType(responseData);
                                            dispatch(apiActionGroup.success(transformedResponse, requestParemeters));
                                    }).catch((error: Error) => dispatch(apiActionGroup.error(error, requestParemeters)));
                                } catch (error) {
                                    dispatch(apiActionGroup.error(error, requestParemeters));
                                }
                            } else {
                                // get empty result and dispatch success
                                try {
                                    // run transformAPIResponseIntoExpectedResultType to get an object of parameter type ExpectedResultType
                                    let emptyResult = transformAPIResponseIntoExpectedResultType({});
                                    dispatch(apiActionGroup.success(emptyResult, requestParemeters));
                                } catch (error) {
                                    dispatch(apiActionGroup.error(error, requestParemeters));
                                }
                            }
                            return;
                        } else if (response.status === 401 && apiActionGroup.unauthorized) {
                            // response status code unauthorized → dispatch unauthorized action if it exists
                            dispatch(apiActionGroup.unauthorized(requestParemeters));
                            return;
                        } else {
                            // response not as expected → dispatch error action
                            let responseText = '';
                            if (this.containsPlaintextBody(response)) {
                                response.text().then((text) => responseText = text )
                                .catch((error: Error) => dispatch(apiActionGroup.error(error, requestParemeters)));
                            } else {
                                responseText = '[no response text]';
                            }
                            dispatch(apiActionGroup.error(
                                new Error('can not handle response – response was not ok, status code: ' + response.status + ', text: ' + responseText), requestParemeters));
                            return;
                        }
                    }).catch((error: Error) => dispatch(apiActionGroup.error(error, requestParemeters)));
            };
    }

    /**
     * make a call to the api and dispatch the appropriate action (without expecting a result body)
     * @param method request method: get or post
     * @param apiPath api path, e. g. '/tours/user' ('/api' is added automatically)
     * @param apiActionGroup api action group containing the actions to dispatch
     * @param requestParemeters object containing the request parameterst (sent in body if method is post)
     */
    private static apiCallNoResult<RequestParameters>(
        method: ApiMethod,
        apiPath: string,
        apiActionGroup: ApiActionGroup<RequestParameters, NoExpectedResultType>,
        requestParemeters: RequestParameters)
        : (dispatch: redux.Dispatch<null>) => void {
        return this.apiCall<RequestParameters, NoExpectedResultType>(
            method,
            apiPath,
            apiActionGroup,
            this.getEmptyResult, // hand over function that returns an empty object as result
            requestParemeters
        );
    }

    /**
     * make a call to the api and dispatch the appropriate action (without parameters)
     * @param method request method: get or post
     * @param apiPath api path, e. g. '/tours/user' ('/api' is added automatically)
     * @param apiActionGroup api action group containing the actions to dispatch
     * @param transformAPIResponseIntoExpectedResultType method that fills the response data into an object of the expected type
     */
    private static apiCallNoParameters<ExpectedResultType>(
        method: ApiMethod,
        apiPath: string,
        apiActionGroup: ApiActionGroup<NoRequestParameters, ExpectedResultType>,
        transformAPIResponseIntoExpectedResultType: ((responseData: any) => ExpectedResultType))
        : (dispatch: redux.Dispatch<null>) => void {
        return this.apiCall<NoRequestParameters, ExpectedResultType>(
            method,
            apiPath,
            apiActionGroup,
            transformAPIResponseIntoExpectedResultType,
            {}, // no request parameters
        );
    }

    /**
     * make a call to the api and dispatch the appropriate action (without parameters and without expecting a result body)
     * @param method request method: get or post
     * @param apiPath api path, e. g. '/tours/user' ('/api' is added automatically)
     * @param apiActionGroup api action group containing the actions to dispatch
     */
    private static apiCallNoParametersNoResult(
        method: ApiMethod,
        apiPath: string,
        apiActionGroup: ApiActionGroup<NoRequestParameters, NoExpectedResultType>)
        : (dispatch: redux.Dispatch<null>) => void {
        return this.apiCall<NoRequestParameters, NoExpectedResultType>(
            method,
            apiPath,
            apiActionGroup,
            this.getEmptyResult, // hand over function that returns an empty object as result
            {}, // no request parameters
        );
    }

    /**
     * generate the request from method and request parameters
     */
    private static getRequestInit<RequestParameters>(method: ApiMethod, requestParemeters: RequestParameters): RequestInit {
        let requestInit: RequestInit = {};
        if (method === ApiMethod.get) {
            requestInit = {
                method: 'GET',
                credentials: 'same-origin',
                headers: {
                    'Accept': 'application/json'
                },
                body: undefined,
            };
        } else if (method === ApiMethod.post || method === ApiMethod.put || method === ApiMethod.delete) {
            let methodString = '';
            switch (method) {
                case ApiMethod.post:
                    methodString = 'POST';
                    break;
                case ApiMethod.put:
                    methodString = 'PUT';
                    break;
                case ApiMethod.delete:
                    methodString = 'DELETE';
                    break;
                default:
                    break;
            }
            requestInit = {
                method: methodString,
                credentials: 'same-origin',
                headers: {
                    'content-type': 'application/json',
                    'Accept' : 'application/json',
                },
                body: JSON.stringify(requestParemeters),
            };
        } else {
            throw('ApiMethod is not handled: ' + method);
        }
        return requestInit;
    }

    private static containsJsonBody (response: Response): boolean {
        const contentType = response.headers.get('content-type');
        return contentType !== null && contentType.includes('application/json');
    }

    private static containsPlaintextBody (response: Response) {
        const contentType = response.headers.get('content-type');
        return contentType !== null && contentType.includes('text/plain');
    }

    /**
     * method that simply returns an empty object
     */
    private static getEmptyResult(response: any): NoExpectedResultType {
        return {};
    }
}
