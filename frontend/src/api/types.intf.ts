import { Action } from '../store/actionTypes.intf';

export enum ApiMethod {
    get,
    post,
    put,
    delete,
}

export type NoExpectedResultType = {};

export type ApiActionGroup<RequestParameter, ExpectedResponseData> = {
    request: (requestParameter: RequestParameter ) => Action & RequestContainer<RequestParameter>,
    success: (expectedResponseData: ExpectedResponseData, requestParameter: RequestParameter)
      => Action & RequestContainer<RequestParameter> & ResponseContainer<ExpectedResponseData>,
    unauthorized?: (requestedParameter: RequestParameter) => Action & RequestContainer<RequestParameter>,
    error: (error: Error, q: RequestParameter) => Action & RequestContainer<RequestParameter> & ErrorContainer,
  };
  
export type ResponseContainer<ExpectedResponseData> = { response: ExpectedResponseData };
export type ErrorContainer = { error: Error };
export type RequestContainer<RequestParameter> = { request: RequestParameter };
