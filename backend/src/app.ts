import { Webserver } from './webserver/webserver';
import { TourApi } from './api/tour/tour.api';
import { UserApi } from './api/user/user.api';
import { Authentication } from './auth/authentication';
import { Api } from './api/api';
import { ConfigProvider } from './config/config.provider';
import { Logger, LogLevel } from './logger/logger';
import { ConsoleLogger } from './logger/console.logger';

Logger.SetLoggerMedium(new ConsoleLogger());
Logger.SetLogLevel(LogLevel.Info);

if (process.argv.length > 2) {
    ConfigProvider.startUp(process.argv[2]);
} else {
    ConfigProvider.startUp();
}
let app = Webserver.startUp();
Api.setApp(app);
TourApi.startUp();
UserApi.startUp();
Authentication.startUp();

Webserver.setFallbackIndex();

Logger.Info('App', 'App is up and running!');
