import * as bcrypt from 'bcryptjs';
import { Logger } from '../logger/logger';

export namespace Helper {
    var _cpoId = 'Helper';
    /**
     * Converts a timeString (milliseconds since 1970) to a date object the database can read
     * @param {string} timeString the timestring
     * @returns {Date} a javascript dateobject
     */
    export function timeStringToDateObjectForDb(timeString: string): Date {
        let time = new Date(0);
        let timeNumber = Number(timeString);
        timeNumber = timeNumber / 1000;
        time.setUTCSeconds(timeNumber);
        return time;
    }

    /**
     * Converts a timeString (milliseconds since 1970) to a string represantation the db can read
     * @param {string} timeString the timestring
     * @returns {string} the string to be inserted into db
     */
    export function timeStringToDateStringForDb(timeString: string): string {
        let time = new Date(0);
        let timeNumber = Number(timeString);
        timeNumber = timeNumber / 1000;
        //TODO: implement a clean solution to represents the timezone (needs the mobile to send correct time too)
        timeNumber -= 60 * new Date().getTimezoneOffset();
        time.setUTCSeconds(timeNumber);
        Logger.Debug(_cpoId, 'converted time to: ' + time.toISOString().slice(0, 19).replace('T', ' '));
        return time.toISOString().slice(0, 19).replace('T', ' ');
    }

    /**
     * Hashes your password with salting
     * @param {string} cleartext the cleartext password you want to hash
     * @returns {Promise<string>} the hashed password
     */
    export async function hashPassword(cleartext: string): Promise<string> {
        const saltRounds = 10;
        return bcrypt.hash(cleartext, saltRounds);
    }

    /**
     * compares your cleantext password with a hashed password and tells if they are the same
     * @param {string} cleartext the password in cleartext
     * @param {string} hashedPassword the hashed value you want to compare with
     * @returns {Promise<boolean>} true if they are the same else false
     */
    export async function comparePassword(cleartext: string, hashedPassword: string): Promise<boolean> {
        return bcrypt.compare(cleartext, hashedPassword);
    }
}
