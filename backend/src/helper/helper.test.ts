import * as bcrypt from 'bcryptjs';
import { Helper } from './helper';
import { } from 'jest';

describe('Helper', () => {
    describe('function timeStringToDateObjectForDb', () => {
        it('should return correct date', () => {
            let myDate = new Date();
            //we do not care about millis
            myDate.setMilliseconds(0);
            const ret = Helper.timeStringToDateObjectForDb(myDate.getTime().toString());
            expect(ret.toString()).toBe(myDate.toString());
        });
    });
    describe('function timeStringToDateStringForDb', () => {
        it('should return a correct date', () => {
            let myDate = new Date(0);
            const ret = Helper.timeStringToDateStringForDb(myDate.getTime().toString());
            let offset = new Date().getTimezoneOffset();
            offset = offset / -60;
            expect(ret).toBe('1970-01-01 0' + offset + ':00:00');
        });
    });
    describe('function hashPassword', () => {
        it('should call hash of bcyrpt', async () => {
            let spy = jest.spyOn(bcrypt, 'hash').mockImplementation((a, b) => {
                return new Promise<string>((resolve, reject) => {
                    resolve(a + b);
                });
            });
            expect(await Helper.hashPassword('test')).toBe('test10');
        });
    });
    describe('function comparePassword', () => {
        it('should return true', async () => {
            let spy = jest.spyOn(bcrypt, 'compare').mockImplementation((a, b) => {
                return new Promise<boolean>((resolve, reject) => {
                    resolve(true);
                });
            });
            expect(await Helper.comparePassword('test', 'test')).toBe(true);
        });
        it('should return false', async () => {
            let spy = jest.spyOn(bcrypt, 'compare').mockImplementation((a, b) => {
                return new Promise<boolean>((resolve, reject) => {
                    resolve(false);
                });
            });
            expect(await Helper.comparePassword('test', 'test')).toBe(false);
        });
    });
});
