import * as express from 'express';
import { } from 'jest';
import { ConfigProvider } from '../config/config.provider';
afterEach(() => {
    Webserver.shutDown();
});

let config = {
    HttpPort: 1234,
    HttpsPort: 5678,
    KeyFile: '/etc/letsencrypt/live/www.trackmyride.ch/privkey.pem',
    CertFile: '/etc/letsencrypt/live/www.trackmyride.ch/fullchain.pem',
    Webroot: '/home/trackmyride/frontend',
    IndexFileRelativeToWebroot: 'index.html',
    Database: {
        connectionLimit: 10,
        host: 'localhost',
        user: 'trackmyride',
        password: '',
        database: 'trackmyride',
    },
};

ConfigProvider.Config = config;
import { Webserver } from './webserver';

describe('Webserver', () => {
    it('Webserver http is on port configured in Config', () => {
        Webserver.startUp();
        let port = Webserver.HttpPort;
        expect(port).toBe(1234);
    });
    it('Webserver https is on port configured in Config', () => {
        Webserver.startUp();
        let port = Webserver.HttpsPort;
        expect(port).toBe(5678);
    });

    describe('function StartUp', () => {
        it('should start a sever', () => {
            let app = Webserver.startUp();
            expect(app).toBeDefined();
            expect(Webserver.Listening).toBe(true);
        });
    });

    describe('function ShutDown', () => {
        it('should stop the sever', () => {
            let app = Webserver.startUp();
            expect(app).toBeDefined();
            Webserver.shutDown();
            expect(Webserver.Listening).toBe(false);
        });

        it('should not crash when called without', () => {
            Webserver.shutDown();
            Webserver.shutDown();
            expect(Webserver.Listening).toBe(false);
        });
    });
    describe('function setFallbackIndex', () => {
        it('should attach a wildcard listener', () => {
            let app = Webserver.startUp();
            let spy = spyOn(app, 'get');
            Webserver.setFallbackIndex();
            expect(spy).toHaveBeenCalledTimes(1);
            expect(app).toBeDefined();
        });
    });
});
