import * as express from 'express';
import * as path from 'path';
import { Server } from 'net';
import { ConfigProvider } from '../config/config.provider';
const Config = ConfigProvider.Config;
import * as fs from 'fs';

import * as http from 'http';
import * as https from 'https';
import { Logger } from '../logger/logger';

export module Webserver {
    var _cpoId = 'Webserver';
    var _app: express.Application;
    var _httpServer: http.Server;
    var _httpsServer: https.Server;
    var _useHttps: boolean;
    export var HttpPort: number;
    export var HttpsPort: number;
    export var Key: string; //'/etc/letsencrypt/live/www.trackmyride.ch/privkey.pem';
    export var Cert: string; //'/etc/letsencrypt/live/www.trackmyride.ch/fullchain.pem';
    export var Listening = false;

    /**
     * starts the webserver
     * @returns the express app
     */
    export function startUp(): express.Application {
        _app = express();
        HttpPort = Config.HttpPort;
        HttpsPort = Config.HttpsPort;
        Key = Config.KeyFile; //'/etc/letsencrypt/live/www.trackmyride.ch/privkey.pem';
        Cert = Config.CertFile; //'/etc/letsencrypt/live/www.trackmyride.ch/fullchain.pem';

        _app.use(express.static(Config.Webroot));

        _app.get('/', function (req: express.Request, res: express.Response) {
            res.sendFile(Config.Webroot + Config.IndexFileRelativeToWebroot);
        });

        _app.get('/testlogin', function (req: express.Request, res: express.Response) {
            res.sendFile(Config.Webroot + 'loginTestHtml.html');
        });

        _app.get('/__download/app-release.apk', function (req: express.Request, res: express.Response) {
            Logger.Info(_cpoId, 'Download of App requested');
            res.sendFile(Config.AppApk);
        });

        var credentials = { key: '', cert: '' };
        try {
            var privateKey = fs.readFileSync(Key, 'utf8');
            var certificate = fs.readFileSync(Cert, 'utf8');
            credentials = { key: privateKey, cert: certificate };
            _useHttps = true;
        } catch (error) {
            Logger.Notice(_cpoId, 'Zertifikat für HTTPS konnte nicht geladen werden. Verwende HTTP');
            _useHttps = false;
        }

        if (_useHttps) {
            _httpsServer = https.createServer(credentials, _app);
            _httpsServer.listen(HttpsPort);

            var redirectApp: express.Application = express();
            redirectApp.get('*', function (req, res) {
                res.redirect('https://' + req.headers.host + req.url);
            });
            _httpServer = http.createServer(redirectApp);
            _httpServer.listen(HttpPort);
            Logger.Info(_cpoId, 'Server listening on Port: ' + HttpsPort);
        } else {
            _httpServer = http.createServer(_app);
            _httpServer.listen(HttpPort);
            Logger.Info(_cpoId, 'Server listening on Port: ' + HttpPort);
        }
        Listening = true;
        return _app;
    }

    /**
     * shuts the webserver down
     */
    export function shutDown() {
        if (_httpsServer && _httpsServer.listening) {
            _httpsServer.close();
            Listening = false;
        }
        if (_httpServer && _httpServer.listening) {
            _httpServer.close();
            Listening = false;
        }
    }

    export function setFallbackIndex() {
        _app.get('*', function (req: express.Request, res: express.Response) {
            res.sendFile(Config.Webroot + Config.IndexFileRelativeToWebroot);
        });
    }
}
