/*
	Aktualisiert die DB auf die neuste Version.
    
    Ergänzungen (Tabellen/Views):
	 -> bitte @ Line "#### ADD NEW CODE HERE ####"
	
    Neue SPs hinzufügen:
     -> @ Line "#### Add additional SPs above this line ####"
    und zusäzlich vorher droppen:
     -> @ Line "#### Additional Drops Here: ####"
     
	Stored Procedures können leider nicht aus Stored Procedures heraus erstellt
    werden und werden deshalb immer gedropt und neu erstellt...
    
*/

-- Remap end-of-line-char, so ';' can be used in the SP
DELIMITER $$

USE `trackmyride`$$

/*
  Leider können IF-Statements bei MySql nur in Stored
  Procedures verwendet werden. Zudem werden Transactions
  für DDL auch nicht unterstützt.
  -> temporäre SP anlegen, ausführen, wieder löschen
     und dabei keine transactions verwenden...
*/

SET sql_notes = 0$$ -- Disable warning "does not exist"
DROP PROCEDURE IF EXISTS `temp_update_db`$$
SET sql_notes = 1$$ -- Re-enable warnings


CREATE PROCEDURE `temp_update_db`()
BEGIN

	DECLARE db_version INT DEFAULT NULL;
    
    -- Get version info from db
	SET db_version = (SELECT `int` FROM `config` WHERE `key` = 'db_version');
    
    SELECT 'start' AS 'update_db', db_version AS 'db_version';
    
    -- Initialize configuration
    IF db_version IS NULL THEN
    
		SET db_version = 0;
        INSERT INTO `config` (`key`, `int`)
		VALUES ('db_version', db_version);
		
    END IF;
    
    -- Create basic tables (`user`, `session`, `tour`, `tour_data`, ...)
    -- and register hook for inserting mock data automatically
    IF db_version = 0 THEN
    	
        #### CONFIGURATION SECTION ####
        
        -- set config-entry for automatic test data generation @ update_mock_data.sql
        INSERT INTO `config` (`key`, `int`)
        VALUES ('test_data_version', 0); -- enable automatic generation = 0
		-- VALUES ('test_data_version', -1); -- disable automatic generation = -1
        

        #### ADD TABLES ####
        
		CREATE TABLE `user` (
			`id` int UNSIGNED NOT NULL AUTO_INCREMENT,
			`username` varchar(255) UNIQUE NOT NULL,
			`email` varchar(255),
			`password` varchar(255) NOT NULL, -- hashed + salted...
            `avatar` longblob, -- user image
			PRIMARY KEY (`id`)
		);
        
        CREATE TABLE `session` (
			`id` varchar(128) UNIQUE NOT NULL,
			`user_id` int UNSIGNED NOT NULL, -- not unique = parallel sessions (ie. app + web)
            `expires` int(11) unsigned NOT NULL,
			`data` text NOT NULL,
			PRIMARY KEY (`id`),
			FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE
        );
        

		CREATE TABLE `tour` (
			`id` int UNSIGNED NOT NULL AUTO_INCREMENT,
			`user_id` int UNSIGNED NOT NULL,
			`upload_time` datetime NOT NULL DEFAULT NOW(),
			`public` boolean NOT NULL DEFAULT FALSE, -- visible to all users and guests
			`name` varchar(128), -- name of the tour
			`description` varchar(1024), -- description text
			`start_time` datetime, -- from start button (not statistical data)
			`end_time` datetime, -- from stop button (not statistical data)
            `picture` longblob, -- main image (cover)
			`processed` boolean NOT NULL DEFAULT FALSE, -- statistics calculated (min/max/avg/... from data table)
			`data_count` int, -- number of data points
			`lat_min` double, -- statistics --> latitude
			`lat_max` double, -- statistics
			`long_min` double, -- statistics --> longitude
			`long_max` double, -- statistics
            `elev_min` double, -- statistics --> elevation
            `elev_max` double, -- statistics
			`v_max` double, -- statistics --> velocity
			`v_avg` double, -- statistics
			`a_max` double, -- statistics --> acceleration
            `t_max` double, -- statistics --> tilt (Neigung)
			PRIMARY KEY (`id`),
			FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE
		);
        
		CREATE TABLE `tour_data` (
			`id` int UNSIGNED NOT NULL AUTO_INCREMENT,
			`user_id` int UNSIGNED NOT NULL,
			`tour_id` int UNSIGNED NOT NULL,
			`time` datetime,
            `lat` double, -- latitude
			`long` double, -- longitude
            `elev` double, -- altitude / elevation
            `velocity` double,
            `heading` double,
            `bearing` double,
            `accuracy` double,
			`ax` double, -- acceleration
			`ay` double,
			`az` double,
			`gx` double, -- gyroscope
			`gy` double,
			`gz` double,
			`azimuth` double, -- orientation
			`pitch` double,
			`roll` double,
			PRIMARY KEY (`id`),
			FOREIGN KEY (`tour_id`) REFERENCES `tour`(`id`) ON DELETE CASCADE,
			FOREIGN KEY (`user_id`) REFERENCES `tour`(`user_id`) ON DELETE CASCADE
		);


        #### ADD VIEWS ####
        
        CREATE VIEW `v_tour_statistics` AS
			SELECT tour_id, (SELECT `name` FROM `tour` WHERE `id` = `tour_id`) AS `tour_name`
				, COUNT(1) AS `count`, min(time), max(time)
				, min(lat), max(lat), min(`long`), max(`long`), min(elev), max(elev)
			FROM `tour_data`
			GROUP BY `tour_id`;


        #### UPDATE CONFIG VERSION ####
        
        SET db_version = 1;
        UPDATE `config` SET `int` = db_version
		WHERE `key` = 'db_version';
    END IF;
    
    
    -- Add Comments & Ratings, Tour Name/Desc: not null + default, Desc len+++
    IF db_version = 1 THEN
    
        #### TOUR: name/description NOT NULL ####
        
        SET SQL_SAFE_UPDATES = 0;
        
		UPDATE `tour` SET `name` = '' WHERE `name` IS NULL;
		ALTER TABLE `tour` MODIFY `name` varchar(128) NOT NULL DEFAULT '';
        
		UPDATE `tour` SET `description` = '' WHERE `description` IS NULL;
		ALTER TABLE `tour` MODIFY `description` nvarchar(15000) NOT NULL DEFAULT '';
        
        SET SQL_SAFE_UPDATES = 1;

        #### ADD COMMENTS ####
        
        CREATE TABLE `comment` (
			`id` int UNSIGNED NOT NULL AUTO_INCREMENT,
			`user_id` int UNSIGNED NOT NULL,
			`tour_id` int UNSIGNED NOT NULL,
            `comment` nvarchar(1024) NOT NULL,
			`date` datetime NOT NULL DEFAULT NOW(),
			PRIMARY KEY (`id`),
			FOREIGN KEY (`tour_id`) REFERENCES `tour`(`id`) ON DELETE CASCADE,
			FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE
        );
    	
        #### ADD RATINGS ####
        
        CREATE TABLE `rating` (
			`user_id` int UNSIGNED NOT NULL,
			`tour_id` int UNSIGNED NOT NULL,
            `rating` int unsigned NOT NULL,
			`date` datetime NOT NULL DEFAULT NOW(),
			PRIMARY KEY (`tour_id`, `user_id`),
			FOREIGN KEY (`tour_id`) REFERENCES `tour`(`id`) ON DELETE CASCADE,
			FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE
        );
        
        #### ADD TOUR VIEW ####

		CREATE VIEW `v_tour` AS
			SELECT `tour`.`id`, `tour`.`user_id`, `upload_time`, `public`, `name`, `description`, `start_time`, `end_time`,
				`picture`, ifnull(avg(`rating`), 0) AS `rating`, `processed`, `lat_min`, `lat_max`, `long_min`, `long_max`, `elev_min`, `elev_max`,
				`v_max`, `v_avg`, `a_max`, `t_max`
			FROM `tour` LEFT OUTER JOIN `rating` ON `tour`.`id` = `rating`.`tour_id`
			GROUP BY `tour`.`id`;

        #### UPDATE CONFIG VERSION ####

        SET db_version = 2;
        UPDATE `config` SET `int` = db_version
		WHERE `key` = 'db_version';
    END IF;
    
    
    
    #### ADD NEW CODE HERE ####
    /*
    -- Description
    IF db_version = 2 THEN
    
    	-- do stuff...

        SET db_version += 1;
        UPDATE `config` SET `int` = db_version
		WHERE `key` = 'db_version';
    END IF;
    */
    
    
    #### ADD NEW CODE HERE ####
    /*
    -- Description
    IF db_version = 3 THEN
    
    	-- do stuff...

        SET db_version += 1;
        UPDATE `config` SET `int` = db_version
		WHERE `key` = 'db_version';
    END IF;
    */

	SELECT 'complete' AS 'update_db', db_version AS 'db_version';
    
END$$

-- Execute update script...
CALL `temp_update_db`()$$

-- Cleanup
DROP PROCEDURE `temp_update_db`$$


#### ADD STORED PROCEDURES ####
-- Create stored procedures for db-access from backend using drop + delete (!)
-- Conditional create and create sp from within other sp not possible...

SET sql_notes = 0$$ -- Disable warning "does not exist"
#### Additional Drops Here: ####
DROP TRIGGER IF EXISTS `sessionLinkUserTrigger`$$
DROP TRIGGER IF EXISTS `t_sessionLinkUserTrigger`$$
DROP PROCEDURE IF EXISTS `sp_register_user`$$
DROP PROCEDURE IF EXISTS `sp_get_user_by_name`$$
DROP PROCEDURE IF EXISTS `sp_delete_session`$$
DROP PROCEDURE IF EXISTS `sp_create_tour`$$
DROP PROCEDURE IF EXISTS `sp_get_tour`$$
DROP PROCEDURE IF EXISTS `sp_get_tour_data`$$
DROP PROCEDURE IF EXISTS `sp_get_public_tours`$$
DROP PROCEDURE IF EXISTS `sp_get_user_tours`$$
DROP PROCEDURE IF EXISTS `sp_update_tour_name`$$
DROP PROCEDURE IF EXISTS `sp_update_tour_description`$$
DROP PROCEDURE IF EXISTS `sp_update_tour_picture`$$
DROP PROCEDURE IF EXISTS `sp_update_tour_public`$$
DROP PROCEDURE IF EXISTS `sp_calculate_tour_statistics`$$
DROP PROCEDURE IF EXISTS `sp_get_user_session`$$
DROP PROCEDURE IF EXISTS `sp_add_session`$$
DROP PROCEDURE IF EXISTS `sp_get_my_tour_ratings`$$
DROP PROCEDURE IF EXISTS `sp_get_tour_comments`$$
DROP PROCEDURE IF EXISTS `sp_add_tour_comment`$$
DROP PROCEDURE IF EXISTS `sp_update_tour_rating`$$
DROP PROCEDURE IF EXISTS `sp_get_user_by_id`$$
DROP PROCEDURE IF EXISTS `sp_delete_tour`$$
SET sql_notes = 1$$ -- Re-enable warnings



CREATE PROCEDURE `sp_get_user_session`
(
	`session_id` varchar(255)
)
BEGIN
	/*
		Get user by session id
		Returns sesseion info or NULL
	*/
	
	SELECT `user_id`,
		   `expiration`,
           `expiration` <= NOW() AS 'expired'
    FROM `session` WHERE `id` = session_id;

END$$


CREATE PROCEDURE `sp_add_session`
(
	session_id varchar(255),
	user_id int UNSIGNED,
	expiration_date datetime
)
BEGIN
	/*
		Inserts session information in db
	*/

	INSERT INTO `session` (`id`, `user_id`, `expiration`)
	VALUES (session_id, user_id, expiration_date);

END$$


CREATE PROCEDURE `sp_register_user`
(
	username varchar(255),
	email varchar(255),
	pwd_hash varchar(255)
)
BEGIN
	/*
		Returns user_id from username
	*/

	INSERT INTO `user` (`username`, `email`, `password`)
	VALUES (username, email, pwd_hash);

	SELECT LAST_INSERT_ID(); -- id rückgeben

END$$


CREATE PROCEDURE `sp_get_user_by_name`
(
	username_input varchar(255)
)
BEGIN
	/*
		Returns all user data
	*/

	SELECT `id`, `username`, `email`, `password`, `avatar` FROM `user` WHERE `username` = username_input;

END$$


CREATE PROCEDURE `sp_get_user_by_id`
(
	id_input int UNSIGNED
)
BEGIN
	/*
		Returns all user data
	*/

	SELECT `id`, `username`, `email`, `password`, `avatar` FROM `user` WHERE `id` = id_input;

END$$


CREATE PROCEDURE `sp_delete_session`
(
	session_id varchar(255)
)
BEGIN
	/*
		Remove session ("logout")
	*/

	IF session_id IS NOT NULL THEN
    
		DELETE FROM `session` WHERE `id` = session_id;
    
    END IF;

END$$


CREATE PROCEDURE `sp_create_tour`
(
	user_id int UNSIGNED,
    start_time datetime,
    end_time datetime,
    tour_name varchar(128),
    tour_description nvarchar(15000)
)
BEGIN
	/*
		Create tour
        Returns tour_id of inserted tour
	*/
	INSERT INTO `tour` (`user_id`, `start_time`, `end_time`, `name`, `description`)
	VALUES (user_id , start_time, end_time, tour_name, tour_description);
    
	SELECT LAST_INSERT_ID(); -- id rückgeben

END$$


CREATE PROCEDURE `sp_get_tour`
(
	tour_id_input int UNSIGNED
)
BEGIN
	/*
		Get statistical tour data
	*/

	SELECT `id`, `user_id`, `upload_time`, `public`, `name`, `description`, `start_time`, `end_time`,
		`picture`, `rating`, `processed`, `lat_min`, `lat_max`, `long_min`, `long_max`, `elev_min`, `elev_max`,
		`v_max`, `v_avg`, `a_max`, `t_max`
	FROM `v_tour` WHERE `id` = tour_id_input;


END$$


CREATE PROCEDURE `sp_get_tour_data`
(
	tour_id_input int UNSIGNED
)
BEGIN
	/*
		Get tour data points
	*/

	SELECT `id`, `user_id`, `tour_id`, `time`, `lat`, `long`, `elev`,
		`velocity`, `heading`, `bearing`, `accuracy`,
		`ax`, `ay`, `az`, `gx`, `gy`, `gz`, `azimuth`, `pitch`, `roll`
	FROM `tour_data` WHERE `tour_id` = tour_id_input
    ORDER BY `time` ASC;


END$$


CREATE PROCEDURE `sp_get_public_tours`
(
)
BEGIN
	/*
		Get all public tours
	*/

	SELECT `id`, `user_id`, `upload_time`, `name`, `description`, `start_time`, `end_time`, `picture`, `rating`, `public`, `processed`,
		`lat_min`, `lat_max`, `long_min`, `long_max`, `elev_min`, `elev_max`, `v_max`, `v_avg`, `a_max`, `t_max`
	FROM `v_tour` WHERE `public` = True AND `processed` = True;

END$$


CREATE PROCEDURE `sp_get_user_tours`
(
	user_id_input int UNSIGNED
)
BEGIN
	/*
		Get all tours from a user
	*/

	SELECT `id`, `user_id`, `upload_time`, `name`, `description`, `start_time`, `end_time`, `picture`, `rating`, `public`, `processed`,
		`lat_min`, `lat_max`, `long_min`, `long_max`, `elev_min`, `elev_max`, `v_max`, `v_avg`, `a_max`, `t_max`
	FROM `v_tour` WHERE `user_id` = user_id_input;
    
END$$



CREATE PROCEDURE `sp_update_tour_name`
(
	tour_id_input int UNSIGNED,
	name_input varchar(128) -- name of the tour
)
BEGIN
	/*
		Update tour details
	*/
	UPDATE `tour`
	SET	`name` = name_input
	WHERE `id` = tour_id_input;
END$$

CREATE PROCEDURE `sp_update_tour_description`
(
	tour_id_input int UNSIGNED,
	description_input nvarchar(15000) -- description text
)
BEGIN
	/*
		Update tour details
	*/
	UPDATE `tour`
	SET `description` = description_input
	WHERE `id` = tour_id_input;
END$$

CREATE PROCEDURE `sp_update_tour_picture`
(
	tour_id_input int UNSIGNED,
	picture_input longblob -- main image (cover)
)
BEGIN
	/*
		Update tour details
	*/
	UPDATE `tour`
	SET `picture` = picture_input
	WHERE `id` = tour_id_input;
END$$

CREATE PROCEDURE `sp_update_tour_public`
(
	tour_id_input int UNSIGNED,
    public_input boolean -- True = visible to all users and guests
)
BEGIN
	/*
		Update tour details
	*/
	UPDATE `tour`
	SET `public` = public_input
	WHERE `id` = tour_id_input;

END$$



CREATE PROCEDURE `sp_calculate_tour_statistics`
(
	tour_id_input int UNSIGNED
)
BEGIN
	/*
		Recalculate and update tour statistics from tour data
	*/

	UPDATE `tour`
	SET
		`processed` = True,
		`data_count` = (SELECT COUNT(1) FROM `tour_data` WHERE `tour_data`.`tour_id` = tour_id),
		`lat_min` = (SELECT min(`tour_data`.`lat`) FROM `tour_data` WHERE `tour_data`.`tour_id` = tour_id),
		`lat_max` = (SELECT max(`tour_data`.`lat`) FROM `tour_data` WHERE `tour_data`.`tour_id` = tour_id),
		`long_min` = (SELECT min(`tour_data`.`long`) FROM `tour_data` WHERE `tour_data`.`tour_id` = tour_id),
		`long_max` = (SELECT max(`tour_data`.`long`) FROM `tour_data` WHERE `tour_data`.`tour_id` = tour_id),
		`elev_min` = (SELECT min(`tour_data`.`elev`) FROM `tour_data` WHERE `tour_data`.`tour_id` = tour_id),
		`elev_max` = (SELECT max(`tour_data`.`elev`) FROM `tour_data` WHERE `tour_data`.`tour_id` = tour_id)
		-- TODO `v_max` = (SELECT `tour_data`.`<{v_max: }> FROM `tour_data` WHERE `tour_data`.`tour_id` = tour_id),
		-- TODO `v_avg` = (SELECT `tour_data`.`<{v_avg: }> FROM `tour_data` WHERE `tour_data`.`tour_id` = tour_id),
		-- TODO `a_max` = (SELECT `tour_data`.`<{a_max: }> FROM `tour_data` WHERE `tour_data`.`tour_id` = tour_id),
		-- TODO `t_max` = (SELECT `tour_data`.`<{t_max: }> FROM `tour_data` WHERE `tour_data`.`tour_id` = tour_id)
	WHERE `id` = tour_id_input;

END$$



CREATE TRIGGER `t_sessionLinkUserTrigger`
	-- trigger which reads the user_id from the json string `data`
	-- required because express-mysql-session inserts sessions itself
	BEFORE INSERT 
	ON `session`
	FOR EACH ROW
		SET NEW.user_id = JSON_EXTRACT(NEW.data,'$.passport.user')
$$


CREATE PROCEDURE `sp_get_my_tour_ratings`
(
	user_id_input int UNSIGNED
)
BEGIN
	/*
		Get all ratings for selected tour
	*/
	SELECT `rating`.`tour_id`, `rating`.`rating`, `tour`.`name`, `tour`.`description`, `user`.`username`
    FROM `rating`
    JOIN `tour` ON `tour`.`id` = `rating`.`tour_id`
    JOIN `user` ON `user`.`user_id` = `tour`.`user_id`
    WHERE `rating`.`user_id` = user_id_input;

END$$


CREATE PROCEDURE `sp_get_tour_comments`
(
	tour_id_input int UNSIGNED
)
BEGIN
	/*
		Get all ratings for selected tour
	*/
	SELECT `username`, `comment`, `comment_date`
    FROM `comment` JOIN `user` ON `comment`.`user_id` = `user`.`id`
    WHERE `tour_id` = tour_id_input;

END$$


CREATE PROCEDURE `sp_add_tour_comment`
(
	tour_id_input int UNSIGNED,
	user_id_input int UNSIGNED,
    comment_input nvarchar(1024) 
)
BEGIN
	/*
		Get all ratings for selected tour
	*/
    INSERT INTO `comment`
		(`user_id`, `tour_id`, `comment`)
	VALUES
		(user_id_input, tour_id_input, comment_input);

END$$

CREATE PROCEDURE `sp_update_tour_rating`
(
	tour_id_input int UNSIGNED,
	user_id_input int UNSIGNED,
    rating_input int UNSIGNED
)
BEGIN
	/*
		Update rating for selected tour
	*/
    REPLACE INTO `rating`
		(`user_id`, `tour_id`, `rating`)
	VALUES
		(user_id_input, tour_id_input, rating_input);

END$$

CREATE PROCEDURE `sp_delete_tour`
(
	tour_id_input int UNSIGNED
)
BEGIN
	/*
		delete all tour fields
	*/

	IF tour_id_input IS NOT NULL THEN

		DELETE FROM `tour` WHERE `id` = tour_id_input;
    
    END IF;

END$$


### Add additional SPs above this line ###

#########################
### When appending Stored Procedures:
### Add drop statements @ Line "#### Additional Drops Here: ####"
#########################

-- Restore end-of-line-char to default
DELIMITER ;