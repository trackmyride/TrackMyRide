/*
	Initialisiert die DB.
    Konfiguration siehe "update_db"
*/

SET sql_notes = 0; -- Disable warning "already exists"

CREATE DATABASE IF NOT EXISTS `trackmyride`;
USE `trackmyride`;

-- Config-Table anlegen
CREATE TABLE IF NOT EXISTS `config` (
	`key` varchar(255) NOT NULL,
    `int` int,
    `real` real,
    `text` varchar(255),
    PRIMARY KEY (`key`)
);

SET sql_notes = 1; -- Re-enable warnings
