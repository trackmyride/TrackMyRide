import * as path from 'path';
import { IConfig } from './config.class';
import * as fs from 'fs';
import { Logger } from '../logger/logger';

export module ConfigProvider {
    var _cpoId = 'ConfigProvider';
    //the Configuration
    export var Config: IConfig = {
        HttpPort: 8000,
        HttpsPort: 3443,
        KeyFile: '/etc/letsencrypt/live/www.trackmyride.ch/privkey.pem',
        CertFile: '/etc/letsencrypt/live/www.trackmyride.ch/fullchain.pem',
        Webroot: '/home/trackmyride/frontend/',
        IndexFileRelativeToWebroot: 'index.html',
        AppApk: '/home/trackmyride/backend/download/app-release.apk',
        Database: {
            connectionLimit: 10,
            host: 'localhost',
            user: 'trackmyride',
            password: '',
            database: 'trackmyride',
        },
    };

    /**
     * starts the configuration up and provides the possibility to overwrite some default settings with an own file
     * @param {string} [filePath] the path to the own config file
     */
    export function startUp(filePath?: string) {
        //Set default Config
        if (filePath) {
            try {
                let file = fs.readFileSync(filePath, { encoding: 'utf8' });
                setConfig(JSON.parse(file));
            } catch (e) {
                Logger.Notice(_cpoId, 'starting with your config failed, using default config now!');
            }
        }

    }

    /**
     * Gets a Json object with settings to overwrite and overwrites these settings
     * @param {*} json the json object with settings
     */
    function setConfig(json: any) {
        merge(Config, json);
        Logger.Info(_cpoId, 'New Configuration:', Config);
    }

    /**
     * Getter for Configuration
     * @returns {IConfig} the configuration
     */
    export function getConfig(): IConfig {
        return Config;
    }

    /**
     * merges one object into another. the second parameter has higher priority
     * @param {*} baseObj the base object
     * @param {*} replacer the object with the settings to replace
     */
    function merge(baseObj: any, replacer: any) {
        for (const key in replacer) {
            if (replacer.hasOwnProperty(key) && baseObj.hasOwnProperty(key)) {
                const element = replacer[key];
                if (typeof element === 'object') {
                    merge(baseObj[key], element);
                } else {
                    baseObj[key] = replacer[key];
                }
            }
        }
    }

}
