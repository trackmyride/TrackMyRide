
export interface IConfig {
    HttpPort: number;
    HttpsPort: number;
    KeyFile: string;
    CertFile: string;
    Webroot: string;
    IndexFileRelativeToWebroot: string;
    AppApk: string;
    Database: {
        connectionLimit: number,
        host: string,
        user: string,
        password: string,
        database: string,
    };
}
