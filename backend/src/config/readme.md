1. File erstellen mit JSON
2. `npm start <PFAD_ZUM_FILE>`
    * bsp: npm start ./config.json
3. `./track-my-ride-backend <PFAD_ZUM_FILE>`
    * bsp: ./track-my-ride-backend ./config.json

Der Fileinhalt muss gültiges JSON sein. Alle nicht definierten Felder werden von der Standardconfig übernommen.

Bitte keine eigenen Configs einchecken....
