jest.mock('fs');
import * as fs from 'fs';
import { ConfigProvider } from './config.provider';
import { } from 'jest';

describe('ConfigProvider', () => {
    describe('function startup', () => {
        it('should accept no path', () => {
            const one = ConfigProvider.Config;
            ConfigProvider.startUp();
            const two = ConfigProvider.Config;
            expect(one).toBe(two);
        });
        it('should accept a path and call fs with it', () => {
            //jest.spyOn(fs, 'readFileSync').mockReturnValue('{\"Webroot\":\"test\"}');
            let spy = jest.spyOn(fs, 'readFileSync');
            spy.mockReturnValue('{"Webroot":"test"}');
            const one = ConfigProvider.Config;
            const str1 = JSON.stringify(one);
            ConfigProvider.startUp('/test');
            const two = ConfigProvider.Config;
            const str2 = JSON.stringify(one);
            expect(str1).not.toBe(str2);
            expect(spy).toHaveBeenCalled();
            expect(two.Webroot).toBe('test');
        });
        it('should accept an invalid path and call fs with it', () => {
            let spy = jest.spyOn(fs, 'readFileSync');
            spy.mockImplementation(() => { throw new Error('hello'); });
            const one = ConfigProvider.Config;
            const str1 = JSON.stringify(one);
            ConfigProvider.startUp('/test');
            const two = ConfigProvider.Config;
            const str2 = JSON.stringify(one);
            expect(str1).toBe(str2);
            expect(spy).toHaveBeenCalled();
        });
    });
    describe('function getConfig', () => {
        it('should return correct date', () => {
            expect(true).toBe(true);
        });
    });
    describe('property config', () => {
        it('should return the default value twice', () => {
            const one = ConfigProvider.Config;
            const two = ConfigProvider.Config;
            expect(one).toBe(two);
        });
    });
});
