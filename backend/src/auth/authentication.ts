import * as passport from 'passport';
import * as passportLocal from 'passport-local';
import { Database } from '../db/database';
import { IDBIntf } from '../db/db.intf';
import { DbImpl } from '../db/db.impl';
import { ITourMetaData } from '../types/tour.meta.data.class';
import { IUserData } from '../types/user.data.class';
import { ILoginCredentials } from '../types/login.credentials.class';
import { Helper } from '../helper/helper';
import { Logger } from '../logger/logger';
var _dbImpl: IDBIntf;

export module Authentication {
    var _cpoId = 'Authentication';
    /**
     * Starts the module up
     */
    export function startUp() {
        _dbImpl = Database.getDbConnection();
    }

    /**
     * testing only
     * attention: this shuts down the whole db-module
     */
    export function shutDown() {
        (_dbImpl as DbImpl).shutDown();
    }

    /**
     * returns a configured instance of passport
     * @returns {passport.PassportStatic} the passport instance
     */
    export function getConfiguredPassport(): passport.PassportStatic {

        var LocalStrategy = passportLocal.Strategy;
        passport.use('local', new LocalStrategy(
            async function (username, password, done) {
                let userFromDb: IUserData;
                try {
                    userFromDb = await _dbImpl.GetUserByName(username);
                } catch (err) {
                    Logger.Warning(_cpoId, 'got error from Db', err);
                    return done(err, null);
                }
                if (await Helper.comparePassword(password, userFromDb!.password)) {
                    // successful authentication
                    return done(null, userFromDb);
                } else {
                    // not authenticated
                    return done('Username oder Password invalid', null);
                }
            },
        ));

        passport.serializeUser<IUserData, number>(function (userFromSession, done) {
            if (userFromSession) {
                return done(null, userFromSession.id);
            }
            // throw error
            return done(null, 0 /* 0 is an invalid user_id */);
        });

        passport.deserializeUser<IUserData, number>(async function (idStoredInSession, done) {
            const userFromDb: IUserData = await _dbImpl.GetUserById(idStoredInSession);
            return done(null, userFromDb);
        });

        return passport;
    }

}
