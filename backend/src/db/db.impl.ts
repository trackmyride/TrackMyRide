import * as mysql from 'mysql';
import { IDBIntf } from './db.intf';
import { ITourMetaData } from '../types/tour.meta.data.class';
import { ILogDataEntry } from '../types/log.data.entry.class';
import { ITour } from '../types/tour.class';
import { ConfigProvider } from '../config/config.provider';
import { Helper } from '../helper/helper';
import { IUserData } from '../types/user.data.class';
import { Logger } from '../logger/logger';
const Config = ConfigProvider.Config;

export class DbImpl implements IDBIntf {

    private _pool: mysql.Pool;
    /**
     * Creates an instance of DbImpl.
     * @memberof DbImpl
     */
    constructor() {
        this._pool = mysql.createPool(Config.Database);
    }

    /**
     * Shuts the db pool down
     * @memberof DbImpl
     */
    public shutDown() {
        this._pool.end();
    }

    /**
     * get all public tours from the db
     * @returns {Promise<ITourMetaData[]>} the resulted tours
     * @memberof DbImpl
     */
    public async GetPublicTours(): Promise<ITourMetaData[]> {
        return new Promise<ITourMetaData[]>((resolve, reject) => {
            this._pool.query('CALL sp_get_public_tours()', function (error, results: any[], fields) {
                if (error) {
                    console.log(error);
                    reject('something went wrong with db');
                } else {
                    let dnc = results[0];
                    let metaDatas: ITourMetaData[] = dnc.map((result: any) => {
                        return {
                            id: result.id,
                            userId: result.user_id,
                            public: result.public === 1,
                            name: result.name,
                            description: result.description,
                            startTime: new Date(result.start_time),
                            endTime: new Date(result.end_time),
                            rating: result.rating,
                            picture: { name: '', resource: '' }, //TODO implement
                        };
                    });
                    resolve(metaDatas);
                }
            });
        });
    }

    /**
     * gets all tours of a user
     * @param {number} userId the id of the user
     * @returns {Promise<ITourMetaData[]>} the metadata of the tours
     * @memberof DbImpl
     */
    public async GetUserTours(userId: number): Promise<ITourMetaData[]> {
        return new Promise<ITourMetaData[]>((resolve, reject) => {
            this._pool.query('CALL sp_get_user_tours(?)', [userId], function (error, results: any[], fields) {
                if (error) {
                    console.log(error);
                    reject('something went wrong with db');
                } else {
                    let rows = results[0];
                    let metaDatas: ITourMetaData[] = rows.map((result: any) => {
                        return {
                            id: result.id,
                            userId: result.user_id,
                            public: result.public === 1,
                            name: result.name,
                            description: result.description,
                            startTime: new Date(result.start_time),
                            endTime: new Date(result.end_time),
                            rating: result.rating,
                            picture: { name: '', resource: '' }, //TODO implement
                        };
                    });
                    resolve(metaDatas);
                }
            });
        });
    }

    /**
     * adds a tour to the database
     * @param {number} userID the id of the user
     * @param {Date} startTime the starttime of the tour
     * @param {Date} endTime the endtime of the tour
     * @param {string} title the title of the tour
     * @param {string} description the description of the tour
     * @returns {Promise<number>} the number of the generated tour
     * @memberof DbImpl
     */
    public async CreateTour(userID: number, startTime: Date, endTime: Date, title: string, description: string): Promise<number> {
        return new Promise<number>((resolve, reject) => {
            try {
                this._pool.query('CALL sp_create_tour(?,?,?,?,?)', [userID, startTime, endTime, title, description], function (error, results: any[], fields) {
                    if (error || results[0].length < 1) {
                        console.log(error);
                        reject('nothing returned at create tour');
                    } else {
                        let rows = results[0];
                        let createdId = rows[0]['LAST_INSERT_ID()'];
                        resolve(createdId);
                    }
                });
            } catch (e) {
                Logger.Error('db.impl', 'create tour failed with error', e);
                reject('something throwed an error while creating the tour');
            }
        });
    }

    /**
     * inserts log data into db of a tour
     * @param {number} tourId the id of the tour
     * @param {number} userId the id of the user
     * @param {ILogDataEntry[]} logData the logdata to insert
     * @returns {Promise<void>} an emtpy promise that resolves after insertion finished
     * @memberof DbImpl
     */
    public async InsertLogDataToTour(tourId: number, userId: number, logData: ILogDataEntry[]): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            try {
                var sql = 'INSERT INTO `trackmyride`.`tour_data`\
                (`user_id`,\
                `tour_id`,\
                `time`,\
                `lat`,\
                `long`,\
                `elev`,\
                `velocity`,\
                `heading`,\
                `bearing`,\
                `accuracy`,\
                `ax`,\
                `ay`,\
                `az`,\
                `gx`,\
                `gy`,\
                `gz`,\
                `azimuth`,\
                `pitch`,\
                `roll`)\
                VALUES ?';
                var values = logData.map((e) => {
                    let timeString = '';
                    if (typeof e.time === 'string') {
                        timeString = Helper.timeStringToDateStringForDb(e.time);
                    }
                    return [
                        userId,
                        tourId,
                        timeString,
                        e.latitude || 0,
                        e.longitude || 0,
                        e.altitude || 0,
                        e.velocity || 0,
                        e.heading || 0,
                        e.bearing || 0,
                        e.accuracy || 0,
                        e.acceleration.x || 0,
                        e.acceleration.y || 0,
                        e.acceleration.z || 0,
                        e.gyroscope.x || 0,
                        e.gyroscope.y || 0,
                        e.gyroscope.z || 0,
                        e.orientation.azimuth || 0,
                        e.orientation.pitch || 0,
                        e.orientation.roll || 0,
                    ];
                });
                this._pool.query(sql, [values], function (error) {
                    if (error) {
                        console.log(error);
                        reject('inserting data did not work out..');
                    } else {
                        resolve();
                    }
                });
            } catch (e) {
                reject('something threw an error while inserting rows into data');
            }
        });
    }

    /**
     * registers a user
     * @param {string} username the name of the user
     * @param {string} email an email address of the user
     * @param {string} passwordHash the hash of the password (password must already be hashed!)
     * @param {string} avatar base64 avatar
     * @returns {Promise<number>} a promise with the user id
     * @memberof DbImpl
     */
    public async RegisterUser(username: string, email: string, passwordHash: string, avatar?: string): Promise<number> {
        return new Promise<number>((resolve, reject) => {
            try {
                var pool = this._pool;
                this._pool.query('CALL sp_register_user(?,?,?)', [username, email, passwordHash], function (error, results: any[], fields) {
                    if (error || results[0].length < 1) {
                        console.log(error);
                        reject('nothing returned at register user');
                    } else {
                        let rows = results[0];
                        let createdId = rows[0]['LAST_INSERT_ID()'];
                        if (avatar !== undefined) {
                            console.log(avatar);
                            pool.query('UPDATE `user` SET `avatar` = ? WHERE `id` = ?;', [avatar, createdId]);
                        }
                        resolve(createdId);
                    }
                });
            } catch (e) {
                reject('something throwed an error while registering the user');
            }
        });
    }

    /**
     * get a user from db by it's name
     * @param {string} username the username
     * @returns {Promise<IUserData>} the userdata
     * @memberof DbImpl
     */
    public async GetUserByName(username: string): Promise<IUserData> {
        return new Promise<IUserData>((resolve, reject) => {
            try {
                this._pool.query('CALL sp_get_user_by_name(?)', [username], function (error, results: any[], fields) {
                    if (error || results[0].length !== 1) {
                        console.log(error);
                        reject('no user with this name or more than one');
                    } else {
                        let userFromDb = results[0][0];
                        let user: IUserData = {
                            username: userFromDb.username,
                            email: userFromDb.email,
                            avatar: userFromDb.avatar ? userFromDb.avatar.toString('ascii') : null,
                            password: userFromDb.password,
                            id: userFromDb.id,
                        };
                        resolve(user);
                    }
                });
            } catch (e) {

                console.log(e);
                reject('something throwed an error while searching for user');
            }
        });
    }

    /**
     * get a user by it's id
     * @param {number} id the id of the user
     * @returns {Promise<IUserData>} the userdata
     * @memberof DbImpl
     */
    public async GetUserById(id: number): Promise<IUserData> {
        return new Promise<IUserData>((resolve, reject) => {
            try {
                this._pool.query('CALL sp_get_user_by_id(?)', [id], function (error, results: any[], fields) {
                    if (error || results[0].length !== 1) {
                        console.log(error);
                        reject('no user with this id or more than one');
                    } else {
                        let userFromDb = results[0][0];
                        let user: IUserData = {
                            username: userFromDb.username,
                            email: userFromDb.email,
                            avatar: userFromDb.avatar ? userFromDb.avatar.toString('ascii') : null,
                            password: userFromDb.password,
                            id: userFromDb.id,
                        };

                        resolve(user);
                    }
                });
            } catch (e) {
                reject('something throwed an error while searching for user');
            }
        });
    }

    /**
     * gets metadata and logdata of a tour
     * @param {number} tourId the id of the tour
     * @returns {Promise<ITour>} the tourdata
     * @memberof DbImpl
     */
    public async GetTourData(tourId: number): Promise<ITour> {
        return new Promise<ITour>((resolve, reject) => {
            this._pool.query('CALL sp_get_tour_data(?)', [tourId], async (error, results: any[], fields) => {
                console.log('can set query on db');
                if (error) {
                    console.log(error);
                    reject('something went wrong with db');
                } else {
                    let logData: ILogDataEntry[] = results[0].map((row: any) => {
                        return {
                            longitude: row.long,
                            latitude: row.lat,
                            time: row.time,
                            velocity: row.velocity,
                            altitude: row.elev,
                            heading: row.heading,
                            accuracy: row.accuracy,
                            bearing: row.bearing,
                            acceleration: { x: row.ax, y: row.ay, z: row.az },
                            gyroscope: { x: row.gx, y: row.gy, z: row.gz },
                            orientation: { azimuth: row.azimuth, pitch: row.pitch, roll: row.roll },
                        };
                    });
                    let metaData = await this.GetTourMetaData(tourId);
                    resolve({ ...metaData, log: logData });
                }
            });
        });
    }

    /**
     * gets only the metadata of a tour
     * @param {number} tourId the id of the tour
     * @returns {Promise<ITourMetaData>} the metadata of the tour
     * @memberof DbImpl
     */
    public async GetTourMetaData(tourId: number): Promise<ITourMetaData> {
        return new Promise<ITourMetaData>((resolve, reject) => {
            this._pool.query('CALL sp_get_tour(?)', [tourId], function (error, results: any[], fields) {
                if (error || results[0].length < 1) {
                    console.log(error);
                    reject('something went wrong with db');
                } else {
                    let result = results[0][0];
                    let metaData: ITourMetaData = {
                        id: tourId,
                        userId: result.user_id,
                        public: result.public === 1,
                        name: result.name,
                        description: result.description,
                        startTime: new Date(result.start_time),
                        endTime: new Date(result.end_time),
                        rating: result.rating,
                        picture: { name: '', resource: '' }, //TODO implement
                    };
                    resolve(metaData);
                }
            });
        });
    }

    /**
     * updates the meta data of a tour
     * @param {ITourMetaData} metaData metadata of the tour
     * @returns {Promise<void>} emtpy promise
     * @memberof DbImpl
     */
    public UpdateTourMetaData(metaData: ITourMetaData): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this._pool.query('UPDATE `tour` SET `public` = ?, `processed` = ?, `name` = ?, `description` = ? WHERE `id` = ?;', [
                metaData.public ? 1 : 0, metaData.public ? 1 : 0, metaData.name, metaData.description, metaData.id,
            ], (error, results: any[], fields) => {
                if (error) {
                    console.log(error);
                    reject('something went wrong with db');
                } else {
                    resolve();
                }
            });
        });
    }

    /**
     * calculates some easy statistics of a tour
     * unused
     * @param {number} tourId id of the tour
     * @returns {Promise<void>} empty promise
     * @memberof DbImpl
     */
    public CalculateTourStatistics(tourId: number): Promise<void> {
        //TODO: implement
        throw new Error('Method not implemented.');
    }

    /**
     * Deletes a tour from the db
     * @param {number} tourId the id of the tour to be deleted
     * @returns {Promise<void>} empty promise
     * @memberof DbImpl
     */
    public DeleteTour(tourId: number): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this._pool.query('CALL sp_delete_tour(?)', [tourId], function (error, results: any[], fields) {
                if (error) {
                    console.log(error);
                    reject('something went wrong with db');
                } else {
                    resolve();
                }
            });
        });
    }
}
