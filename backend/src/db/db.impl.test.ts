import * as express from 'express';
import { Database } from './database';
import { } from 'jest';
import { DbImpl } from './db.impl';
import { Helper } from '../helper/helper';
import * as mysql from 'mysql';

let dbImpl: DbImpl;
let endMock = jest.fn();
let queryMock = jest.fn();

let mockPool = {
    end: endMock,
    query: queryMock,
};

let spy = jest.spyOn(mysql, 'createPool');
spy.mockImplementation(() => {
    return mockPool;
});

beforeEach(() => {
    dbImpl = Database.getDbConnection();
    endMock.mockReset();
    queryMock.mockReset();
});

afterEach(() => {
    dbImpl.shutDown();
});

describe('Db Impl', () => {
    describe('function createTour', () => {
        it('should create a Tour', async () => {
            queryMock.mockImplementation((query, fields, callback) => {
                callback(null, [[{ 'LAST_INSERT_ID()': 5 }]], null);
            });
            let tourId = await dbImpl.CreateTour(1, Helper.timeStringToDateObjectForDb('1522415412149'), Helper.timeStringToDateObjectForDb('1522415412149'), '', '');
            expect(tourId).toBe(5);
        });
        it('should reject if an error is returned from db', async () => {
            queryMock.mockImplementation((query, fields, callback) => {
                callback('testerror', null, null);
            });
            let promise = dbImpl.CreateTour(1, Helper.timeStringToDateObjectForDb('1522415412149'), Helper.timeStringToDateObjectForDb('1522415412149'), '', '');
            expect(promise).rejects.toBe('nothing returned at create tour');
        });
        it('should reject if nothing is returned from db', async () => {
            queryMock.mockImplementation((query, fields, callback) => {
                callback(null, [[]], null);
            });
            let promise = dbImpl.CreateTour(1, Helper.timeStringToDateObjectForDb('1522415412149'), Helper.timeStringToDateObjectForDb('1522415412149'), '', '');
            expect(promise).rejects.toBe('nothing returned at create tour');
        });
        it('should reject if db throws error', async () => {
            let error = new Error('testerror');
            queryMock.mockImplementation((query, fields, callback) => {
                throw error;
            });
            let p1 = Helper.timeStringToDateObjectForDb('1522415412149');
            let p2 = Helper.timeStringToDateObjectForDb('1522415412149');
            expect(dbImpl.CreateTour(1, p1, p2, '', '')).rejects.toBe('something throwed an error while creating the tour');
        });
    });
    describe('function InsertLogDataToTour', () => {
        const logData = [{
            time: '1522415412149',
            latitude: 47.6336832,
            longitude: 8.7944755,
            altitude: 0,
            velocity: 0,
            bearing: 0,
            accuracy: 14.583999633789062,
            acceleration: { x: 0, y: 0, z: 0 },
            gyroscope: { x: 0, y: 0, z: 0 },
            orientation: { azimuth: 0, pitch: 0, roll: 0 },
        },
        {
            time: '1522415412153',
            latitude: 48.6436832,
            longitude: 8.7944755,
            altitude: 0,
            velocity: 0,
            bearing: 0,
            accuracy: 14.583999633789062,
            acceleration: { x: 0, y: 0, z: 0 },
            gyroscope: { x: 0, y: 0, z: 0 },
            orientation: { azimuth: 0, pitch: 0, roll: 0 },
        }];
        it('should send TourLogData to db', async () => {
            queryMock.mockImplementation((query, fields, callback) => {
                callback(null, null, null);
            });
            let promise = dbImpl.InsertLogDataToTour(5, 1, logData as any);
            expect(promise).resolves.toBeCalled();
        });
        it('should reject when db returns error', async () => {
            queryMock.mockImplementation((query, fields, callback) => {
                callback('testerror', null, null);
            });
            let promise = dbImpl.InsertLogDataToTour(5, 1, logData as any);
            expect(promise).rejects.toBe('inserting data did not work out..');
        });
        it('should reject when db throws error', async () => {
            queryMock.mockImplementation((query, fields, callback) => {
                throw new Error('test');
            });
            let promise = dbImpl.InsertLogDataToTour(5, 1, logData as any);
            expect(promise).rejects.toBe('something threw an error while inserting rows into data');
        });
    });
    describe('function RegisterUser', () => {
        it('should call db', async () => {
            queryMock.mockImplementation((query, fields, callback) => {
                callback(null, [[{ 'LAST_INSERT_ID()': 1337 }]], null);
            });
            let userId = await dbImpl.RegisterUser('testuser', 'test@test', 'testhash');
            expect(userId).toBe(1337);
            expect(queryMock).toHaveBeenCalledWith(expect.any(String), ['testuser', 'test@test', 'testhash'], expect.any(Function));
        });
        it('should reject when db returns error', async () => {
            queryMock.mockImplementation((query, fields, callback) => {
                callback('testerror', null, null);
            });
            let promise = dbImpl.RegisterUser('testuser', 'test@test', 'testhash');
            expect(promise).rejects.toBe('nothing returned at register user');
        });
        it('should reject when db throws error', async () => {
            queryMock.mockImplementation((query, fields, callback) => {
                throw new Error('test');
            });
            let promise = dbImpl.RegisterUser('testuser', 'test@test', 'testhash');
            expect(promise).rejects.toBe('something throwed an error while registering the user');
        });
    });
    describe('function GetUserByName', () => {
        let user = {
            username: 'test',
            email: 'test',
            avatar: 'test',
            password: 'test',
            id: 5,
        };
        it('should call db', async () => {
            queryMock.mockImplementation((query, fields, callback) => {
                callback(null, [[user]], null);
            });
            let userFromDb = await dbImpl.GetUserByName('testuser');
            expect(userFromDb).toEqual(user);
            expect(queryMock).toHaveBeenCalledWith(expect.any(String), ['testuser'], expect.any(Function));
        });
        it('should reject when db returns error', async () => {
            queryMock.mockImplementation((query, fields, callback) => {
                callback('testerror', null, null);
            });
            let promise = dbImpl.GetUserByName('testuser');
            expect(promise).rejects.toBe('no user with this name or more than one');
        });
        it('should reject when db throws error', async () => {
            queryMock.mockImplementation((query, fields, callback) => {
                throw new Error('test');
            });
            let promise = dbImpl.GetUserByName('testuser');
            expect(promise).rejects.toBe('something throwed an error while searching for user');
        });
    });
    describe('function GetUserById', () => {
        let user = {
            username: 'test',
            email: 'test',
            avatar: 'test',
            password: 'test',
            id: 5,
        };
        it('should call db', async () => {
            queryMock.mockImplementation((query, fields, callback) => {
                callback(null, [[user]], null);
            });
            let userFromDb = await dbImpl.GetUserById(5);
            expect(userFromDb).toEqual(user);
            expect(queryMock).toHaveBeenCalledWith(expect.any(String), [5], expect.any(Function));
        });
        it('should reject when db returns error', async () => {
            queryMock.mockImplementation((query, fields, callback) => {
                callback('testerror', null, null);
            });
            let promise = dbImpl.GetUserById(5);
            expect(promise).rejects.toBe('no user with this name or more than one');
        });
        it('should reject when db throws error', async () => {
            queryMock.mockImplementation((query, fields, callback) => {
                throw new Error('test');
            });
            let promise = dbImpl.GetUserById(5);
            expect(promise).rejects.toBe('something throwed an error while searching for user');
        });
    });
    describe('function GetTourData', () => {
        let tourlogDataFromDb = [{
            lat: 13,
            long: 12,
            time: '20180404 12:12',
            velocity: 34,
            elev: 400,
            heading: 230,
            accuracy: 0,
            bearing: 0,
            ax: 1,
            ay: 2,
            az: 3,
            gx: 4,
            gy: 5,
            gz: 6,
            azimuth: 7,
            pitch: 8,
            roll: 9,
        }];
        let tourMetaDataFromDb = {
            user_id: 4,
            public: 1,
            name: 'test',
            description: 'test',
            start_time: 0,
            end_time: 0,
        };
        let expectedTourData = {
            id: 5,
            userId: 4,
            public: true,
            name: 'test',
            description: 'test',
            startTime: new Date(0),
            endTime: new Date(0),
            picture: { name: '', resource: '' },
            log: [{
                longitude: 12,
                latitude: 13,
                time: '20180404 12:12',
                velocity: 34,
                altitude: 400,
                heading: 230,
                accuracy: 0,
                bearing: 0,
                acceleration: { x: 1, y: 2, z: 3 },
                gyroscope: { x: 4, y: 5, z: 6 },
                orientation: { azimuth: 7, pitch: 8, roll: 9 },
            }],
        };
        it('should call db', async () => {
            queryMock.mockImplementationOnce((query, fields, callback) => {
                callback(null, [tourlogDataFromDb], null);
            })
                .mockImplementationOnce((query, fields, callback) => {
                    callback(null, [[tourMetaDataFromDb]], null);
                });
            let userFromDb = await dbImpl.GetTourData(5);
            expect(userFromDb).toEqual(expectedTourData);
            expect(queryMock).toHaveBeenCalledWith(expect.any(String), [5], expect.any(Function));
        });
        it('should reject when db returns error', async () => {
            queryMock.mockImplementation((query, fields, callback) => {
                callback('testerror', null, null);
            });
            let promise = dbImpl.GetTourData(5);
            expect(promise).rejects.toBe('something went wrong with db');
        });
    });
    describe('function GetTourMetaData', () => {
        let tourMetaDataFromDb = {
            user_id: 4,
            public: 1,
            name: 'test',
            description: 'test',
            start_time: 0,
            end_time: 0,
        };
        let expectedTourMetaData = {
            id: 5,
            userId: 4,
            public: true,
            name: 'test',
            description: 'test',
            startTime: new Date(0),
            endTime: new Date(0),
            picture: { name: '', resource: '' },
        };
        it('should call db', async () => {
            queryMock.mockImplementation((query, fields, callback) => {
                callback(null, [[tourMetaDataFromDb]], null);
            });
            let tourFromDb = await dbImpl.GetTourMetaData(5);
            expect(tourFromDb).toEqual(expectedTourMetaData);
            expect(queryMock).toHaveBeenCalledWith(expect.any(String), [5], expect.any(Function));
        });
        it('should reject when db returns error', async () => {
            queryMock.mockImplementation((query, fields, callback) => {
                callback('testerror', null, null);
            });
            let promise = dbImpl.GetTourMetaData(5);
            expect(promise).rejects.toBe('something went wrong with db');
        });
        it('should reject when db returns nothing', async () => {
            queryMock.mockImplementation((query, fields, callback) => {
                callback('testerror', [[]], null);
            });
            let promise = dbImpl.GetTourMetaData(5);
            expect(promise).rejects.toBe('something went wrong with db');
        });
    });
    describe('function CalculateTourStatistics', () => {
        it('should throw not implemented error', async () => {
            expect(() => {
                let promise = dbImpl.CalculateTourStatistics(1);
            }).toThrowError('Method not implemented.');
        });
    });
    describe('function UpdateTourMetaData', () => {
        let tourMetaData = {
            id: 5,
            userId: 4,
            public: true,
            name: 'testName',
            description: 'testDescription',
            startTime: new Date(0),
            endTime: new Date(0),
            picture: { name: '', resource: '' },
            rating: 3.0,
        };
        it('should call db', async () => {
            queryMock.mockImplementation((query, fields, callback) => {
                callback(null, [], null);
            });
            await dbImpl.UpdateTourMetaData(tourMetaData);
            expect(queryMock).toHaveBeenCalledWith(expect.any(String), [1, 1, 'testName', 'testDescription', 5], expect.any(Function));
        });
        it('should reject when db returns error', async () => {
            queryMock.mockImplementation((query, fields, callback) => {
                callback('testerror', null, null);
            });
            let promise = dbImpl.UpdateTourMetaData(tourMetaData);
            expect(promise).rejects.toBe('something went wrong with db');
        });
    });
    describe('function DeleteTourById', () => {
        it('should call db', async () => {
            queryMock.mockImplementation((query, fields, callback) => {
                callback(null, [], null);
            });
            await dbImpl.DeleteTour(5);
            expect(queryMock).toHaveBeenCalledWith(expect.any(String), [5], expect.any(Function));
        });
        it('should reject when db returns error', async () => {
            queryMock.mockImplementation((query, fields, callback) => {
                callback('testerror', null, null);
            });
            let promise = dbImpl.DeleteTour(5);
            expect(promise).rejects.toBe('something went wrong with db');
        });
    });
});
