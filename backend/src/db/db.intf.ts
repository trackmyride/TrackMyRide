import { ITourMetaData } from '../types/tour.meta.data.class';
import { ILogDataEntry } from '../types/log.data.entry.class';
import { ITour } from '../types/tour.class';
import { IUserData } from '../types/user.data.class';

export interface IDBIntf {
    GetPublicTours(): Promise<ITourMetaData[]>;
    GetUserTours(userId: number): Promise<ITourMetaData[]>;
    CreateTour(userID: number, startTime: Date, endTime: Date, title: string, description: string): Promise<number>; //tourId
    InsertLogDataToTour(tourId: number, userId: number, logData: ILogDataEntry[]): Promise<void>;
    RegisterUser(username: string, email: string, passwordHash: string, avatar: string): Promise<number>;
    GetUserByName(username: string): Promise<IUserData>;
    GetUserById(username: number): Promise<IUserData>;
    GetTourData(tourId: number): Promise<ITour>;
    GetTourMetaData(tourId: number): Promise<ITourMetaData>;
    UpdateTourMetaData(metaData: ITourMetaData): Promise<void>;
    CalculateTourStatistics(tourId: number): Promise<void>;
    DeleteTour(tourId: number): Promise<void>;
}
