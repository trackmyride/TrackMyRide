import { DbImpl } from './db.impl';

export module Database {

    /**
     * returns a DB implementation
     * @returns {DbImpl} an instance of the db connection
     */
    export function getDbConnection(): DbImpl {
        return new DbImpl();
    }

}
