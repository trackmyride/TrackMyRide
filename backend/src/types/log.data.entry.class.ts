export interface ILogDataEntry {
    longitude: number;
    latitude: number;
    time: number;
    velocity: number;
    altitude: number;
    heading: number;
    accuracy: number;
    bearing: number;
    acceleration: IAcceleration;
    gyroscope: IGyroscope;
    orientation: IOrientation;

}

export interface IAcceleration {
    x: number;
    y: number;
    z: number;
}

export interface IGyroscope {
    x: number;
    y: number;
    z: number;
}

export interface IOrientation {
    azimuth: number;
    pitch: number;
    roll: number;
}
