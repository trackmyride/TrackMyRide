import { ILoginCredentials } from './login.credentials.class';

export interface IUserData extends ILoginCredentials {
    email: string;
    avatar: string;
    id: number;
}
