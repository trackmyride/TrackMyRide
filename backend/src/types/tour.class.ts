import { ILogDataEntry } from './log.data.entry.class';
import { ITourMetaData } from './tour.meta.data.class';

export interface ITour extends ITourMetaData {
    log: ILogDataEntry[];
}
