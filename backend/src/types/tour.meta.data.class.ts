export interface ITourMetaData {
    id: number;
    userId: number;
    public: boolean;
    name: string;
    description: string;
    startTime: Date;
    endTime: Date;
    rating: number;
    picture: IPicture;
}

export interface IPicture {
    name: string;
    resource: string;
}

export interface IComment {
    userID: number;
    text: string;
}
