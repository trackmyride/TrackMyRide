import { Api } from '../api';
import * as express from 'express';
import * as session from 'express-session';
import * as passport from 'passport';
import { Database } from '../../db/database';
import { IDBIntf } from '../../db/db.intf';
import { DbImpl } from '../../db/db.impl';
import { ITourMetaData } from '../../types/tour.meta.data.class';
import { ILogDataEntry } from '../../types/log.data.entry.class';
import { Helper } from '../../helper/helper';
import { Logger } from '../../logger/logger';
var _dbImpl = {} as any as IDBIntf;

export module UserApi {
    var _cpoId = 'UserApi';
    /**
     * Starts up the user ai module
     * adds all calls to the api and gets a db connection
     */
    export function startUp() {
        _dbImpl = Database.getDbConnection();
        Api.registerApiMethod('POST', '/register', registerUser);
        Api.registerApiMethod('POST', '/login', loginUser);
        Api.registerApiMethod('GET', '/logout', logoutUser);
        Api.registerApiMethod('GET', '/user', getUser);
    }

    /**
     * Testing only
     * Attention this shuts down the whole db module also for all other modules
     */
    export function shutDown() {
        (_dbImpl as DbImpl).shutDown();
    }

    /**
     * registers a User
     * 400 invalid input
     * 201 item created
     * @param {express.Request} req the request object
     * @param {express.Response} res the response object
     */
    async function registerUser(req: express.Request, res: express.Response) {
        if (req.body) {
            const username = req.body.username; // todo: check username validity
            const email = req.body.email; // todo: check email validity
            const passwordHash = await Helper.hashPassword(req.body.password);
            const avatar = req.body.avatar;
            _dbImpl.RegisterUser(username, email, passwordHash, avatar);
            res.sendStatus(201 /*item created*/);
        } else {
            Logger.Notice(_cpoId, 'Received Data but is not in correct format!');
            res.sendStatus(400 /*invalid input*/);
        }
    }

    /**
     * logs a user in if correct credentials
     * 401 unauthorized
     * 500 server error
     * 200 success
     * @param {express.Request} req the request object
     * @param {express.Response} res the response object
     */
    function loginUser(req: express.Request, res: express.Response) {
        passport.authenticate('local', function (err, user, info) {
            if (!user) {
                return res.sendStatus(401 /*unauthorized*/);
            }

            req.logIn(user, function (err) {
                if (err) {
                    res.status(500 /*internal server error*/);
                    return res.send({ status: 'err', message: err.message });
                }
                let userObject = getUserObject(user);
                return res.send(userObject);
            });

            return;
        },
        )(req, res);
    }

    /**
     * logs a user out
     * 200 success
     * 500 internal server error
     * @param {express.Request} req the request object
     * @param {express.Response} res the response object
     */
    function logoutUser(req: express.Request, res: express.Response) {
        //terminate user sessin
        req.logout();
        if (req.session) {
            req.session.destroy((err) => { });
        }
        res.sendStatus(200 /*session terminated*/);
    }

    /**
     * returns a userobject
     * 401 unauthorized
     * 200 success
     * @param {express.Request} req the request object
     * @param {express.Response} res the response object
     */
    function getUser(req: express.Request, res: express.Response) {
        if (req.user) {
            const userToSend = getUserObject(req.user);
            res.send(userToSend);
        } else {
            res.sendStatus(401 /*Unauthorized*/);
        }
    }

    function getUserObject(user: any) {
        let userToSend;
        if (user) {
            userToSend = {
                username: user.username,
                userId: user.id,
                email: user.email,
                avatar: user.avatar,
            };
        }
        return userToSend;
    }
}
