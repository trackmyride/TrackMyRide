import * as express from 'express';
import { Api } from '../api';
import { UserApi } from './user.api';
import { } from 'jest';
jest.mock('../api');

describe('UserApi', () => {
    describe('function startUp', () => {
        it('should register all API methods', () => {
            let spy = jest.spyOn(Api, 'registerApiMethod').mockImplementation(jest.fn());

            UserApi.startUp();
            expect(spy).toHaveBeenCalledWith('POST', '/register', expect.any(Function));
            expect(spy).toHaveBeenCalledWith('POST', '/login', expect.any(Function));
            expect(spy).toHaveBeenCalledWith('GET', '/logout', expect.any(Function));
        });
    });
});
