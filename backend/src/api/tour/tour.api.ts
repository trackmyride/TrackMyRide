import { Api } from '../api';
import * as express from 'express';
import { IDBIntf } from '../../db/db.intf';
import { Database } from '../../db/database';
import { ITourMetaData } from '../../types/tour.meta.data.class';
import { ILogDataEntry } from '../../types/log.data.entry.class';
import { ITour } from '../../types/tour.class';
import { DbImpl } from '../../db/db.impl';
import { Helper } from '../../helper/helper';
import { Logger } from '../../logger/logger';
//just dont touch this import! even if typescript really kindly asks you to. just dont!!!
import * as moment from 'moment';
var _dbImpl: IDBIntf;

export module TourApi {
    var _cpoId = 'TourApi';

    /**
     * Starts the instance of the TourApi
     * Attaches all Apis call to the api and gets an instance of the db pool
     */
    export function startUp() {
        Logger.Info(_cpoId, 'starting up Tour Api');
        _dbImpl = Database.getDbConnection();
        Api.registerApiMethod('GET', '/tours', getAllTours);
        Api.registerApiMethod('GET', '/tours/user', getTourByUser);
        Api.registerApiMethod('GET', '/tours/:id', getTourById);
        Api.registerApiMethod('PUT', '/tours/:id', changeTour);
        Api.registerApiMethod('POST', '/tours', addTour);
        Api.registerApiMethod('POST', '/tour', addTour);
        Api.registerApiMethod('DELETE', '/tours/:id', deleteTourById);
    }

    /**
     * testing only
     * attention: this method shuts down the whole db-module and therefore no module will be able to call the db afterwards
     */
    export function shutDown() {
        (_dbImpl as DbImpl).shutDown();
    }

    /**
     * requests all public tours from the db and send them to the frontend
     * 200 array of tours
     * 400 there are not tours available TODO: is that wanted
     * 500 internal server error
     * @param {express.Request} req the request object
     * @param {express.Response} res the response object
     */
    async function getAllTours(req: express.Request, res: express.Response) {
        let allPublicTours: ITourMetaData[] = await _dbImpl.GetPublicTours();
        res.json(allPublicTours);
    }

    /**
     * requests all tours of a user from the db and send them to the frontend
     * 200 there are own tours
     * 400 there are no public tours TODO: is that wanted
     * 500 internal server error
     * @param {express.Request} req the request object
     * @param {express.Response} res the response object
     */
    async function getTourByUser(req: express.Request, res: express.Response) {
        if (!req.user) {
            res.sendStatus(401 /*Unauthorized*/);
            return;
        }
        let userId = req.user.id;
        try {
            let tour = await _dbImpl.GetUserTours(userId);
            res.json(tour);
        } catch (e) {
            Logger.Warning(_cpoId, 'error in loading tour', e);
            res.sendStatus(500 /*sever-error*/);
        }
    }

    /**
     * requests a tour from the db and sends it to the frontend
     * 200 return tour
     * 400 tour does not exist
     * 403 forbidden
     * 401 unautorized
     * 500 internal server error
     * @param {express.Request} req the request object
     * @param {express.Response} res the response object
     */
    async function getTourById(req: express.Request, res: express.Response) {
        try {
            // TODO: //could lead to a unhandled error
            let userId = Number((req.user && req.user.id) ? req.user.id : '0');
            let requestedTour = (req.params.id);

            let tour: ITour = await _dbImpl.GetTourData(requestedTour);

            tour.log = calculateVelocity(tour.log);

            if (tour.public === true || tour.userId === userId) {
                Logger.Info(_cpoId, 'answering with tour');
                res.json(tour);
            } else if (!req.user) {
                Logger.Warning(_cpoId, 'answering with unauthorized rror');
                res.sendStatus(401 /*Unauthorized*/);
            } else {
                Logger.Warning(_cpoId, 'answering with forbidden rror');
                res.sendStatus(403);
            }
        } catch (e) {
            Logger.Warning(_cpoId, 'error in loading tour', e);
            res.sendStatus(500 /*sever-error*/);
        }
    }

    async function deleteTourById(req: express.Request, res: express.Response) {
        try {
            let userId = Number((req.user && req.user.id) ? req.user.id : '0');
            let requestedTour = (req.params.id);

            let tour: ITourMetaData = await _dbImpl.GetTourMetaData(requestedTour);

            if (tour.userId === userId) {
                Logger.Info(_cpoId, 'deleting tour');
                await _dbImpl.DeleteTour(requestedTour);
                res.sendStatus(200 /*status ok*/);
            } else if (!req.user) {
                Logger.Warning(_cpoId, 'answering with unauthorized error');
                res.sendStatus(401 /*Unauthorized*/);
            } else {
                Logger.Warning(_cpoId, 'answering with forbidden error');
                res.sendStatus(403);
            }
        } catch (e) {
            Logger.Warning(_cpoId, 'error in deleting tour', e);
            res.sendStatus(500 /*sever-error*/);
        }
    }

    /**
     * adds a Tour to the db
     * 201 item created
     * 400 invalid input
     * 401 unauthorized
     * 500 internal error
     * @param {express.Request} req the request object
     * @param {express.Response} res the response object
     */
    async function addTour(req: express.Request, res: express.Response) {
        if (!req.user) {
            res.sendStatus(401 /*Unauthorized*/);
            return;
        }
        let userId = req.user.id;
        try {
            if (req.body) {
                Logger.Info(_cpoId, 'got new tour from app');
                Logger.Debug(_cpoId, 'data from mobile received:', req.body);
                var data: ILogDataEntry[] = req.body.log;

                // sort array before insert
                data = data.sort((a, b) => Number(a.time) - Number(a.time));
                //await _dbImpl.CreateTour(userId, req.body.startTime, req.body.endTime);
                let startTime = Helper.timeStringToDateObjectForDb(data[0].time as any);
                let stopTime = Helper.timeStringToDateObjectForDb(data[data.length - 1].time as any);

                let title = req.user.username + '\'s Tour vom ' + moment(startTime).format('DD.MM.YYYY HH:mm');
                let description = '';

                let tourId = await _dbImpl.CreateTour(userId, startTime, stopTime, title, description);

                await _dbImpl.InsertLogDataToTour(tourId, userId, data);
                res.sendStatus(201 /*item created*/);
            } else {
                Logger.Warning(_cpoId, 'Received Data but is not in correct format!');
                res.sendStatus(400 /*invalid input*/);
            }
        } catch (e) {
            Logger.Warning(_cpoId, 'error in adding tour', e);
            res.sendStatus(500 /*sever-error*/);
        }
    }

    /**
     * updates a tour
     * 201 item updated
     * 400 invalid input, object invalid
     * 401 unauthorized
     * 403 forbidden
     * 500 internal server error
     * @param {express.Request} req the request object
     * @param {express.Response} res the response object
     */
    async function changeTour(req: express.Request, res: express.Response) {
        if (!req.user) {
            res.sendStatus(401 /*Unauthorized*/);
            return;
        }
        let userId = req.user.id;
        try {
            if (req.body) {
                Logger.Info(_cpoId, 'got put call from frontend');
                Logger.Debug(_cpoId, 'got put call from frontend with data:', req.body);
                var data: ITourMetaData = req.body;

                // get tour from db
                const tourFromDb = await _dbImpl.GetTourMetaData(data.id);

                // check if the user owns the tour
                if (userId !== tourFromDb.userId) {
                    res.sendStatus(403 /*Forbidden*/);
                    return;
                }

                //merge with whole data to not delete anything
                let tourToSendBack = tourFromDb;
                tourToSendBack.name = data.name;
                tourToSendBack.description = data.description;
                tourToSendBack.public = data.public;

                //send tour to db
                await _dbImpl.UpdateTourMetaData(tourToSendBack);

                res.sendStatus(201 /*item created*/);
            } else {
                Logger.Warning(_cpoId, 'received no data to update tour!');
                res.sendStatus(400 /*invalid input*/);
            }
        } catch (e) {
            Logger.Warning(_cpoId, 'error in changing tour metadata', e);
            res.sendStatus(500 /*sever-error*/);
        }
    }

    function calculateVelocity(items: ILogDataEntry[]): ILogDataEntry[] {
        let sum = 0;
        for (let k = 1; k < items.length; k++) {
            sum += items[k].velocity;
        }

        if (items.length === 1 || sum > 0) {
            return items;
        }
        // build average of all

        var firstOfTheSame: number = -1;

        //start at 1 because we access i-1
        for (let i = 1; i < items.length; i++) {
            let j = i;
            if (items[i].latitude === items[i - 1].latitude && items[i].longitude === items[i - 1].longitude) {
                if (firstOfTheSame >= 0) {
                    continue;
                } else {
                    firstOfTheSame = i - 1;
                }
            } else {
                if (firstOfTheSame >= 0) {
                    //calculate diffence and set it to all items between the points
                    let totalSpeed = calculateVelocityBetweenPoints(items[firstOfTheSame], items[i]);
                    let speed = totalSpeed / (i - firstOfTheSame);
                    for (let j = firstOfTheSame; j < i; j++) {
                        items[j].velocity = speed;
                    }
                    firstOfTheSame = -1;
                } else {
                    let speed = calculateVelocityBetweenPoints(items[i - 1], items[i]);
                    items[i - 1].velocity = speed;
                }
            }
        }

        //moving average over three elements to reduce huge jumpgs...
        if (items.length > 2) {
            let sum = items[0].velocity + items[1].velocity + items[2].velocity;
            items[0].velocity = sum / 3;
            for (let i = 1; i < items.length - 2; i++) {
                items[i].velocity = sum / 3;
                if (isNaN(items[i].velocity)) {
                    Logger.Warning(_cpoId, 'Calculation is broken: sum: ' + sum + ', i: ' + i + '\n to prevent damage, break the algorithm and return what we have now');
                    break;
                }
                sum -= items[i - 1].velocity;
                sum += items[i + 2].velocity;
            }
            items[items.length - 2].velocity = sum / 3;
            items[items.length - 1].velocity = sum / 3;
        }

        return items;
    }

    function calculateVelocityBetweenPoints(x: ILogDataEntry, y: ILogDataEntry): number {
        var geodist: any = require('geodist');
        var distance = geodist({ lat: x.latitude, lon: x.longitude }, { lat: y.latitude, lon: y.longitude }, { unit: 'meters' });
        var timeBetweenInSeconds = Math.abs(y.time - x.time) / 1000;
        if (timeBetweenInSeconds === 0) {
            timeBetweenInSeconds++;
        }
        //console.log(timeBetweenInSeconds);
        //Logger.Warning(_cpoId, 'time between two datapoints: ' + timeBetweenInSeconds +  ', Distance between: ' + distance);
        return (distance / timeBetweenInSeconds) * 3.6;
    }
}
