import * as express from 'express';
import { Api } from '../api';
import { Database } from '../../db/database';
import { TourApi } from './tour.api';
import { } from 'jest';
jest.mock('../api');

describe('TourApi', () => {
    describe('function startUp', () => {
        it('should register all API methods', () => {
            let spy = jest.spyOn(Api, 'registerApiMethod').mockImplementation(jest.fn());
            let closeSpy = jest.fn();
            let dbSpy = jest.spyOn(Database, 'getDbConnection').mockImplementation(() => {
                return {
                    shutDown: closeSpy,
                };
            });

            TourApi.startUp();
            expect(spy).toHaveBeenCalledWith('GET', '/tours/:id', expect.any(Function));
            expect(spy).toHaveBeenCalledWith('GET', '/tours', expect.any(Function));
            expect(spy).toHaveBeenCalledWith('GET', '/tours/user', expect.any(Function));
            expect(spy).toHaveBeenCalledWith('POST', '/tours', expect.any(Function));
            expect(spy).toHaveBeenCalledWith('PUT', '/tours/:id', expect.any(Function));
            expect(dbSpy).toHaveBeenCalledTimes(1);
            TourApi.shutDown();
            expect(closeSpy).toHaveBeenCalledTimes(1);
        });
    });
});
