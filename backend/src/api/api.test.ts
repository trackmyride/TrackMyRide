import { Webserver } from '../webserver/webserver';
import * as express from 'express';
import { Api } from './api';
import { } from 'jest';

afterEach(() => {
    Api.shutDown();
});

describe('Api', () => {
    describe('function setApp', () => {
        it('should accept an app', () => {
            Api.setApp({ get: () => { }, use: (a: any) => { } } as any);
        });
        it('should use the app for further actions', () => {
            let app = { get: () => { }, use: (a: any) => { } };
            let spy = jest.spyOn(app, 'get');
            Api.setApp(app as any as express.Application);
            Api.registerApiMethod('GET', 'test', (a, b) => { });
            expect(spy).toHaveBeenCalled();
        });
    });
    describe('function registerApiMethod', () => {
        it('should call the correct method of express app `GET`', () => {
            let app = { get: () => { }, use: (a: any) => { } };
            let spy = jest.spyOn(app, 'get');
            Api.setApp(app as any as express.Application);
            Api.registerApiMethod('GET', 'test', (a, b) => { });
            expect(spy).toHaveBeenCalled();
        });
        it('should call the correct method of express app `POST`', () => {
            let app = { post: () => { }, use: (a: any) => { } };
            let spy = jest.spyOn(app, 'post');
            Api.setApp(app as any as express.Application);
            Api.registerApiMethod('POST', 'test', (a, b) => { });
            expect(spy).toHaveBeenCalled();
        });
        it('should call the correct method of express app `PUT`', () => {
            let app = { put: () => { }, use: (a: any) => { } };
            let spy = jest.spyOn(app, 'put');
            Api.setApp(app as any as express.Application);
            Api.registerApiMethod('PUT', 'test', (a, b) => { });
            expect(spy).toHaveBeenCalled();
        });
    });
});
