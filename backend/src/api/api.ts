import * as express from 'express';
import * as session from 'express-session';
import * as MySQLStore from 'express-mysql-session';
import * as path from 'path';
import { Authentication } from '../auth/authentication';
import { ConfigProvider } from '../config/config.provider';
const Config = ConfigProvider.Config;

/* TEST Calls

curl -X POST  -H "Accept: Application/json" -H "Content-Type: application/json" https://www.trackmyride.ch/api/tour -d '{"id":"IDVALUE","name":"Mike"}'
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET https://davemoser.ch/api/tours/1
*/

export module Api {
    var _app: express.Application;
    var _sessionStore: MySQLStore;

    /**
     * Sets an app for the api to listen for api calls on
     * @param {express.Application} app the app etiher the same as the webserver or a different one
     */
    export function setApp(app: express.Application) {
        _app = app;

        _app.use(express.json({ limit: '50mb' })); // for parsing application/json
        _app.use(express.urlencoded({ limit: '50mb', extended: true }));

        var options = {
            host: Config.Database.host,
            user: Config.Database.user,
            password: Config.Database.password,
            database: Config.Database.database,
            createDatabaseTable: true,
            schema: {
                tableName: 'session',
                columnNames: {
                    session_id: 'id',
                    expires: 'expires',
                    data: 'data',
                },
            },
        };

        //attention if this line is uncommented, tessting with a running db will not stop anymore
        var MySQLStore = require('express-mysql-session')(session);
        _sessionStore = new MySQLStore(options);

        //use sessions for tracking logins
        var _sessionOptions: session.SessionOptions = {
            secret: 'no worry, this will be changed later on...',
            resave: false,
            saveUninitialized: false,
            cookie: {
                maxAge: 10368000000, // in milliseconds, is exactly 120 days (~ 4 months)
                httpOnly: true,
                sameSite: 'strict',
            },
            name: 'TrackMyRide.Auth',
            store: _sessionStore as any,
        };
        _app.use(session(_sessionOptions));

        var passport = Authentication.getConfiguredPassport();

        _app.use(passport.initialize());
        _app.use(passport.session());
    }

    /**
     * shuts down the module
     */
    export function shutDown() {
        if (_sessionStore) {
            _sessionStore.close();
        }
    }

    /**
     * with this method an api call can be registered
     * @param {('GET' | 'POST' | 'PUT')} type the type of http request you want to handle with your api call
     * @param {string} name the name of the api call beginning with a slash
     * @param {(req: express.Request, res: express.Response) => void} method the method to call if the api is requested
     */
    export function registerApiMethod(type: 'GET' | 'POST' | 'PUT' | 'DELETE', name: string, method: (req: express.Request, res: express.Response) => void) {
        switch (type) {
            case 'GET':
                _app.get('/api' + name, method);
                break;
            case 'POST':
                _app.post('/api' + name, method);
                break;
            case 'PUT':
                _app.put('/api' + name, method);
                break;
            case 'DELETE':
                _app.delete('/api' + name, method);
                break;
        }
    }
}
