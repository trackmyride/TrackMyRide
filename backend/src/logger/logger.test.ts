import { Logger, LogLevel } from './logger';
import { } from 'jest';
import { ILoggerMedium } from './logger.intf';

let logMock = jest.fn();
let loggerMedium: ILoggerMedium = {
    log: logMock,
};
const _cpoId = 'test';
beforeEach(() => {
    Logger.SetLoggerMedium(loggerMedium);
    Logger.SetLogLevel(LogLevel.Debug);
    logMock.mockReset();
});

describe('Logger', () => {

    describe('function Emergency', () => {
        it('should accept a log to the loglevel Emergency and send it to the loggerMedium', () => {
            Logger.Emergency(_cpoId, 'Emergency');
            expect(loggerMedium.log).toHaveBeenCalledWith(LogLevel.Emergency, _cpoId, 'Emergency', undefined);
        });
    });
    describe('function Alert', () => {
        it('should accept a log to the loglevel Alert and send it to the loggerMedium', () => {
            Logger.Alert(_cpoId, 'Alert');
            expect(loggerMedium.log).toHaveBeenCalledWith(LogLevel.Alert, _cpoId, 'Alert', undefined);
        });
    });
    describe('function Critical', () => {
        it('should accept a log to the loglevel Critical and send it to the loggerMedium', () => {
            Logger.Critical(_cpoId, 'Critical');
            expect(loggerMedium.log).toHaveBeenCalledWith(LogLevel.Critical, _cpoId, 'Critical', undefined);
        });
    });
    describe('function Error', () => {
        it('should accept a log to the loglevel Error and send it to the loggerMedium', () => {
            Logger.Error(_cpoId, 'Error');
            expect(loggerMedium.log).toHaveBeenCalledWith(LogLevel.Error, _cpoId, 'Error', undefined);
        });
    });
    describe('function Warning', () => {
        it('should accept a log to the loglevel Warning and send it to the loggerMedium', () => {
            Logger.Warning(_cpoId, 'Warning');
            expect(loggerMedium.log).toHaveBeenCalledWith(LogLevel.Warning, _cpoId, 'Warning', undefined);
        });
    });
    describe('function Notice', () => {
        it('should accept a log to the loglevel Notice and send it to the loggerMedium', () => {
            Logger.Notice(_cpoId, 'Notice');
            expect(loggerMedium.log).toHaveBeenCalledWith(LogLevel.Notice, _cpoId, 'Notice', undefined);
        });
    });
    describe('function Info', () => {
        it('should accept a log to the loglevel Info and send it to the loggerMedium', () => {
            Logger.Info(_cpoId, 'Info');
            expect(loggerMedium.log).toHaveBeenCalledWith(LogLevel.Info, _cpoId, 'Info', undefined);
        });
    });
    describe('function Debug', () => {
        it('should accept a log to the loglevel Debug and send it to the loggerMedium', () => {
            Logger.Debug(_cpoId, 'Debug');
            expect(loggerMedium.log).toHaveBeenCalledWith(LogLevel.Debug, _cpoId, 'Debug', undefined);
        });
    });
    describe('function SetLogLevel', () => {
        it('it should set the loglevel and send any messages with a lower loglevel than the set one', () => {
            Logger.SetLogLevel(LogLevel.Error);
            Logger.Emergency(_cpoId, 'Emergency');
            expect(loggerMedium.log).toHaveBeenCalled();
        });
        it('it should set the loglevel and send a message with the set loglevel', () => {
            Logger.SetLogLevel(LogLevel.Error);
            Logger.Error(_cpoId, 'Error');
            expect(loggerMedium.log).toHaveBeenCalled();
        });
        it('it should set the loglevel and dont send any messages with a higher loglevel than the set one', () => {
            Logger.SetLogLevel(LogLevel.Error);
            Logger.Debug(_cpoId, 'Debug');
            expect(loggerMedium.log).not.toHaveBeenCalled();
        });
    });
    describe('function SetLoggerMedium', () => {
        it('should set the logMedium', () => {
            Logger.SetLoggerMedium(loggerMedium);
            Logger.Emergency(_cpoId, 'Emergency');
            expect(loggerMedium.log).toHaveBeenCalled();
        });
        it('should not call the logMedium if nothing is set', () => {
            Logger.SetLoggerMedium(undefined);
            Logger.Emergency(_cpoId, 'Emergency');
            expect(loggerMedium.log).not.toHaveBeenCalled();
        });
    });
});
