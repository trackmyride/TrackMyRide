import { ILoggerMedium } from './logger.intf';
import { LogLevel } from './logger';

export class ConsoleLogger implements ILoggerMedium {
    private _longestLogLevelName = 9;
    private _longestModuleName = 10;
    public log(logLevel: LogLevel, module: string, message: string, data?: any): void {
        console.log('|' + this.getLogLevelWithSameLength(logLevel) + ' |' + this.getModuleNameWithSameLength(module) + ' | ' + message);
        if (data) {
            if (typeof data === 'object') {
                try {
                    console.log(JSON.stringify(data, null, 4));
                } catch {
                    console.log(data);
                }
            } else {
                console.log(data);
            }
        }
    }

    private getLogLevelWithSameLength(logLevel: LogLevel): string {
        let logLevelString = LogLevel[logLevel];
        let c = 0;
        while (logLevelString.length < this._longestLogLevelName && c < 100) {
            logLevelString = logLevelString + ' ';
            c++;
        }
        return logLevelString.toLocaleUpperCase();
    }
    private getModuleNameWithSameLength(modulename: string): string {
        let c = 0;
        while (modulename.length < this._longestModuleName && c < 100) {
            modulename = modulename + ' ';
            c++;
        }
        return modulename;
    }
}
