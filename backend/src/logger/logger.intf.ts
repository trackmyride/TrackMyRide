import { LogLevel } from './logger';

export interface ILoggerMedium {
    log(logLevel: LogLevel, module: string, message: string, data?: any): void;
}
