import { ILoggerMedium } from './logger.intf';

export module Logger {
    var _activeLogger: ILoggerMedium;
    var _logLevel = 0;

    export function Emergency(module: string, message: string, data?: any) {
        log(LogLevel.Emergency, module, message, data);
    }
    export function Alert(module: string, message: string, data?: any) {
        log(LogLevel.Alert, module, message, data);
    }
    export function Critical(module: string, message: string, data?: any) {
        log(LogLevel.Critical, module, message, data);
    }
    export function Error(module: string, message: string, data?: any) {
        log(LogLevel.Error, module, message, data);
    }
    export function Warning(module: string, message: string, data?: any) {
        log(LogLevel.Warning, module, message, data);
    }
    export function Notice(module: string, message: string, data?: any) {
        log(LogLevel.Notice, module, message, data);
    }
    export function Info(module: string, message: string, data?: any) {
        log(LogLevel.Info, module, message, data);
    }
    export function Debug(module: string, message: string, data?: any) {
        log(LogLevel.Debug, module, message, data);
    }

    export function SetLogLevel(logLevel: LogLevel) {
        _logLevel = logLevel;
    }

    export function SetLoggerMedium(logMedium: ILoggerMedium) {
        _activeLogger = logMedium;
    }

    function log(logLevel: LogLevel, module: string, message: string, data?: any) {
        if (logLevel <= _logLevel) {
            if (_activeLogger) {
                _activeLogger.log(logLevel, module, message, data);
            }
        }
    }
}

export enum LogLevel {
    Emergency,
    Alert,
    Critical,
    Error,
    Warning,
    Notice,
    Info,
    Debug,
}
